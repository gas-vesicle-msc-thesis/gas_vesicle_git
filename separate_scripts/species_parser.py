#!/usr/bin/env python3
"""
Author: Matthijs Smits 950103779040
"""


from sys import argv
import re



def species_parser(infile=argv[1]):
    """
    Parses fasta file.

    infile:String,file name referring to an accessible fasta file
    (default=argv[1]).
    Returns: Dictionary with strings, identifiers as keys, sequences as
    values.
    """
    species_list=[]
    with open(infile) as text:
        for line in text:
            split_line=line.split()
            species_name=split_line[1] + ' ' + split_line[2]
            species_name=species_name.replace('[','')
            species_name=species_name.replace(']','')
            #print(species_name)
        
            species_list.append(species_name)
    return list(set(species_list))
    
def species_text (input_list):
    print(input_list)
    print(len(input_list))
    handle = open('pfam_species.txt','w') 
    for species in input_list:
        handle.write(species+'\n')
    handle.close()
    return None
     
if __name__ == '__main__':
    species_text(species_parser())
