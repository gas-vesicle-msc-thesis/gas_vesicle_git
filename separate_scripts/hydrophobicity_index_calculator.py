#!/usr/bin/env python3
"""
Author: Matthijs Smits 950103779040
"""


from sys import argv
import os
from matplotlib import pyplot as plt


hydro_dict={'A':1.8,'R':-4.5,'N': -3.5, 'D':-3.5, 
            'C':2.5, 'Q':-3.5, 'E':-3.5, 'G':-0.4,
            'H':-3.2,'I':4.5,'L':3.8,'K':-3.9,
            'M':1.9,'F':2.8,'P':-1.6, 'S':-0.8,
            'T':-0.7,'W':-0.9,'Y':-1.3,'V':4.2}
keywords=['gas ','Gvp','vesicle','gvp']
            
def fasta_parser(indir=argv[1]):
    """
    Parses fasta file.

    infile:String,file name referring to an accessible fasta file
    (default=argv[1]).
    Returns: Dictionary with strings, identifiers as keys, sequences as
    values.
    """
    
    for fasta_file in os.listdir(indir):
        if 'all_GVPAs.fasta' in fasta_file:   
            target_file=os.path.join(indir,fasta_file)
            with open(target_file) as text:
                fasta_dict = {}
                for line in text:
                    if line.startswith('>'):
                        label = line[1:].strip()
                        fasta_dict[label] = ''
                    else:
                        fasta_dict[label] += line[:].replace(' ', '').strip()
    return fasta_dict
            
def hydro_scores(protein_string):
    num_residues = (len(protein_string))
    num_residues_list=[residues for residues in range(1,(num_residues+1))]
    hydro_scores=[]
    for letter in protein_string:
        if letter in hydro_dict:
            hydro_scores.append(hydro_dict[letter.upper()])
    return hydro_scores, num_residues, num_residues_list
    
def genome_wide_hydro(fasta_dict):
    hydrophobicity_dict={}
    for key, value in fasta_dict.items():
        scores=hydro_scores(value)
        num_residues=scores[1]
        nett_hydrophobicity=(sum(scores[0]))
        average_hydropobicity= nett_hydrophobicity/num_residues
        hydrophobicity_dict[key]=average_hydropobicity
    return hydrophobicity_dict

def histogram (dictionary):
    hydro_values=list(dictionary.values())
    print(hydro_values)
    average_of_all=sum(hydro_values)/len(hydro_values)
    print(average_of_all)
    plt.hist(hydro_values, bins=20)
    plt.ylabel('Nr of occurences')
    plt.yticks([])
    plt.xlim([-1.5,1.5])
    plt.xlabel('Average hydrophobicity per protein (Overall average:-0.3672)')
    plt.title('500 input genomes containing GvpA domain')
    plt.show()

    return None
    
    
if __name__ == '__main__':
    hydrophobicity_dict=genome_wide_hydro(fasta_parser())
    histogram(hydrophobicity_dict)
    
