#!/usr/bin/env python3
"""
Author: Matthijs Smits 950103779040
"""

from sys import argv
import os
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import SeqFeature, FeatureLocation
from Bio import SeqIO

protein_data_list=[]


def multigene_parser(filepath=argv[1]):
    in_handle = open(filepath, 'r')
    input_file=in_handle.read()
    for record in input_file.split('>>')[1:]:
        info=record.split('\n\n')[1].split('\n')[0].split()[1]
        location_1=record.split('\n\n')[3].split('\n')[1].split()[1]
        location_2=record.split('\n\n')[3].split('\n')[-1].split()[2]
        yield(info,location_1,location_2)
        
def record_maker(filepath):
    in_handle = open(filepath, 'r')
    for record in SeqIO.parse(in_handle, "genbank"):
        yield(record)
        
def record_parser(generator_func,start_cluster,stop_cluster):
    for rec in generator_func:
        previous_stop=0
        chromosome=rec.id
        organism=rec.annotations['organism'].split()
        organism='_'.join(organism)
        topology=rec.annotations['topology']
        sequence=rec.seq
        feats = [feat for feat in rec.features if feat.type == "CDS"]
        for feat in feats:
            location_str=str(feat.location)
            start=int(''.join(i for i in (location_str.split(':')[0]) if i.isdigit()))
            stop=int(''.join(i for i in (location_str.split(':')[1]) if i.isdigit()))
            if start_cluster<=start and stop_cluster>=stop:
                product=str(feat.qualifiers.get('product', "None"))
                if '+' in location_str:
                    sense='+'
                else:
                    sense='-'
                prot_seq=feat.qualifiers.get('translation', "None")[0]
                product=feat.qualifiers.get('product', "None")[0].split()
                product='_'.join(product)
                prot_id=feat.qualifiers.get('protein_id', "None")[0]
                protein_data_tuple=(prot_id,chromosome,organism,product,start,stop,sense,prot_seq)
                previous_stop=stop
                print(protein_data_tuple)
                protein_data_list.append(protein_data_tuple)

def cluster_slicer (gbk_location=argv[2]):
    for info,loc_1,loc_2 in multigene_parser():
        target_file = [filename for filename in os.listdir(gbk_location)
        if filename.startswith(info[:-2])][0]
        genbank_string='%s/%s'%(gbk_location,target_file)
        record_parser(record_maker(genbank_string),int(loc_1),int(loc_2))
       
def fasta_output (protein_data_list):
    with open(argv[3], 'w') as fasta_output:
        for protein_data_tuple in protein_data_list:
            fasta_output.write('>%s|%s|%s|%s|%s|%s|%s\n%s\n'%(protein_data_tuple[0],
            protein_data_tuple[1],protein_data_tuple[2],protein_data_tuple[3],
            protein_data_tuple[4],protein_data_tuple[5],protein_data_tuple[6],
            protein_data_tuple[7]))
            
if __name__ == '__main__':
    cluster_slicer()
    fasta_output(protein_data_list)
    
