#!/usr/bin/env python3
"""
Author: Matthijs Smits 950103779040
"""
from sys import argv

def csv_to_fasta(input_file=argv[1]):
    list_of_lines= open(input_file).read().split('\n')
    print(list_of_lines)
    with open('%s.fasta'%(argv[1].split('.')[0]),'w') as fasta_output:
        for line in list_of_lines:
            if line!='':
                header=line.split(';')[0]
                seq=line.split(';')[1]
                fasta_output.write('>%s\n%s\n'%(header,seq))
    return None



if __name__ == '__main__':
    csv_to_fasta() 
    
