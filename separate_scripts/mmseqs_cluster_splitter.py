#!/usr/bin/env python3
"""
Author: Matthijs Smits 950103779040
Arguments:
1 - Mmseqs2 fasta output
2 - textual list of verified species
3 - Output directory for fasta files 

Further processing:
Alignment - for i in *;do clustalw "$i";done
Hmm creation - for i in *;do hmmbuild "$i.hmm" "$i";done
for file in *.; do mv -- "$file" "$(basename -- "$file" .).hmm";done
"""
from sys import argv

list_of_mmseq_entries = open(argv[1]).readlines()



def cluster_splitter(list_of_entries):
    start_of_cluster=0
    cluster_list=[]
    for index,entry in enumerate (list_of_entries):
        if entry.strip()==list_of_entries[index-1].strip():
            sub_list=list_of_entries[start_of_cluster:index-1]
            start_of_cluster=index-1
            cluster_list.append(sub_list)
            
    return cluster_list
    
def remove_singletons(cluster_list):
    clean_cluster_list=[]
    for index,cluster in enumerate(cluster_list):
        if not len(cluster)==3:
            clean_cluster_list.append(cluster_list[index])
    return clean_cluster_list
    
def remove_non_gvp(cluster_list):
    clean_cluster_list=[]
    for index,cluster in enumerate(cluster_list): #selecting clusters that have at least one occurence of a key word in them
        count=0
        if any("gas " in s for s in cluster) or any("Gvp" in s for s in cluster) or any("vesicle" in s for s in cluster) or any("gas_)" in s for s in cluster):
            clean_cluster_list.append(cluster_list[index])
        for entry in cluster:
            if '|' in entry:
                count+=1
        if count>=30: #clusters that consist of more than 30 proteins will be included even if they don't contain the keywords
            clean_cluster_list.append(cluster_list[index])

    return clean_cluster_list
            
def species_verifier (clean_cluster_list):
    verified_species = open(argv[2]).read().split('\n')
    verified_cluster_list=[]
    for cluster in clean_cluster_list:
        print(len(cluster))
        species_in_cluster=[]
        verified_cluster=False
        for entry in cluster:
            if '|' in entry:
                species= entry.split('|')[2].replace('_',' ')
                species_in_cluster.append(species)
        print(len(species_in_cluster))
        for species in verified_species:
            if any(species in s for s in species_in_cluster):
               
                verified_cluster=True
        if verified_cluster==True:
            verified_cluster_list.append(cluster)
    return verified_cluster_list

def cluster_length (clean_cluster_list):
    minimal_length_cluster_list=[]
    for cluster in clean_cluster_list:
        species_in_cluster=[]
        for entry in cluster:
            if '|' in entry:
                species= entry.split('|')[2].replace('_',' ')
                species_in_cluster.append(species)
        if len(species_in_cluster)>=10:
            minimal_length_cluster_list.append(cluster)
    return minimal_length_cluster_list
    
def fasta_writer(verified_cluster_list):
    output_directory=argv[3]
    for index,cluster in enumerate(verified_cluster_list):
        with open('%s/cluster%s.fasta'%(output_directory, index), 'w') as fasta_output:
            for entry in cluster[1:]:
                fasta_output.write('%s\n'%(entry.strip()))
        print('Wrote cluster %s succesfully' % (index))
    return None
        
            
                
if __name__ == '__main__':
    cluster_list=cluster_splitter(list_of_mmseq_entries)
    print(len(cluster_list))
    clean_cluster_list=remove_singletons(cluster_list)
    print(len(clean_cluster_list))
    cleaner_cluster_list=remove_non_gvp(clean_cluster_list)
    print(len(cleaner_cluster_list))
    #print(cleaner_cluster_list)
    minimal_length_cluster_list=cluster_length(cleaner_cluster_list)
    print(len(minimal_length_cluster_list))
    fasta_writer(minimal_length_cluster_list)
            
