#!/usr/bin/env python3
"""
Author: Matthijs Smits 950103779040
"""

from sys import argv
import os
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import SeqFeature, FeatureLocation
from Bio import SeqIO
        
def record_maker(filepath):
    in_handle = open(filepath, 'r')
    for record in SeqIO.parse(in_handle, "genbank"):
        yield(record)
        
def record_parser(generator_func):
    protein_data_list=[]
    for rec in generator_func:
        previous_stop=0
        chromosome=rec.id
        organism=rec.annotations['organism'].split()
        organism='_'.join(organism)
        topology=rec.annotations['topology']
        sequence=rec.seq
        feats = [feat for feat in rec.features if feat.type == "CDS"]
        for feat in feats:
            location_str=str(feat.location)
            start=int(''.join(i for i in (location_str.split(':')[0]) if i.isdigit()))
            stop=int(''.join(i for i in (location_str.split(':')[1]) if i.isdigit()))
            product=str(feat.qualifiers.get('product', "None"))
            if '+' in location_str:
                sense='+'
            else:
                sense='-'
            prot_seq=feat.qualifiers.get('translation', "None")[0]
            product=feat.qualifiers.get('product', "None")[0].split()
            product='_'.join(product)
            prot_id=feat.qualifiers.get('protein_id', "None")[0]
            protein_data_tuple=(prot_id,chromosome,organism,product,start,stop,sense,prot_seq)
            previous_stop=stop
            print(protein_data_tuple)
            protein_data_list.append(protein_data_tuple)
    return protein_data_list
            
            
                
                
       
def fasta_output (protein_data_list,genbank_location,genbank_filename):
    with open('%s/%s.fasta'%(genbank_location,genbank_filename.replace('gbk', 'fasta')), 'w') as fasta_output:
        for protein_data_tuple in protein_data_list:
            fasta_output.write('>%s|%s|%s|%s|%s|%s|%s\n%s\n'%(protein_data_tuple[0],
            protein_data_tuple[1],protein_data_tuple[2],protein_data_tuple[3],
            protein_data_tuple[4],protein_data_tuple[5],protein_data_tuple[6],
            protein_data_tuple[7]))
            
if __name__ == '__main__':
    gbk_location=argv[1]
    for genbank_file in os.listdir(gbk_location):
        genbank_string='%s/%s'%(gbk_location,genbank_file)
        protein_data_list=record_parser(record_maker(genbank_string))
        fasta_output(protein_data_list,gbk_location,genbank_file)

    
