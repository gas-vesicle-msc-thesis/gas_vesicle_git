#!/usr/bin/env python3
"""
Author: Matthijs Smits 950103779040
"""

from sys import argv

def line_changer(input_file=argv[1]):
    organism_list=[]
    input_list=open(input_file,'r', encoding='utf-8', errors='ignore')
    input_handle=input_list.readlines()
    output_file=open('hamap_species.txt','w')
    for line in input_handle:
        if len(line.split())>1:
            organism_string='%s %s'%(line.split()[0],line.split()[1])
            organism_list.append(organism_string)
    pfam_list=open('pfam_species.txt', 'r').read().split('\n')
    for accession in pfam_list:
        if accession!='':
            organism_list.append(accession)
    unique_organism_list=list(set(organism_list))
    for organism in unique_organism_list:
        output_file.write('%s\n'%(organism))        
    
if __name__ == '__main__':
    line_changer()
        
