#!/usr/bin/env python3
"""
Author: Matthijs Smits 950103779040
argv[1]=list of species or taxids in text format
argv[2]=name of output file where refseqs will be stored
"""
from sys import argv
import os
import time
from urllib.request import urlopen
from Bio import Entrez
Entrez.email = "matthijs.smits@wur.nl"    
                
def refseq_writer(best_refseqs,species,assembly_type):
    print('Largest assembly for species %s was found to be %d refseqs long and is @ level: %s' %(species,len(best_refseqs),
    assembly_type))
    print('Assembly consists of refseqs:')
    print(best_refseqs)
    with open(argv[2],'a') as refseqs_list:
        for refseq in best_refseqs:
            refseqs_list.write('%s\n'%(refseq))
    print('Stored refseqs succesfully')
    return None

def refseq_retriever (species,ID_list,include_scaffold,include_contig):
    max_refseqs=0
    best_refseqs=[]
    for ID in ID_list:
        esummary_handle = Entrez.esummary(db="assembly", id=ID, retmode="xml",report='full')
        esummary_record = Entrez.read(esummary_handle)
        link = esummary_record['DocumentSummarySet']['DocumentSummary'][0]['FtpPath_Assembly_rpt']
        list_of_refseqs=assembly_report_parser_complete(link)
        if len(list_of_refseqs)>max_refseqs:
                best_refseqs=list_of_refseqs
                assembly_type='complete'
                max_refseqs=len(list_of_refseqs)
    if len(best_refseqs)==0 and include_scaffold:
        for ID in ID_list:
            esummary_handle = Entrez.esummary(db="assembly", id=ID, retmode="xml",report='full')
            esummary_record = Entrez.read(esummary_handle)
            link = esummary_record['DocumentSummarySet']['DocumentSummary'][0]['FtpPath_Assembly_rpt']
            list_of_refseqs=assembly_report_parser_scaffold(link)
            if len(list_of_refseqs)>max_refseqs:
                best_refseqs=list_of_refseqs
                assembly_type='scaffold'
                max_refseqs=len(list_of_refseqs)
    if len(best_refseqs)==0 and include_contig:
        for ID in ID_list:
            esummary_handle = Entrez.esummary(db="assembly", id=ID, retmode="xml",report='full')
            esummary_record = Entrez.read(esummary_handle)
            link = esummary_record['DocumentSummarySet']['DocumentSummary'][0]['FtpPath_Assembly_rpt']
            list_of_refseqs=assembly_report_parser_contig(link)
            if len(list_of_refseqs)>max_refseqs:
                best_refseqs=list_of_refseqs
                assembly_type='contig'
                max_refseqs=len(list_of_refseqs)
    if best_refseqs!=[]:
        refseq_writer(best_refseqs,species,assembly_type)
    else:
        print ('Assemblies were found to be inadequate')
        return None
    return None
        
def ID_retriever(species,include_scaffold,include_contig):
    search_term='%s[orgn]'%(species)
    handle = Entrez.esearch(db="assembly", term=search_term, idtype="acc")
    record = Entrez.read(handle)
    ID_list=record["IdList"]
    if len(ID_list)==0:
        print('No assemblies were found for species %s'%(species))
        return None
    print('Found %d assemblies for species %s'%(len(ID_list), species))
    refseq_retriever(species,ID_list,include_scaffold,include_contig)
    return None

def assembly_report_parser_complete(link):
    list_of_refseqs=[]
    assembly_report_handle = urlopen(link)
    assembly_report = assembly_report_handle.read().decode('utf-8')
    if '# Assembly level: Complete Genome' in assembly_report:
        complete_genome_found=True
        last_lines=assembly_report.split('# Sequence-Name')[1].split('\n')[1:]
        for line in last_lines:
            if '\t=\t' in line:
                refseq=line.split('\t=\t')[1].split('\t')[0]
                list_of_refseqs.append(refseq)
            if '\t<>\t' in line:
                refseq=line.split('\t<>\t')[0].split('\t')[-1]
                list_of_refseqs.append(refseq)
    return list_of_refseqs

def assembly_report_parser_scaffold(link):
    list_of_refseqs=[]
    assembly_report_handle = urlopen(link)
    assembly_report = assembly_report_handle.read().decode('utf-8')
    if '# Assembly level: Scaffold' in assembly_report:
        complete_genome_found=True
        last_lines=assembly_report.split('# Sequence-Name')[1].split('\n')[1:]
        for line in last_lines:
            if '\t=\t' in line:
                refseq=line.split('\t=\t')[1].split('\t')[0]
                list_of_refseqs.append(refseq)
            if '\t<>\t' in line:
                refseq=line.split('\t<>\t')[0].split('\t')[-1]
                list_of_refseqs.append(refseq)
    return list_of_refseqs

def assembly_report_parser_contig(link):
    list_of_refseqs=[]
    assembly_report_handle = urlopen(link)
    assembly_report = assembly_report_handle.read().decode('utf-8')
    if '# Assembly level: Contig' in assembly_report:
        complete_genome_found=True
        last_lines=assembly_report.split('# Sequence-Name')[1].split('\n')[1:]
        for line in last_lines:
            if '\t=\t' in line:
                refseq=line.split('\t=\t')[1].split('\t')[0]
                list_of_refseqs.append(refseq)
            if '\t<>\t' in line:
                refseq=line.split('\t<>\t')[0].split('\t')[-1]
                list_of_refseqs.append(refseq)
    return list_of_refseqs
    
def taxid_to_species(taxid,include_scaffold,include_contig):
    search_term='%s[uid]'%(taxid)
    handle = Entrez.esearch(db="taxonomy", term=search_term)
    record = Entrez.read(handle)
    ID=record["IdList"][0]
    esummary_handle = Entrez.esummary(db="taxonomy", id=ID, retmode="xml",report='full')
    esummary_record = Entrez.read(esummary_handle)
    scientific_name=esummary_record[0]['ScientificName']
    print('Taxid %s converted to scientific name %s'%(taxid.strip(),scientific_name))
    time.sleep(30)
    ID_retriever(scientific_name,include_scaffold)
    
    
def main():
    include_scaffold=False
    include_contig=False
    incorrect_input=True
    while incorrect_input:
        userinput1=input('Press S for species, press T for taxids')
        userinput2=input('Press A for complete assemblies only, press S to also incorporate scaffold level assemblies or press C to also incorporate contig level assemblies.')
        if userinput2=='S':
            include_scaffold=True
        if userinput2=='C':
            include_scaffold=True
            include_contig=True
        if userinput1=='S':
            with open(argv[1],'r') as species_list:
                all_species=species_list.readlines()
                for species in all_species:
                    ID_retriever(species.strip(),include_scaffold,include_contig)
                    
            incorrect_input=False
            
        elif userinput2=='T':
            with open(argv[1],'r') as taxid_list:
                taxids=taxid_list.readlines()
                for taxid in taxids:
                    taxid_to_species(taxid.strip(),include_scaffold,include_contig)
            incorrect_input=False
            
        else:
            print('Wrong input')

    

if __name__ == '__main__':
    main()

    
