#!/usr/bin/env python3
"""
Author: Matthijs Smits 950103779040
"""
from sys import argv
import os
import time
list_of_refseqs = open(argv[1]).readlines()
output_directory=argv[2]
from Bio import Entrez
Entrez.email = "matthijs.smits@wur.nl"    
 
 
def genome_downloader (accession):
    output_string='%s/%s.gbk'%(output_directory,accession.strip())
    print(output_string)
    if not os.path.exists(output_string):
        handle = Entrez.efetch(db="nucleotide", id=accession, rettype="gbwithparts", retmode="text")
        record_string=handle.read()
        with open(output_string, 'w') as record_writer:
            record_writer.write(record_string)
        print('Download of %s succesful'%(accession.strip()))
        time.sleep(60)
        
    return None
    

if __name__ == '__main__':
    for accession in list_of_refseqs:
        genome_downloader(accession)
        
