#!/usr/bin/env python3
"""
Author: Matthijs Smits 950103779040
"""

from sys import argv
import csv


def fasta_writer(dictionary_input=argv[1]):
    csv = open(dictionary_input, "r")
    fa=open('fasta_output.fa', 'w')
    count=0
    for line in csv:
        count+=1
        entry_list=line.split("'")
        length=len(entry_list)
        fa.write('>%s %s\n%s\n'%(entry_list[1],entry_list[0][:-3],entry_list[length-2]))
        if count==800:
            break
        
        
if __name__ == '__main__':
    fasta_writer()

