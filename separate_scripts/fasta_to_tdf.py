#!/usr/bin/env python3
"""
Author: Matthijs Smits 950103779040
"""

from sys import argv
import csv
from Bio import SeqIO
search_terms=['GvpA','gvpa ','gvpA']

def fasta_to_tab (fasta_input=argv[1]):
    in_handle = open(fasta_input, 'r')
    with open('tab_delimited_output', 'w') as output:
        for line in in_handle:
            if line.startswith('>'):
                line_list=line.split('|')
                output.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s'%
                (line_list[0][1:],line_list[1],line_list[2],
                line_list[3],line_list[4],line_list[5],line_list[6],
                line_list[7]))
              
            
    return None
def fasta_csv (fasta_input=argv[1]):
    in_handle = open(fasta_input, 'r')
    with open('comma_separated_output.csv', 'w') as output:
        for line in in_handle:
            if line.startswith('>'):
                line_list=line.split('|')
                output.write('%s,%s,%s,%s,%s,%s,%s,%s'%
                (line_list[0][1:],line_list[1],line_list[2],
                line_list[3],line_list[4],line_list[5],line_list[6],
                line_list[7]))
                
def gvpa_finder (fasta_input=argv[1]):
    in_handle = open(fasta_input, 'r')
    out_handle=open('GVPA_fasta.fasta', 'w')
    for record in SeqIO.parse(in_handle,'fasta'):
        product=record.id
        if any(search_term in product for search_term in search_terms) and record.seq!='N':
            ident=str(record.id)
            sequence=str(record.seq)
            out_handle.write('>%s\n%s\n'%(ident,sequence))

def unique (fasta_input=argv[1]):
    found_species=[]
    in_handle = open(fasta_input, 'r')
    out_handle=open('GVPA_unique.fasta', 'w')
    for record in SeqIO.parse(in_handle,'fasta'):
        product=record.id
        species=product.split('|')[2].split('_')[0]
        if not any (found_specie in species for found_specie in found_species):
            found_species.append(species)
            ident=str(record.id)
            sequence=str(record.seq)
            out_handle.write('>%s\n%s\n'%(ident,sequence))
    

if __name__ == '__main__':
    #fasta_to_tab()
    fasta_csv()
    #gvpa_finder()
    #unique()
    
