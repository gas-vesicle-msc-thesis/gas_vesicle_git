#!/usr/bin/env python3
"""
Author: Matthijs Smits 950103779040

Arguments:
1 - cluster path
2 - fasta output filename/location
"""


from sys import argv
import os
from shutil import copy

def migrate_regions (cluster_path):
    for hit_dir in os.listdir(cluster_path):
        if 'region' not in hit_dir and os.path.isdir('%s/%s'%(cluster_path,hit_dir)):
            for region in os.listdir('%s/%s'%(cluster_path,hit_dir)):
                if 'gbk' in region and 'region' in region:
                    copy('%s/%s/%s'%(cluster_path,hit_dir,region),'/mnt/scratch/smits083/antismash2/antismash/selected_regions')
    return None
    
    
if __name__=='__main__':
    migrate_regions('/mnt/scratch/smits083/antismash2/antismash')
    
                
