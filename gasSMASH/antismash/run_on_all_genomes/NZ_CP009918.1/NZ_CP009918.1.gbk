LOCUS       NZ_CP009918            10610 bp    DNA     circular CON 03-APR-2017
DEFINITION  Bacillus megaterium NBRC 15308 = ATCC 14581 plasmid pBMV_4, complete
            sequence.
ACCESSION   NZ_CP009918
VERSION     NZ_CP009918.1
DBLINK      BioProject: PRJNA224116
            BioSample: SAMN03174779
            Assembly: GCF_000832985.1
KEYWORDS    RefSeq.
SOURCE      Bacillus megaterium NBRC 15308 = ATCC 14581
  ORGANISM  Bacillus megaterium NBRC 15308 = ATCC 14581
            Bacteria; Firmicutes; Bacilli; Bacillales; Bacillaceae; Bacillus.
REFERENCE   1  (bases 1 to 10610)
  AUTHORS   Johnson,S.L., Daligault,H.E., Davenport,K.W., Jaissle,J., Frey,K.G.,
            Ladner,J.T., Broomall,S.M., Bishop-Lilly,K.A., Bruce,D.C.,
            Gibbons,H.S., Coyne,S.R., Lo,C.C., Meincke,L., Munk,A.C.,
            Koroleva,G.I., Rosenzweig,C.N., Palacios,G.F., Redden,C.L.,
            Minogue,T.D. and Chain,P.S.
  TITLE     Complete genome sequences for 35 biothreat assay-relevant bacillus
            species
  JOURNAL   Genome Announc 3 (2), e00151-15 (2015)
   PUBMED   25931591
  REMARK    Publication Status: Online-Only
REFERENCE   2  (bases 1 to 10610)
  AUTHORS   Bishop-Lilly,K.A., Broomall,S.M., Chain,P.S., Chertkov,O.,
            Coyne,S.R., Daligault,H.E., Davenport,K.W., Erkkila,T., Frey,K.G.,
            Gibbons,H.S., Gu,W., Jaissle,J., Johnson,S.L., Koroleva,G.I.,
            Ladner,J.T., Lo,C.-C., Minogue,T.D., Munk,C., Palacios,G.F.,
            Redden,C.L., Rosenzweig,C.N., Scholz,M.B., Teshima,H. and Xu,Y.
  TITLE     Direct Submission
  JOURNAL   Submitted (12-NOV-2014) Bioscience, Los Alamos National Laboratory,
            P.O. Box 1663, MS M888, Los Alamos, NM 87545, USA
COMMENT     ##Genome-Assembly-Data-START##
            Assembly Date                     :: 27-OCT-2014
            Assembly Method :: Newbler v. 2.6; Velvet v. 1.2.08; Allpaths
            Genome Coverage                   :: 450x
            Sequencing Technology             :: Illumina; PacBio; 454
            ##Genome-Assembly-Data-END##
            ##Genome-Annotation-Data-START##
            Annotation Provider               :: NCBI
            Annotation Date                   :: 04/03/2017 01:15:03
            Annotation Pipeline               :: NCBI Prokaryotic Genome
            Annotation Method                 :: Best-placed reference protein
            Annotation Software revision      :: 4.1
            Features Annotated                :: Gene; CDS; rRNA; tRNA; ncRNA;
            Genes (total)                     :: 5,952
            CDS (total)                       :: 5,779
            Genes (coding)                    :: 5,607
            CDS (coding)                      :: 5,607
            Genes (RNA)                       :: 173
            rRNAs                             :: 15, 13, 13 (5S, 16S, 23S)
            complete rRNAs                    :: 15, 13, 13 (5S, 16S, 23S)
            tRNAs                             :: 124
            ncRNAs                            :: 8
            Pseudo Genes (total)              :: 172
            Pseudo Genes (ambiguous residues) :: 0 of 172
            Pseudo Genes (frameshifted)       :: 70 of 172
            Pseudo Genes (incomplete)         :: 89 of 172
            Pseudo Genes (internal stop)      :: 40 of 172
            Pseudo Genes (multiple problems)  :: 25 of 172
            ##Genome-Annotation-Data-END##
            REFSEQ INFORMATION: The reference sequence was derived from
            CP009918.
            The DNA was obtained from USAMRIID.
            Annotation was added by the NCBI Prokaryotic Genome Annotation
            Pipeline (released 2013). Information about the Pipeline can be
            found here: https://www.ncbi.nlm.nih.gov/genome/annotation_prok/
            v. 44837; Phrap v. SPS - 4.24; HGAP v.
            2.2.0
            Annotation Pipeline
            set; GeneMarkS+
            repeat_region
            COMPLETENESS: full length.
            ##antiSMASH-Data-START##
            Version      :: 5.0.0-af0cea8(changed)
            Run date     :: 2019-06-07 15:32:13
            ##antiSMASH-Data-END##
FEATURES             Location/Qualifiers
     gene            complement(1..1125)
                     /db_xref="GeneID:29906912"
                     /locus_tag="BG04_RS29005_LOWER"
     CDS             complement(1..1125)
                     /codon_start=1
                     /db_xref="GeneID:29906912"
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_012209756.1"
                     /locus_tag="BG04_RS29005_LOWER"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /product="hypothetical protein"
                     /protein_id="WP_051975685.1"
                     /transl_table=11
                     /translation="MATIKLSTTKNANALLKYAEKRAEVSNSLNCDVDYVRNQFKATRE
                     IWGKNGGIQAHHVIQSFKPDEVDPQQANEIGLQLAEKLAKGHEVAVYTHTDKDHIHNHI
                     VINAVNYEDGRKFHAHGQEAIDHVRKISDELCKEHGLSIVEERSADVRYTLAEQSLFQK
                     GESSWKDEIRTAVDSSKEQATSFEDFQERLKDQGVQATLRGKNITYEHLESNKKVRGTK
                     LGLAYEKETILHGFERQVTRERETRNSATTVNRGFEENIRRDDFASQSDRGLSLDVPER
                     KYEQRDDDATRARTDQTDEPRRTSGNEIDFADIDQQIRDRHLRAQAIYERNFGRDEDGH
                     QGIEQRDEKKSGVSEQRVPNKQSSDQRRTREDQQQ"
     source          1..10610
                     /country="Japan"
                     /culture_collection="ATCC:14581"
                     /db_xref="taxon:1348623"
                     /isolation_source="Soil"
                     /mol_type="genomic DNA"
                     /note="type strain of Bacillus megaterium"
                     /organism="Bacillus megaterium NBRC 15308 = ATCC 14581"
                     /plasmid="pBMV_4"
                     /strain="ATCC 14581"
     gene            complement(1107..1460)
                     /db_xref="GeneID:29906909"
                     /locus_tag="BG04_RS00080"
                     /old_locus_tag="BG04_6012"
     CDS             complement(1107..1460)
                     /codon_start=1
                     /db_xref="GeneID:29906909"
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_012209755.1"
                     /locus_tag="BG04_RS00080"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /old_locus_tag="BG04_6012"
                     /product="plasmid mobilization relaxosome protein MobC"
                     /protein_id="WP_034656369.1"
                     /transl_table=11
                     /translation="MSETKGERRNEPKQVKFRVTEEEFERLSLMADNVGMTVPAFVKAK
                     AQGTRVRQPKINREGALAIAKELRAVGTNVNQIAKWCNARDIEQLNEQEFERLIYNLDE
                     IKKGLAEAWQQLS"
     gene            complement(1465..1668)
                     /db_xref="GeneID:29906910"
                     /locus_tag="BG04_RS00085"
     CDS             complement(1465..1668)
                     /codon_start=1
                     /db_xref="GeneID:29906910"
                     /inference="COORDINATES: ab initio prediction:GeneMarkS+"
                     /locus_tag="BG04_RS00085"
                     /note="Derived by automated computational analysis using
                     gene prediction method: GeneMarkS+."
                     /product="hypothetical protein"
                     /protein_id="WP_034656371.1"
                     /transl_table=11
                     /translation="MAGVDSVRGLAPPWKGGWGDCYAKHNQAQLGTNCRACLIPTVWCI
                     MEAYSWNFTPSSPRPVGEHKEE"
     gene            complement(2191..2376)
                     /db_xref="GeneID:29906913"
                     /locus_tag="BG04_RS00090"
                     /old_locus_tag="BG04_6013"
     CDS             complement(2191..2376)
                     /codon_start=1
                     /db_xref="GeneID:29906913"
                     /inference="COORDINATES: ab initio prediction:GeneMarkS+"
                     /locus_tag="BG04_RS00090"
                     /note="Derived by automated computational analysis using
                     gene prediction method: GeneMarkS+."
                     /old_locus_tag="BG04_6013"
                     /product="hypothetical protein"
                     /protein_id="WP_034656373.1"
                     /transl_table=11
                     /translation="MVKGKGLQWIGFLIALAGIFVNMITENSTVTVTMLIIGAIVLIIG
                     LFVSKKSKNGNIKKTV"
     gene            complement(2474..2971)
                     /db_xref="GeneID:29906911"
                     /locus_tag="BG04_RS00095"
     CDS             complement(2474..2971)
                     /codon_start=1
                     /db_xref="GeneID:29906911"
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_016766106.1"
                     /locus_tag="BG04_RS00095"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /product="HTH domain-containing protein"
                     /protein_id="WP_052098018.1"
                     /transl_table=11
                     /translation="MLVKKMLNGIMMKEITIKELADRYDVSTRTIQLKIKKLGYEWDSK
                     ESIYRYVGEESEPLDVDFSTLISKNSKMPAEQKLSTSEVAASTSYLDESGSTSTSSSKA
                     STVDAIDLLLQSPKDRSKRVYRGFYFDDDVLSIIDQVPKSYKSELVNEALRKVFKEKGL
                     LD"
     gene            complement(3422..3610)
                     /db_xref="GeneID:29906916"
                     /locus_tag="BG04_RS00100"
                     /old_locus_tag="BG04_6015"
     CDS             complement(3422..3610)
                     /codon_start=1
                     /db_xref="GeneID:29906916"
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_016766198.1"
                     /locus_tag="BG04_RS00100"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /old_locus_tag="BG04_6015"
                     /product="hypothetical protein"
                     /protein_id="WP_034656363.1"
                     /transl_table=11
                     /translation="MNKNEDINVFLLMLSVISICGGLLLTNVFIYSPLIGWLVAVFCLI
                     MNFIYKEEQLKYRKRED"
     gene            3930..4265
                     /db_xref="GeneID:29906908"
                     /locus_tag="BG04_RS00105"
                     /old_locus_tag="BG04_6017"
     CDS             3930..4265
                     /codon_start=1
                     /db_xref="GeneID:29906908"
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_016766747.1"
                     /locus_tag="BG04_RS00105"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /old_locus_tag="BG04_6017"
                     /product="membrane protein"
                     /protein_id="WP_034656365.1"
                     /transl_table=11
                     /translation="MKLRGNLNALIGVWFIIAPWAIGYSDQSGALWSSIVFGIIQVIVS
                     LWGYDKPGWNSWQNWISAITGVWFIIFPFIYSLTNGEVWSSVVLGLITTIFSLWNLGSN
                     SGSKAAG"
     gene            5848..6390
                     /db_xref="GeneID:29906905"
                     /locus_tag="BG04_RS00110"
                     /old_locus_tag="BG04_6018"
     CDS             5848..6390
                     /codon_start=1
                     /db_xref="GeneID:29906905"
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_012209763.1"
                     /locus_tag="BG04_RS00110"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /old_locus_tag="BG04_6018"
                     /product="hypothetical protein"
                     /protein_id="WP_051975683.1"
                     /transl_table=11
                     /translation="MNLKDWEKDLIGDTQRKKINKEVKETVTNDDGEKLFQRHIEESYV
                     SKEPDYVKIYLDNILQINNLSSGIQKTLNVLLKRMAYDNIVVLNAYIKKQMAEELGFNT
                     VQSLNNNINKLVKEGIMIRKGTGTYEMNPFLFGRGSWDNIKKIRFQVIFEEGKISQKAD
                     FDYKDEVAISHEEKTTD"
     gene            complement(6954..7502)
                     /db_xref="GeneID:29906907"
                     /locus_tag="BG04_RS00115"
                     /old_locus_tag="BG04_6019"
     CDS             complement(6954..7502)
                     /codon_start=1
                     /db_xref="GeneID:29906907"
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_001187397.1"
                     /locus_tag="BG04_RS00115"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /old_locus_tag="BG04_6019"
                     /product="DNA-binding protein"
                     /protein_id="WP_034656378.1"
                     /transl_table=11
                     /translation="MAKQVKVSAMTIKRNCQTYKDFINVKQGEKNKYLIDSSSLPVFLF
                     IAKMRNKRSLQSAQILELLEEEGFPKIYDEENINTSKQQTEQSLDKQGIVPVQDIESMV
                     AKRVQEELKKHEHFLSEWTLKQEEQQRKFQEGLLKRLDDRDQKLMSSIRELQESKLEIA
                     ASIEEPKKTLNKKWWEFWK"
     gene            complement(7963..8553)
                     /db_xref="GeneID:29906914"
                     /locus_tag="BG04_RS29010"
                     /old_locus_tag="BG04_6020"
     CDS             complement(7963..8553)
                     /codon_start=1
                     /db_xref="GeneID:29906914"
                     /inference="COORDINATES: ab initio prediction:GeneMarkS+"
                     /locus_tag="BG04_RS29010"
                     /note="Derived by automated computational analysis using
                     gene prediction method: GeneMarkS+."
                     /old_locus_tag="BG04_6020"
                     /product="hypothetical protein"
                     /protein_id="WP_051975684.1"
                     /transl_table=11
                     /translation="MKKILALIVALSAFVFLLPSNLVSAAEESPEVSTLSDNEIAFNLN
                     SNTPQEEDVYDDEGKYLGTMGIEPLSTSNSSETSGGFHTMGTYPVKEGTSTFKIYWKTG
                     VINLSYRINISRPKGLFTKSKITKAYDEWHLVTPPFTVNSDKLSRLRIQETSDKPAQAR
                     YRVNYGVIGQGSINYDLNSKVSKQYLYTSVSSF"
     gene            complement(9508..10167)
                     /db_xref="GeneID:29906906"
                     /locus_tag="BG04_RS00125"
                     /old_locus_tag="BG04_6022"
     CDS             complement(9508..10167)
                     /codon_start=1
                     /db_xref="GeneID:29906906"
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_003209759.1"
                     /locus_tag="BG04_RS00125"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /old_locus_tag="BG04_6022"
                     /product="hypothetical protein"
                     /protein_id="WP_034656367.1"
                     /transl_table=11
                     /translation="MKKALEEAEKKARIRDFKVEELINQAQVGLSTEQQRMLMEVLHNA
                     TGENYFIGKRKKKTDGVKFVQLIMDNVSYLNDIGYLKSAQEAFLFKLSPYVEFKSNVLI
                     EKVKQVDLDNDDLEIATNALTPTYLAKEFGVTRKHISTIMNSLLEKGILGVAEAGMTTE
                     DGRICTSRTWFVNPNIMCCSPKDGIDRATQHVFKKSLKNFVVEGSSKKHNLPVYLF"
     gene            complement(10391..10591)
                     /db_xref="GeneID:29906915"
                     /locus_tag="BG04_RS00130"
                     /old_locus_tag="BG04_6023"
     CDS             complement(10391..10591)
                     /codon_start=1
                     /db_xref="GeneID:29906915"
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_012209757.1"
                     /locus_tag="BG04_RS00130"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /old_locus_tag="BG04_6023"
                     /product="hypothetical protein"
                     /protein_id="WP_041907468.1"
                     /transl_table=11
                     /translation="MLSEKAKSELSEIEKNSKTMISNAKLGVVFNGWLDWIKYGAGAAV
                     FMVPVFFLFKWFFGLFGINLG"
     gene            complement(10518..10610)
                     /db_xref="GeneID:29906912"
                     /locus_tag="BG04_RS29005_UPPER"
     CDS             complement(10518..10610)
                     /codon_start=1
                     /db_xref="GeneID:29906912"
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_012209756.1"
                     /locus_tag="BG04_RS29005_UPPER"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /product="hypothetical protein"
                     /protein_id="WP_051975685.1"
                     /transl_table=11
                     /translation="SGEQVRHVEREGEIRTQRNREKQQDYDIER"
ORIGIN
        1 ttgttgttga tcttctcgag ttcgtctttg atcacttgat tgtttgtttg gaactcgttg
       61 ttcagaaact cctgattttt tttcatctcg ctgttcaata ccctgatggc cgtcttcatc
      121 tcggccaaag tttcgttcgt atattgcttg tgctctaaga tggcggtctc taatttgttg
      181 atcgatgtca gcgaagtcaa tctcatttcc gcttgtacgt cttggctcgt ctgtttgatc
      241 tgttcgagct cttgttgcat catcatctct ttgttcatat tttctttcag gaacgtcgag
      301 agataaccct ctgtcgctct gagacgcaaa atcatctctt cgtatattct cttcaaatcc
      361 tctgttgact gttgttgctg aatttcttgt ctctctttct cttgtaactt gtctttcaaa
      421 gccatgtaaa atcgtctcct tttcgtacgc caaccctaat ttagtgccac gtaccttttt
      481 attcgattct agatgctcat acgtgatgtt tttacctcgt agcgttgctt ggacgccttg
      541 gtcttttaaa cgttcctgaa agtcttcaaa actagttgct tgttcttttg aagagtcgac
      601 ggccgtccga atttcatctt tccaactcga ctcacctttt tgaaacaatg actgttcagc
      661 taatgtgtag cgaacatcgg ccgagcgctc ttctacaatg gacagcccat gttccttgca
      721 cagttcatca cttattttgc gcacatggtc aatagcttct tggccatgtg catgaaactt
      781 tcggccatcc tcatagttca cagcattaat gacaatgtgg ttatgaatgt gatccttatc
      841 ggtgtgcgta tagacggcca cctcgtgacc ttttgccaat ttctcagcta gttgtaaccc
      901 aatttcattg gcttgctggg gatccacttc atctggttta aatgactgga taacatgatg
      961 ggcttgtatg ccgccgttct tgccccatat ttcccttgta gccttaaatt ggttcctaac
     1021 gtagtccaca tcgcaattaa gcgaatttga cacctcagca cgcttctcag cgtacttcag
     1081 taacgcattg gcatttttcg tcgtactcag cttaattgtt gccatgcttc agctaatccc
     1141 tttttaattt catcgagatt gtatatcaat ctctcaaact cctgttcatt caattgctca
     1201 atgtctcgtg cattgcacca tttcgcaatc tggttcacat tggtacccac ggcacgcaat
     1261 tccttcgcaa ttgctagagc accctcacga ttgatcttcg gctgtcgtac gcgggtaccc
     1321 tgtgcctttg ctttcacaaa ggcaggcacc gtcattccta cgttatccgc catcaacgat
     1381 agccgttcga attcctcctc agtgacacgg aacttcacct gtttaggctc gttcctacgc
     1441 tcgcctttcg tctcactcat tcctttattc ctccttatgt tcacctactg gacgtggtga
     1501 acttggtgta aaattccagc tgtatgcttc cataatacac caaacagtcg gaataaggca
     1561 agctctacaa ttggtaccca attgagcttg gttatgcttt gcataacaat ccccccagcc
     1621 ccctttccaa gggggagcca aaccgcggac gctgtccaca cctgccaaag gaaagctcct
     1681 ttggaatggc ttctacaaac acccccaacc ccctcaaaac tttgaggggg ctccctcgcc
     1741 ggttggcgtc aaacaaacgt ggttgtttga gtgccaagag ggtttgggtg tagagacatt
     1801 gtcaattcct taatgctccc tacggtccgc ttaccattga caatgcaact tatctccccc
     1861 tttgttttgg ggtagaggct ggcttcttat gtttttaagt gcgttgctca tcttgaaata
     1921 agatgaacac tttaggacgg ctgatctagc ggttcggcag gtatactttc ctaaacaagc
     1981 cgatttccta actccttcgt tttgggtatg tcggcttggc gggagatcca aacctgccga
     2041 aattttttat gcaaggggaa aagagtggag attttttaag tgggtttctt aaaaaatacg
     2101 actcgtacct tgtaaaaaaa atagctatta aattatagaa aataaaataa gccaaaactg
     2161 aagttgaaaa acctcagctt tggctcttat ttaaactgtt ttctttatgt ttccattttt
     2221 acttttttta cttacaaata gtccaatgat tagtacaatg gctccaatga ttagcatagt
     2281 aacggtcact gtactatttt ccgtgatcat gtttacaaat atacctgcta aagcaattag
     2341 aaaccctatc cattgtaatc cttttccttt taccaagttg atcacctctt atagtatcaa
     2401 tttggaaata taatatattg ttaagtctaa atcattaact gagtgctttc aatctatatt
     2461 cctaaacaac tatttaatcc aataatcctt tttctttaaa tacttttctt agagcttcat
     2521 ttactaattc agacttataa cttttaggaa cttgatcaat aatgcttaac acatcatcat
     2581 caaaataaaa cccccgatag actctttttg aacggtcttt tggactttgc agcaaaaggt
     2641 ctattgcatc tactgtgcta gcttttgaag agctagtact cgtacttcca ctttcatcaa
     2701 gatagctcgt actagctgct acttcactag tagataactt ttgttctgct ggcattttac
     2761 tgtttttaga gataagagta ctaaaatcaa catctaaagg ttctgattct tccccaacat
     2821 agcgataaat gctttcctta ctgtcccatt catagcctaa tttcttaatc tttaattgaa
     2881 tcgtacgagt gcttacatca tatcgatcag ccaattcctt aatggtgatt tctttcatca
     2941 taatgccatt taacatcttt tttaccagca tcatactagc ctccaattcc tttttaaaag
     3001 tatagtacta gtattcaaat gcaagatgat atttcctgct ataacttctt ttaaaacaaa
     3061 cctatgtgtc agcaatgata caagatgttt cattttctat acataaatgt atcattccct
     3121 ctaaaacgta attatcatgt tatactgaca gacactgaca aaacattgta tatgcaatat
     3181 gttttgttca ttttctgcat tgaacgaaca aaaaccactg tttaggtcca gtggtttttg
     3241 tttttcagaa ttgaatatgt ttttattact tgagcaaact atacaagata ccgtaaggtg
     3301 tcattggata aagtcgtgag aaaaaccaac ctttaagtcc ttaaaggttg gtttttcctg
     3361 tcttatgtag ttaacataaa acacgttata agaatccata aaaaaagaga agatcctatt
     3421 cttaatcttc tctttttcta tatttcagtt gttcctcttt ataaataaaa ttcattatta
     3481 aacaaaatac tgctacaagc caaccaataa gaggggaata aataaaaaca tttgttaata
     3541 acaatccccc acaaatagag atcaccgaca acattaataa aaaaacgtta atatcttcat
     3601 ttttattcat ataaaacacc tctctaatta atcaaccaag tgaaaaaata aaagctttcc
     3661 atttcttaat aaatgtttcc aaagcaaaaa aggacatttc ttttatgccc tgatcccttg
     3721 tatcagcccc acctacgcta aatactatga cagccactca tgccatccaa cctatcttga
     3781 ttataaatga aagcgattac aaagtaaagg tattgtttaa atattctgtt tatttaattg
     3841 ttatatattg agttaatctc tatagtttct aaacatataa tctaacaatg gaatctctaa
     3901 agattcctaa acttagagga ggtgtaatta tgaaattaag gggtaattta aatgcattaa
     3961 ttggtgtctg gtttattatc gcaccttggg ctatcgggta ttcagatcaa tcaggagctt
     4021 tatggtcaag tatagttttt ggaatcattc aagttattgt atcgttatgg ggctatgaca
     4081 aacccggctg gaattcatgg caaaattgga tctccgcgat tactggcgtc tggtttataa
     4141 ttttcccatt tatctactca ttaacaaatg gagaggtttg gtcaagcgta gttcttggtc
     4201 taatcacgac tatatttagt ttatggaact taggatctaa ttcgggttca aaggctgctg
     4261 gttaaaaata aaacggatag gaatcgacta ttcgagcaaa ctacctaaga cgccgtgagg
     4321 catctatcct taaaacaaga gcttacatct caaaaaagag aaaacctccc ttattcgcat
     4381 gaggaggttt tctctttcta cgctgtatat catatacaat cctagttaac ataatggatt
     4441 ttttcggaac acattaaata tctatacaag aggatattgc aaatctgcta tgtaaaggtt
     4501 gcatagttgc tgcaaccttt gagcaaacta ctcaagatga gtagagtccc ttcgttacag
     4561 cttctactcc attacaccta acaaactacc tcgtcacctg tctaaacggc ttaggggtag
     4621 tttgtttttt tgaaagaaac aagtcataga actttaaaga caagcatata atctaaaaaa
     4681 gagccccacg tgctcatgaa ttgaactagt agataaccca tgaatacaat gtattcatgg
     4741 gttatttgct ttttatatga attagaacat tataaccagg gcaaactacc cgagatgtat
     4801 taaacatcaa aataaacctt tagggcaagg tttagaaaaa agactactgt ataggaaaca
     4861 gtggtctttt tctgttttta ctctgtatac catatataaa ttcagttaac ataatgtact
     4921 ttacaggaac cataagttaa aaaatatatt cactttcata tataaaacaa tcaaatacta
     4981 agatcatttt tgtagccatg atctccacca acgtaaacat tgccaacgcc accatcgacg
     5041 aatgagcata tattttcact cctatatatt tatactttca aaatatgtac aaggagtaaa
     5101 aaggttcatc ttgaaattag agactttcat atattaatga actatatcaa aaaagccctg
     5161 ggatcaatga tccaaaggtt tttgagatag cctatgagag tgggaaaact aatgttgtat
     5221 gattttatca gtttcagtat attcaccttc tttgttttaa gtaacttgcc tattcagaat
     5281 aaagatttta tatgatgagc aaactactca agatgcctga acggcgtctt ccactgcaac
     5341 cttatgaaaa agaaagctgt cctccctgac agctttcttt ttttttgatt atttaagaat
     5401 aaaattcatt agtttaagca aactacccaa gacgatgcaa agcgtcaatt acaaatctta
     5461 tatgtattca tacagaaaaa ctcccttaac gcattcaggg agtttttctc tttttaagct
     5521 gtataggata tacatcttta gttaacataa tacctctttt cggaacccaa caaaacaaca
     5581 caaaaaacat gtacataaaa taaaagaaaa gaagagataa aactctataa acattgatat
     5641 aacaacattt ctttgaatta actataatcg tatacgttat aaactataat cgtatttatt
     5701 ataaattata atcacatacg ttacagatca taattatatt tgttataatt tataatctga
     5761 aatagattat aaattataac aaaatacctt gaaattatct attttttata atacgataca
     5821 gattataaat tataatataa aagaggtgtg aatttgaaag attgggaaaa ggacttaatt
     5881 ggagatacac aaagaaaaaa aatcaataaa gaagttaaag aaacagttac taatgatgat
     5941 ggagaaaagc tatttcaaag acatatcgaa gaatcatacg tttccaaaga acctgattac
     6001 gtaaaaattt atttagataa tatcttacag ataaataact tatcaagtgg tattcagaag
     6061 acattaaatg ttcttctaaa aagaatggcc tatgataaca tcgttgtttt aaatgcatac
     6121 ataaaaaaac aaatggctga agaattgggg tttaatacag ttcaaagttt aaataacaac
     6181 atcaataagc ttgttaaaga agggattatg attagaaaag gaactggaac ttatgaaatg
     6241 aatccatttc tatttggtag aggatcttgg gacaatatta agaagattag attccaagta
     6301 atctttgaag aagggaaaat cagtcaaaaa gcagattttg actataaaga tgaagttgct
     6361 atatctcatg aagaaaaaac tactgattag acataagttc ttacgaactt gagcaaacta
     6421 ctcaagatgc ctgaacggcg tcttccactg caaccttatg aaaaagaaag ctgtcctccc
     6481 tgacagcttt cttttttttt aattatttaa gaataaaatt cattagttta agcaaattac
     6541 ccaagacgat gcaaagcgtc aattacaaac cttatatgta ttcacacaga aaaactccct
     6601 taacgcattg ggggagtttt tctgttttca cgctgtatag gatatacaaa ggtagttaac
     6661 ataatacctc tttccggaac acaccaccaa aaagaaccct tgtcgcagaa cggttccgcc
     6721 gagggttctt tttccggagt atgcagcttt ttatacatag ttgataaatc aacgttttct
     6781 cagtaggtgg agaagaaact aatgtataag aagtgcacct tttattcata tgctgacgag
     6841 ctaaaaaata gcattaatac ccaatatatg ttatattttc aataattact tgaaatttca
     6901 acgtatttga aagcaacctc actctcctat gacaaaaatg aggttgctct tttttatttc
     6961 caaaattccc accacttttt attgagagtt tttttaggtt cttcaataga tgctgctatc
     7021 tctaatttag actcttgtaa ttctctaata gagctcatca gcttttgatc tcgatcatct
     7081 aaccttttaa gtaatccttc ttgaaatttc ctctgttgtt cttcttgttt aagtgtccat
     7141 tcacttaaga aatgctcatg ctttttcaat tcctcctgga cccttttagc aaccattgat
     7201 tctatgtcct gaacaggaac aatcccttgc ttatctaagg attgttcagt ttgttgttta
     7261 gaagtgttta tgttttcttc atcataaatt tttgggaaac cttcttcttc taatagctct
     7321 agtatctgag cagattgtag acttcgtttg tttctcattt tagctataaa tagaaaaacc
     7381 ggtaacgaag atgaatcaat taaatacttg tttttttcac cttgcttaac atttataaaa
     7441 tctttataag tttgacaatt acgttttata gtcatggctg atacttttac ttgtttagcc
     7501 atatcttcag ctgataacca ctgtttagaa gtcttcactg ttcagatcac cgcctttaaa
     7561 tttaactaaa catctaaact gaacaccatt caaacaatta gataattcca tgctacatct
     7621 actttaacct ctttttctgt tcagatcatt gttcagatct ttttaaaaaa taataatcta
     7681 aacaatgatc tgaacacacc taaagaatga aaaaccacgc taaaaattct atttaaaaac
     7741 tgaaggccgc ctttaggcgg ttcgatagcc tttaggcata ttaataaaaa aataatagta
     7801 tcatatctta tagtggccgc tttttattag acaagaaaat cttgcgattt aattataaaa
     7861 atttaaacga aaaaattaca aaaagacctt tgcaattcta tgagcgaatt gcaaaggtct
     7921 ttttgtatta ttttctaaat tttacttata ataaattact aattagaagc ttgaaacaga
     7981 tgtatataga tattgcttac ttacttttga atttaaatca taatttatag atccttgtcc
     8041 aataacacca tagtttactc tatatctagc ttgagctggc ttatctgatg tttcttgaat
     8101 tctaagccta gataatttat cagagtttac agtaaaaggt ggtgttacca aatgccattc
     8161 atcataagct ttagttattt tggactttgt aaatagacct ttaggtctgc ttatattaat
     8221 acgatagctt aaatttatta caccagtctt ccaatatatt ttaaatgtac tagtaccttc
     8281 tttaactgga taggtaccca tagtatggaa accgccacta gtttcagaag aatttgaagt
     8341 cgataatggt tcaataccca tagtacctag atatttacct tcatcatcgt aaacatcttc
     8401 ttcttgtgga gtattagagt ttaaattaaa agcaatttca ttatctgata aagttgatac
     8461 ttctggactt tcttctgctg cactgactaa atttgaaggt aatagaaata caaacgcaga
     8521 taaagcaacg attaaagcta aaattttctt caaaactttc actctcccta ttaatctttt
     8581 aaaacaataa ctataataac acaaaaaata caataagtta caaaaaatga taaaaaatgt
     8641 tataattact ttaatataga aactatgtaa aattactctt gttgcctcga gtacttttaa
     8701 agtattatta aatgtgtata taaaattaga aaggtgaatg taatatattg attttattat
     8761 tttcagcatt atgtatatca atacttttaa atatttactt atataaacaa tttagaaaag
     8821 agaaaaagaa ttcttattat gaaagcagtt ggaaagaaaa gttaaatctt aataataaga
     8881 taaatgcgtt aatatgggct tggttaacaa ttattttatt tttatgtctt gtcggattgt
     8941 tttctggtat agattcataa ataaaaagga gtatgaataa tatggatata ttacaaacaa
     9001 tttttacaac tttaatacca tttataataa taattgcatt agcatatcct atttggaaat
     9061 ttgtatctat tctattgaca ataaaaaata attcatcaaa aaaagttgaa caaaatgaaa
     9121 agattatcaa tttattggag agtaaaaata tataacttcc aacaaatcaa ttcataactt
     9181 aaggtgtgac aaaaaggtca caccttaagt tataacattg aaagaaatac aagagtcaca
     9241 tagttaagtt aaaccactca attaatttga gtggttttta tatgttgtaa agctgtattt
     9301 ttttctaacg aaaaaataaa aaaagatcct tgcaattcta tgaacgaatt gctatttaaa
     9361 tagagtgaga aggggcatat actaaaagtg agagttatca agggtttgaa gcacctatag
     9421 tgtaaccata tggttacacc tgtgtaacca tatggttaca cctaataaat ggattttaaa
     9481 taaaaaaacc tcaaaaaata ttgaggttta aaataaataa acaggtaaat tgtgtttctt
     9541 agatgaacct tctacaacga agttctttaa agacttttta aaaacatgtt gagttgctct
     9601 atctatacca tctttaggag agcaacacat aatattaggg tttacaaacc aagtgcgact
     9661 tgtacaaatt cgtccatctt ctgtagtcat tcctgcttca gctactccta atattccttt
     9721 ttctaaaaga ctgttcatta tagtcgatat gtgtttcctt gttacaccaa attcttttgc
     9781 tagatatgta ggtgtcaaag cattagttgc tatttctaaa tcatcattat ctagatctac
     9841 ttgctttacc ttttctataa ggacgtttga tttaaattca acataaggac ttaatttaaa
     9901 aagaaaagcc tcttgagcag attttaaata tcctatatcg ttcaaatagc ttacattatc
     9961 cataattaat tgcacaaact tcaccccatc agtttttttc tttcgtttac cgatgaaata
    10021 gttttctcca gtagcattat gcaagacttc cattaacatt ctttgttgtt ctgtagacaa
    10081 accaacctga gcttgattaa ttaactcttc tactttgaaa tctctaattc ttgctttctt
    10141 ttctgcttct tctaatgctt ttttcaccgt aactcctcca tttagtgtgg aaggaattaa
    10201 ccttgctgtg ttagaataag attataataa tgtgtttttg aaggttaatt ccttctttat
    10261 tttctttgaa aagtcctctg agtcggccaa aacttgaggg cttttcttat ttttattgta
    10321 caggtaaaat taccgaaatt tctaccattt cttttgattt tctactttta aactaatatt
    10381 tccccatacc ttaacctaga ttaataccaa ataacccaaa gaaccattta aacaagaaga
    10441 aaacaggtac cataaagacc gctgcaccag ccccgtattt tatccaatcc aaccaaccat
    10501 tgaacactac acctaactta gcgttcgata tcatagtctt gctgtttttc tcgatttcgc
    10561 tgagttctga tttcgccttc tcgctcaaca tgtcgaactt gctccccaga
//
