LOCUS       NC_010009               9098 bp    DNA     circular CON 30-MAR-2017
DEFINITION  Bacillus megaterium QM B1551 plasmid pBM200, complete sequence.
ACCESSION   NC_010009
VERSION     NC_010009.2
DBLINK      BioProject: PRJNA224116
            BioSample: SAMN02603354
            Assembly: GCF_000025825.1
KEYWORDS    RefSeq.
SOURCE      Bacillus megaterium QM B1551
  ORGANISM  Bacillus megaterium QM B1551
            Bacteria; Firmicutes; Bacilli; Bacillales; Bacillaceae; Bacillus.
REFERENCE   1  (bases 1 to 9098)
  AUTHORS   Eppinger,M., Bunk,B., Johns,M.A., Edirisinghe,J.N., Kutumbaka,K.K.,
            Koenig,S.S., Creasy,H.H., Rosovitz,M.J., Riley,D.R., Daugherty,S.,
            Martin,M., Elbourne,L.D., Paulsen,I., Biedendieck,R., Braun,C.,
            Grayburn,S., Dhingra,S., Lukyanchuk,V., Ball,B., Ul-Qamar,R.,
            Seibel,J., Bremer,E., Jahn,D., Ravel,J. and Vary,P.S.
  TITLE     Genome sequences of the biotechnologically important Bacillus
            megaterium strains QM B1551 and DSM319
  JOURNAL   J. Bacteriol. 193 (16), 4199-4213 (2011)
   PUBMED   21705586
REFERENCE   2  (bases 1 to 9098)
  AUTHORS   Kunnimalaiyaan,M., Zhou,Y., Scholle,M., Baisa,G.A. and Vary,P.S.
  TITLE     Discoveries within the Seven Plasmid Array of Bacillus megaterium QM
            B1551
  JOURNAL   Plasmid 45, 142-142 (2001)
REFERENCE   3  (bases 1 to 9098)
  AUTHORS   Stevenson,D.M., Zhou,Y., Mueller,K., Jablonski,L. and Vary,P.S.
  TITLE     Replicons of the Indigenous Plasmids of Bacillus megaterium QM B1551
  JOURNAL   Plasmid 37, 232-232 (1997)
REFERENCE   4  (bases 1 to 9098)
  AUTHORS   Eppinger,M., Bunk,B., Johns,M.A., Edirisinghe,J.N., Kutumbaka,K.K.,
            Riley,D.R., Huot-Creasy,H., Koenig,S.S.K., Galens,K., Orvis,J.,
            Creasy,T., Biedendieck,R., Braun,C., Grayburn,S., Jahn,D., Vary,P.S.
            and Ravel,J.
  TITLE     Direct Submission
  JOURNAL   Submitted (05-FEB-2010) Institute for Genome Sciences, University of
            Maryland School of Medicine, 801 West Baltimore St, Baltimore, MD
            21201, USA
REFERENCE   5  (bases 1 to 9098)
  AUTHORS   Baisa,G.A., Kunnimalaiyaan,M., Edirisinghe,J.N. and Vary,P.S.
  TITLE     Direct Submission
  JOURNAL   Submitted (07-MAR-2006) Biological Sciences, Northern Illinois
            University, Montgomery 306, DeKalb, IL 60115, USA
COMMENT     ##Genome-Annotation-Data-START##
            Annotation Provider               :: NCBI
            Annotation Date                   :: 03/29/2017 23:26:47
            Annotation Pipeline               :: NCBI Prokaryotic Genome
            Annotation Method                 :: Best-placed reference protein
            Annotation Software revision      :: 4.1
            Features Annotated                :: Gene; CDS; rRNA; tRNA; ncRNA;
            Genes (total)                     :: 5,674
            CDS (total)                       :: 5,492
            Genes (coding)                    :: 5,379
            CDS (coding)                      :: 5,379
            Genes (RNA)                       :: 182
            rRNAs                             :: 13, 12, 12 (5S, 16S, 23S)
            complete rRNAs                    :: 13, 12, 12 (5S, 16S, 23S)
            tRNAs                             :: 137
            ncRNAs                            :: 8
            Pseudo Genes (total)              :: 113
            Pseudo Genes (ambiguous residues) :: 0 of 113
            Pseudo Genes (frameshifted)       :: 62 of 113
            Pseudo Genes (incomplete)         :: 38 of 113
            Pseudo Genes (internal stop)      :: 32 of 113
            Pseudo Genes (multiple problems)  :: 16 of 113
            ##Genome-Annotation-Data-END##
            REFSEQ INFORMATION: The reference sequence was derived from
            CP001985.
            On Apr 14, 2010 this sequence version replaced NC_010009.1.
            Bacteria available from the Bacillus Genetic Stock Center,
            (http://www.bgsc.org/), The Ohio State University, 776 Biological
            Sciences Building, 484 West 12th Avenue, Columbus. Ohio 43210 under
            inventory No. 7A116 or from Patricia S. Vary (pvary@niu.edu), Dept.
            of Biological Sciences, Northern Illinois University, DeKalb, IL
            60115.
            Annotation was added by the NCBI Prokaryotic Genome Annotation
            Pipeline (released 2013). Information about the Pipeline can be
            found here: https://www.ncbi.nlm.nih.gov/genome/annotation_prok/
            Annotation Pipeline
            set; GeneMarkS+
            repeat_region
            COMPLETENESS: full length.
            ##antiSMASH-Data-START##
            Version      :: 5.0.0-af0cea8(changed)
            Run date     :: 2019-06-07 13:26:50
            ##antiSMASH-Data-END##
FEATURES             Location/Qualifiers
     source          1..9098
                     /db_xref="taxon:545693"
                     /mol_type="genomic DNA"
                     /organism="Bacillus megaterium QM B1551"
                     /plasmid="pBM200"
                     /strain="QM B1551"
     gene            913..1062
                     /locus_tag="BMQ_RS25640"
     CDS             913..1062
                     /codon_start=1
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_013057978.1"
                     /locus_tag="BMQ_RS25640"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /product="YjcZ family sporulation protein"
                     /protein_id="WP_041816876.1"
                     /transl_table=11
                     /translation="MGFGGCGYGGYGYGGGGSGFGSGFALIIVLFILLIIIGASCFGGF
                     GGGC"
     gene            1814..2167
                     /locus_tag="BMQ_RS25645"
                     /old_locus_tag="BMQ_pBM20004"
     CDS             1814..2167
                     /codon_start=1
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_012209755.1"
                     /locus_tag="BMQ_RS25645"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /old_locus_tag="BMQ_pBM20004"
                     /product="MobC family plasmid mobilization relaxosome
                     protein"
                     /protein_id="WP_012209755.1"
                     /transl_table=11
                     /translation="MSETKGERRNEPKQVKFRVTEEEFERLSLMADNVGMTVPAFVKAK
                     AQGTRVRQPKINREGALAIAKELRAVGTNVNQIARWCNARDIEQLSEQELERLVYNLDE
                     IKKGLAAAWQQLS"
     gene            2149..3375
                     /locus_tag="BMQ_RS27750"
                     /old_locus_tag="BMQ_pBM20005"
     CDS             2149..3375
                     /codon_start=1
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_012209756.1"
                     /locus_tag="BMQ_RS27750"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /old_locus_tag="BMQ_pBM20005"
                     /product="relaxase/mobilization nuclease domain-containing
                     protein"
                     /protein_id="WP_012209756.1"
                     /transl_table=11
                     /translation="MATIKLSTTKNANALLKYAEKRAEVSNSLDCDVDYVRNQFKATRE
                     IWGKNGGIQAHHVIQSFKPDEVDSHQANEIGLQLAEKLAKGYEVAVYTHTDKDHIHNHI
                     VINAVNYEDGRKFHAHGQETIDRFREASDELCKENGLSIVEERSADVRYTLAEQSLLEK
                     GESSWKDEIRTAIDQAKEQTASFEEFQEHLKEQGVQATLRGKNITYEHLESNKKVRGMK
                     LGLAYEKETILHGFERQVTRERETRNSAATAERGDTTVTAVISGDDLTSQNDRGLSLDV
                     PERKHEQRDDDRTRARTDQTDEPRRAAGNELNFDDIAQQIRERHRNTQAVYKRNFGRDE
                     DGHQSATERSEKEPGLSEQRVPSKQSSDQGRAREDQQQPGEQVRQLERDSEGRTQRGRK
                     KQQDYDLER"
     gene            3750..4424
                     /locus_tag="BMQ_RS25655"
                     /old_locus_tag="BMQ_pBM20007"
     CDS             3750..4424
                     /codon_start=1
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_001001385.1"
                     /locus_tag="BMQ_RS25655"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /old_locus_tag="BMQ_pBM20007"
                     /product="hypothetical protein"
                     /protein_id="WP_012209758.1"
                     /transl_table=11
                     /translation="MNLEKKLAEAEKKARLKDFKNKSYSSEEVSDIQRSLLDLMQEITG
                     EKHFIGKEKDIFAGEPFNQTMHRNLDLLTRINYLTTAEESFLFRIQAYLEFKSNVIICR
                     EDKFKKKRNNVVDDDYELPKAATVSEIAEMIGKSREKTSLIMNSLKKKEILLNPEGAGQ
                     VSENGRSVSPRTWIVNPYIMICAPRKNEVKLDKLTMRLFQHSLKNLKDPNGKKVNLPVR
                     FF"
     gene            complement(4755..5339)
                     /locus_tag="BMQ_RS25660"
                     /old_locus_tag="BMQ_pBM20008"
     CDS             complement(4755..5339)
                     /codon_start=1
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_012209759.1"
                     /locus_tag="BMQ_RS25660"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /old_locus_tag="BMQ_pBM20008"
                     /product="DUF3967 domain-containing protein"
                     /protein_id="WP_012209759.1"
                     /transl_table=11
                     /translation="MEVITNMYMEFYEKVEGLARAIGVTTSTVKKYYLLFEEYGYNFKR
                     NQQGQLLFSQDDINLFKELIVLKNEKGMTVPKAVKEIIKDKVMTDTTDITDITATTEDI
                     TVISKHVTTVMSELDELKQLVKNQNEIIEKQQEDFKKQFLKQEAYIKDSLEQRDKTLIT
                     TLREVQESQKEMATSLSKDQAKKKWWQIWLD"
     gene            complement(5471..5674)
                     /locus_tag="BMQ_RS25665"
     CDS             complement(5471..5674)
                     /codon_start=1
                     /inference="COORDINATES: ab initio prediction:GeneMarkS+"
                     /locus_tag="BMQ_RS25665"
                     /note="Derived by automated computational analysis using
                     gene prediction method: GeneMarkS+."
                     /product="hypothetical protein"
                     /protein_id="WP_041816878.1"
                     /transl_table=11
                     /translation="MLGNSRTLKIHVLTTTLDKYEERLRDFLQNNNFEIIDIKICNVGP
                     DISVLIIYRDLVQHPFNFKKSL"
     gene            complement(6404..6997)
                     /locus_tag="BMQ_RS25670"
                     /old_locus_tag="BMQ_pBM20009"
     CDS             complement(6404..6997)
                     /codon_start=1
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_012209760.1"
                     /locus_tag="BMQ_RS25670"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /old_locus_tag="BMQ_pBM20009"
                     /product="hypothetical protein"
                     /protein_id="WP_012209760.1"
                     /transl_table=11
                     /translation="MKMGNCKKQNRKRQELDINELFKRITFETKQSQRTFKEIENLLNR
                     SQQLMRKLLTNSNNILTSENAFFNVAGAPAAPVAPPTPPTPTPLPPITQTQQTLIINLL
                     STVRDIRSVSRQIRDTYEALIGGVLLLRRRAIERSEAFDEDQFDRADDLTRSIEDITLG
                     ILILESQLENLKTQLEGLQENLEIYIALLANTVL"
     gene            complement(7574..8116)
                     /locus_tag="BMQ_RS25675"
                     /old_locus_tag="BMQ_pBM20012"
     CDS             complement(7574..8116)
                     /codon_start=1
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_012209763.1"
                     /locus_tag="BMQ_RS25675"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /old_locus_tag="BMQ_pBM20012"
                     /product="hypothetical protein"
                     /protein_id="WP_012209763.1"
                     /transl_table=11
                     /translation="MNLKDWEKDLIGDTQRKKINKEVKETVTNDDGEKLFQRHIEESYV
                     SKEPDYVKIYLDNILQINNLSSGIQKTLNVLLKRMAYDNIVVLNAYIKKQMAEELGFNT
                     VQSLNNNINKLVKEGIMIRKGTGTYEMNPFLFGRGSWDNIRKIRFQVIFEEGKISQKAD
                     FDYKDEVAISHEEKTTD"
ORIGIN
        1 ggatccgtca gtagtttttt ctctgattta tagaaagaga cagaatggtt agtcgcataa
       61 tttatgatat gtaaagtaat tacaagtgtt tggacaagtg ctgatattat aaaaatttta
      121 ttaactttga ttgatatagt aagagtacat aacttaaaaa cagaaaaaga ctacttcttt
      181 ccagtcaaaa gaagtagtct ttttctgtta cgtaattggg tttcatgagt gatgcctcat
      241 tgcatcttgg gtagtttgct caatttatca ataacttatg tattaaaaga aaaacccacc
      301 tgatttgtca gaggtggatt tttcttttag aaaatctaca tatgattagt ttactctaac
      361 tattgtcttt tattcctaat aggcaagtta cctaaaggaa aataagggca tatgatgtat
      421 ttcgaataat tttcgtctta cacttagaat ccttttagtt tcatttttca ccttcataat
      481 ttacaaagtc aaaaaaaaga aaaaacaatc tgtattggaa gacagattgt tttttctttt
      541 aaatgagaga tggctttatc gaagtcgcat tcctagagcc ttctctaata atattcaaaa
      601 tcttttcaga taatgaattc caataaaatc cattatacta actaaacata tatataatat
      661 acatctaaaa aaacagaaaa actcccaggc ggcagggagt ttttctgttt tttaacaact
      721 gcaaatccgt tcgggagggt aactgatcta ctgggattca atgttatggt tcattcattc
      781 tatgtaaaat aatctaaatg gtttatatat atctacaaca atcctattat ttttaccagt
      841 ccagagtata ggcgagcatt ttaatttttt atgcatatct tagaagtatt aagcatatac
      901 gggggtgaaa atatgggctt tggcggttgc ggttatggcg gatatggtta cggcggaggc
      961 ggcagtggtt ttggcagtgg ttttgcttta attattgtcc tctttattct attaattatt
     1021 atcggggctt cttgtttcgg cggctttggt ggaggatgct aatcattaaa tctaatttgt
     1081 ttaaaaaaag ttagctagat gctagctttt tttatttttg gatgaaaatt cattttcaat
     1141 caatgggggc tagcctttat acaaggtacg agtcgtattt ttcaagaaac ccacttaaaa
     1201 aatctccact cttttctcct tgtataaaga ctttcggcag gtttgaatct ctcgccaagc
     1261 cgacataccc aaaacgaagg atatgggaat cggcttgttt aagcaagtat acctgccgaa
     1321 ccgccagctc ggccgtacaa aagtgctcat cttactccaa ggtaagcaac gcacttaaaa
     1381 acataagaag ccagcctcta ccccaaaaca aagggggaga taagctgcgt tgtcaatggt
     1441 aagcggaccg tagggagcat taaggaattg acaatgtcac tacaccaaag accctcttgg
     1501 cactcaaaca aacgagtttg tttgacgcca accggcgagg gagccccctc aaagttttga
     1561 gggggttggg ggtgtttgta caagccattc caaaggagct ttcctttggc aggtgtggac
     1621 agcgtccgcg gtttggctcc cccttgaaaa gggggctggg gggattgtta tgcctagcat
     1681 aaccaagctc aattgggtac caattgtaga gcttgcctta ttccgactgt ttggtgtatt
     1741 atggaagcat acagctggaa ttttacacca agttcaccac gttcagtagg tgaacataag
     1801 gaggaataaa gggatgagcg agacgaaagg cgagcgtagg aacgagccta aacaggtgaa
     1861 gttccgtgtt actgaagagg aattcgaacg gctatcgttg atggcggata acgtaggaat
     1921 gacggtgcct gcctttgtga aagcaaaggc acagggtacc cgtgtacgac agccgaagat
     1981 caatcgtgag ggtgctctgg ccatcgcgaa ggaattgcgt gctgtgggta ccaatgtgaa
     2041 ccagattgcg agatggtgca atgcacgaga cattgaacaa ttaagtgaac aggagcttga
     2101 gagattggta tacaatctcg acgaaattaa aaagggattg gctgcagcat ggcaacaatt
     2161 aagctgagta cgacgaaaaa tgccaatgcg ttactgaagt acgctgagaa gcgcgctgag
     2221 gtgtcaaatt cacttgattg tgatgtggac tatgtaagaa accaatttaa agctacaagg
     2281 gaaatatggg gcaagaacgg cggtatacaa gcccatcatg tcattcagtc gtttaaacca
     2341 gatgaagtgg attcccacca agccaatgaa attggattac aactggctga gaagttggca
     2401 aaaggttatg aggtggccgt ctatacgcac acggataaag atcatatcca taaccacatc
     2461 gtgattaatg cggtgaacta tgaagatggc cggaagtttc atgcccatgg ccaagagacg
     2521 attgaccgtt ttcgcgaagc aagtgatgaa ctgtgtaagg aaaatggcct ttccattgta
     2581 gaagagcgct cggcggatgt tcgctacaca ttagctgaac agtcattact tgaaaaaggt
     2641 gagtctagtt ggaaggatga aattcggacg gccatcgatc aagcgaagga gcagacagct
     2701 agttttgaag agtttcagga acatttaaaa gagcagggcg tccaagcaac tttaagaggc
     2761 aaaaacatca cgtatgagca tctagaatcg aataaaaagg tacgtggcat gaaattaggg
     2821 ttggcgtacg aaaaggagac gattttacat ggctttgaaa gacaagttac aagagaaaga
     2881 gagacaagaa attcagcagc cacagcagaa agaggagata caacagttac agcagttata
     2941 tcaggagatg atcttacgtc tcagaacgac agagggctat ctctcgacgt tcctgaaaga
     3001 aaacatgaac aaagagatga tgatcgaaca agagctcgaa cagatcaaac agacgagcca
     3061 agacgtgcag caggaaatga gcttaacttc gatgacattg ctcagcaaat tagagaacga
     3121 catcgtaaca cacaagcagt atacaaacga aactttggcc gagatgaaga cggccatcag
     3181 agtgctacag aacgatctga aaaagaacca ggactttctg agcaacgagt tccaagcaaa
     3241 caatcaagtg atcaaggacg agctagagaa gatcaacaac aacctgggga gcaagttcga
     3301 cagcttgagc gcgacagcga aggcagaact cagcgaggtc gaaaaaaaca gcaagactat
     3361 gatctcgaac gctaagttag gggttgtgtt taatggctgg ttagattggg tgaaatacgg
     3421 agctggcgca gctgtcttta tggtacctat gtttttcttg ttcaaatggt tctttggttt
     3481 atttggtatt aatctaggat aatagataag gcttttataa aattagttga aaaactgcga
     3541 aaagagtaga aaaaaaggga aattattgat agtgttgaat gtagtataaa tgagaaaaac
     3601 cgcccatgct ttggtcggca ggcggtttct caaaaacaga aagggaatgg gtcccttata
     3661 tacatattta tgtacatatt ctatcataag gggctggttc ctgccatata aggagaggaa
     3721 ttagtttgga aaacaacaaa aataatattg tgaatttgga aaaaaaactt gctgaagcgg
     3781 aaaaaaaggc aagattaaaa gatttcaaaa ataaatcata ttcatcagaa gaagtctctg
     3841 atattcaaag atcattatta gatctaatgc aggaaattac aggtgaaaaa cattttatcg
     3901 gtaaggaaaa ggatattttt gcaggggagc cgtttaatca aactatgcat agaaatttag
     3961 atttgttgac tagaataaat tatttaacaa ctgctgagga aagctttttg tttagaattc
     4021 aagcttattt agagtttaaa agtaatgtta ttatttgcag agaagacaaa tttaaaaaga
     4081 aacgtaataa tgttgtagat gatgattatg agcttcctaa ggcagcaaca gtttcagaaa
     4141 ttgcagaaat gattggtaag tctagggaaa aaacttcttt aattatgaat tctttaaaga
     4201 aaaaagagat tttattaaat cctgaaggtg caggacaagt tagtgaaaat ggtcgttctg
     4261 taagccctag aacgtggatt gtaaatcctt atattatgat ttgtgctccg cgtaaaaacg
     4321 aagttaagtt agataaatta acaatgagat tatttcagca ttctttaaaa aaccttaaag
     4381 atccgaatgg aaaaaaagta aatttgccag ttagattttt ttaataatga ttttagatcg
     4441 tatcaaaaat gatacggtct tttttaagca tttggatttt tttaggtgtg acgaaaaggt
     4501 cacaaaacta ccctaaagtg tgacgaaaag gtcacacccc aaaaaccttg acaaacccag
     4561 tcataccaag ggctcatcgg ggtttgttaa ctttctctct attatcacta ttatttatta
     4621 gcaattcgct gatagaattg caaggatctt tttatacttt ttttcgttta aaaaaagtag
     4681 cctaaaggct aaaaaaaccg tctcaagacg gtcatctttt aatttaaaaa aactactcaa
     4741 tcaatttgag tagtttaatc tagccatatt tgccaccatt tctttttagc ttgatctttt
     4801 gacaagctag tagccatttc tttttgtgat tcttgaactt ccctcaatgt tgtaattaat
     4861 gtcttatctc gttgttctaa actatctttt atgtatgcct cttgttttag aaattgtttc
     4921 ttaaagtcct cttgctgttt ttctataatc tcattttgat tttttactaa ctgttttaac
     4981 tcgtctaatt cagacataac ggttgtgacg tgtttcgata taaccgttat gtcttctgtt
     5041 gtagcagtta tatccgttat atcggttgtg tccgtcataa ctttatcttt tataatttct
     5101 tttacagcct tgggaacagt catgcctttt tcatttttta aaacgattaa ttctttaaat
     5161 aaatttatat catcttgtga aaacaagagt tgtccttgct gattacgttt aaagttataa
     5221 ccatattctt caaaaagtaa ataatatttt ttgacagttg aagtagtaac tccaatagct
     5281 cttgctaatc cttctacttt ttcatagaat tccatgtaca tattagttat aacctccaat
     5341 ttatttagga ttataaccag tataacacca tcttgattta aaaatcttta gaattcatta
     5401 gggagaaatg ataagaacaa ttttaatact tgtttcgtac ttatcaaatt taataagtac
     5461 gaaacaagta tcaaagactt tttttaaagt tgaagggatg ctgtactaga tctctataaa
     5521 taattaaaac tgaaatatca ggaccaacat tacaaatttt aatatctata atctcaaaat
     5581 tattattttg aagaaagtca cgcaatcgtt cttcatattt atccaatgtg gtcgttaaaa
     5641 catgaatctt taatgtccta gaattaccta acataagatc atacacttct ttctatattt
     5701 gtcgtatatt acagtgtatg gagcttgtac agaatggaaa ctgcagttta tatctttttc
     5761 ttttataaaa aggatatatc taaagatgat taatgttttt aacaaaataa aaagctacct
     5821 cgccagtgga agcgaggtag ctcataaata gggttcatac gttgaaattt gtaaaaaata
     5881 gtctgattat taatatatca tattcacatg gaagatcaat tatgtggagt gtttagcata
     5941 tgaataaaag gtgcgctttt tattcatcta tttgtcactc agcccgagag gaaacgttga
     6001 taaatcaagg atgtataaaa agctgcatac tccggaaaaa gaaccctcgg cggaaccgtt
     6061 ctgcgacaag ggttcttttt tgggttgtgt tccggaaaga gtgattatgt taactacctt
     6121 tgtatatcat atacagcata aaaagagaaa aaactacctt ggttagaaga tagttttctc
     6181 tacatacatt tggtgttagg acgatgtttt gtgacatcat cagtattttg ttctaattat
     6241 ttttgtttta ttaattttga agattcactt agtaatgagt agcaatatat attaaaggct
     6301 gcttttagtt cattttatcg gttggatagt tcacttaaag tgagctaata aacataattt
     6361 atttggtaac taattataat ctttatctta attcttttca taattacaaa actgtatttg
     6421 ctaacaatgc tatataaatt tctaagtttt cttgtaatcc ttctaattgt gtctttaggt
     6481 tttctaactg ggattctaaa atcaatatgc caagagttat gtcctcaata gatcttgtta
     6541 aatcatctgc tctatcgaat tgatcttcat caaatgcttc tgatcgttct atagctcttc
     6601 ttcgaagaag gagaacacca ccgatcaacg cctcataggt atccctaatc tgtctactta
     6661 cgctcctaat gtcacgtact gtcgagagga gattgatgat caacgtttgt tgggtttgtg
     6721 taattggtgg cagaggagtt ggagttggag gagttggagg agcaacaggg gcagcagggg
     6781 ctcctgctac attaaaaaat gcattttcac ttgttaatat gttattacta ttagttagta
     6841 attttctcat taattgttga cttctattta aaagattctc gatttcttta aatgttcttt
     6901 ggctttgttt agtttcaaat gtaattcttt taaacaattc attaatatct agctcttggc
     6961 gcttacgatt ttgtttttta caattaccca ttttcaatat ttcccccttt ttgctctaat
     7021 tcctaaaagt tgttctgcta atttttcatt ttttcccaga agtttttcaa tactttcaat
     7081 tcggttattt tcacttttaa gcaatttaat taagtctcta atatctgaat cttcatcgtg
     7141 gtatttttta caattaccca tttttaaatt acctccattg atcaattttc agatgaatgt
     7201 aacaggattt ttgatagcct tgctatactt gtccttctta cgttcaccat cttttgggtt
     7261 gtaataagat ttatatatgc ttattacttc tattctctgt attatatttg ctaatagaaa
     7321 aagtgtttgt atgcttgtcc tatgatattg ataatattat agaaaacagc ctttagaagt
     7381 aactatttct aaaggctgtt tggttcttat aatattcatt atgttaacta cctttgtata
     7441 tcgtatacag cttaaaaaca gaaaaaacta ctggccatgc tctccagtag ttttttctgt
     7501 gacataatta gaagttagtc tagatgcctt tagacatctt gagtagtttg ctcaagttcg
     7561 taagaactta tgtctaatca gtagtttttt cttcatgaga tatagcaact tcatctttat
     7621 agtcaaaatc tgctttttga ctgattttcc cttcttcaaa gattacttgg aatctaatct
     7681 ttctaatatt gtcccaagat cctctaccaa atagaaatgg attcatttca taagttccag
     7741 ttccttttct aatcataatc ccttctttaa caagcttatt gatgttgtta tttaaacttt
     7801 gaactgtatt aaaccccaat tcttcagcca tttgtttttt tatgtatgca tttaaaacaa
     7861 cgatgttatc ataggccatt ctttttagaa gaacatttaa tgtcttctga ataccacttg
     7921 ataggttatt tatctgtaag atattatcta aataaatttt tacgtaatca ggttctttgg
     7981 aaacgtatga ttcttcgata tgtctttgaa atagcttttc tccatcatca ttagtaactg
     8041 tttctttaac ttccttattg attttttttc tttgtgtatc tccgattaag tccttttccc
     8101 aatctttcaa attcacacct cttttatatt ataatttata atctgtatcg tattataaaa
     8161 attagataat ttcaaggtgt tttgttataa tttataatct atttcagatt ataaattata
     8221 acaaatatga ttatgatctg taacgtatgt gattataatt tataataaat acgattatag
     8281 tttataacgt atacgattat agttaattca aagaaatgtt gttatatcaa tgtttataga
     8341 gttttatctc ttcttttctt ttattttatg tacatgtttt tttgtgttgt tttactggat
     8401 tctggaaaga ggtattatgt taactaactt gtatatgata tacagcttaa aaaaacgaaa
     8461 aaccaacctc tgagtcctaa aggttggttt ttctgctctt ggagaagaga ttcgtgttgg
     8521 acattggatg cctcacggcg tcatgagtag tttgcttaag aaataaaatt ttattcctaa
     8581 taggcaagtt acctaatcaa taacaaaaac atacagtgta ggaagaataa tttagactat
     8641 ggaatgaata actcttaacc tccgtttttt tgcacctcct tgatttatat agctaaagat
     8701 agaaaaacct atctcttctt agacaaaaga gataggtttt tctgttgcag aatttagttg
     8761 tatgagtcga tgcccgtata catcttgagt agtttgctcc aattattttt gtattattca
     8821 tctggacaat ttgcttagta atgggcagca acatatatta tgtatgacaa ataccacaac
     8881 tcttactcta ttttcatcct ttgtaaggat ggcaaacagc ccatgaatta attcatgggc
     8941 ttctttctag tagcaatata tgaactagag acctatttga agagcaagga ggttgctcaa
     9001 tattatagta tgcaaagtaa tgaaaaaggt ttggacaagc gttcatttta tctaagtttt
     9061 ctactatttc atacagccta aaaacagaaa aaactact
//
