LOCUS       NZ_CP026286             6987 bp    DNA     circular CON 06-FEB-2018
DEFINITION  Microcystis aeruginosa NIES-2549 plasmid p1, complete sequence.
ACCESSION   NZ_CP026286
VERSION     NZ_CP026286.1
DBLINK      BioProject: PRJNA224116
            BioSample: SAMN03480410
            Assembly: GCF_000981785.2
KEYWORDS    RefSeq.
SOURCE      Microcystis aeruginosa NIES-2549
  ORGANISM  Microcystis aeruginosa NIES-2549
            Bacteria; Cyanobacteria; Oscillatoriophycideae; Chroococcales;
            Microcystaceae; Microcystis.
REFERENCE   1  (bases 1 to 6987)
  AUTHORS   Yamaguchi,H., Suzuki,S., Tanabe,Y., Osana,Y., Shimura,Y., Ishida,K.
            and Kawachi,M.
  TITLE     Complete Genome Sequence of Microcystis aeruginosa NIES-2549, a
            Bloom-Forming Cyanobacterium from Lake Kasumigaura, Japan
  JOURNAL   Genome Announc 3 (3), e00551-15 (2015)
   PUBMED   26021928
  REMARK    Publication Status: Online-Only
REFERENCE   2  (bases 1 to 6987)
  AUTHORS   Yamaguchi,H.
  TITLE     Direct Submission
  JOURNAL   Submitted (15-JAN-2018) Center for Envrionmental Biology and
            Ecosystem Studies, National Institute for Environmental Studies,
            16-2 Onogawa, Tsukuba, Ibaraki 305-8506, Japan
COMMENT     ##Genome-Assembly-Data-START##
            Assembly Date                     :: 25-FEB-2015
            Assembly Method                   :: HGAP2 v. 2.3.0
            Genome Representation             :: Full
            Expected Final Version            :: Yes
            Genome Coverage                   :: 105.5x
            Sequencing Technology             :: PacBio RSII
            ##Genome-Assembly-Data-END##
            ##Genome-Annotation-Data-START##
            Annotation Provider               :: NCBI
            Annotation Date                   :: 02/05/2018 12:10:10
            Annotation Pipeline               :: NCBI Prokaryotic Genome
            Annotation Method                 :: Best-placed reference protein
            Annotation Software revision      :: 4.4
            Features Annotated                :: Gene; CDS; rRNA; tRNA; ncRNA;
            Genes (total)                     :: 4,219
            CDS (total)                       :: 4,168
            Genes (coding)                    :: 3,843
            CDS (coding)                      :: 3,843
            Genes (RNA)                       :: 51
            rRNAs                             :: 2, 2, 2 (5S, 16S, 23S)
            complete rRNAs                    :: 2, 2, 2 (5S, 16S, 23S)
            tRNAs                             :: 41
            ncRNAs                            :: 4
            Pseudo Genes (total)              :: 325
            Pseudo Genes (ambiguous residues) :: 0 of 325
            Pseudo Genes (frameshifted)       :: 98 of 325
            Pseudo Genes (incomplete)         :: 219 of 325
            Pseudo Genes (internal stop)      :: 76 of 325
            Pseudo Genes (multiple problems)  :: 57 of 325
            CRISPR Arrays                     :: 4
            ##Genome-Annotation-Data-END##
            REFSEQ INFORMATION: The reference sequence was derived from
            CP026286.
            Bacteria and source DNA available from the Microbial Culture
            Collection at the National Institute for Environmental Studies,
            Japan.
            Annotation was added by the NCBI Prokaryotic Genome Annotation
            Pipeline (released 2013). Information about the Pipeline can be
            found here: https://www.ncbi.nlm.nih.gov/genome/annotation_prok/
            Annotation Pipeline
            set; GeneMarkS+
            repeat_region
            COMPLETENESS: full length.
            ##antiSMASH-Data-START##
            Version      :: 5.0.0-af0cea8(changed)
            Run date     :: 2019-06-07 17:02:59
            ##antiSMASH-Data-END##
FEATURES             Location/Qualifiers
     gene            complement(1..689)
                     /locus_tag="myaer_RS21055_LOWER"
                     /old_locus_tag="myaer_p00005"
     CDS             complement(1..689)
                     /codon_start=1
                     /inference="COORDINATES: ab initio prediction:GeneMarkS+"
                     /locus_tag="myaer_RS21055_LOWER"
                     /note="Derived by automated computational analysis using
                     gene prediction method: GeneMarkS+."
                     /old_locus_tag="myaer_p00005"
                     /product="DUF3987 domain-containing protein"
                     /protein_id="WP_103672908.1"
                     /transl_table=11
                     /translation="MIYRDRHGFLKAANRLKSLADGEQRPAMRNVLIKARGLIGRLALN
                     LHLINSLFHGITPSIEIGKEIVNAAIELVSFFINQVRGMYGKLSDDLPSTYARLLSVAT
                     DWISPNNARKKAFNTKTRQDFPTTRIAEIFKELADQGYGEVRKTSRSIEFKTLKGDKGD
                     SKVTKGDSIVTPLEPLPDKDFNLKGDKGDSKNHFAVKTTNPLSENHPSSTENLKGDNLL
                     NSESPLS"
     source          1..6987
                     /collection_date="17-Jul-2005"
                     /country="Japan: Lake Kasumigaura"
                     /db_xref="taxon:1641812"
                     /isolation_source="eutrophic water"
                     /mol_type="genomic DNA"
                     /organism="Microcystis aeruginosa NIES-2549"
                     /plasmid="p1"
                     /strain="NIES-2549"
     gene            complement(658..2697)
                     /locus_tag="myaer_RS21060"
                     /old_locus_tag="myaer_p00010"
     CDS             complement(658..2697)
                     /codon_start=1
                     /inference="COORDINATES: protein motif:HMM:PF13148.4"
                     /locus_tag="myaer_RS21060"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /old_locus_tag="myaer_p00010"
                     /product="DUF3987 domain-containing protein"
                     /protein_id="WP_103672900.1"
                     /transl_table=11
                     /translation="MPLKTTKKPQRLTIAVVSYFLKIKDITTMYSKYSETNGKSLVTVT
                     KDNPCPHCGKPDWCYSIGDLSVCKRNHEPAPGWYKTDGADSEGHSYYAPIKEKETWAKP
                     PRPAKTTHYYYPDRDGNPLIRVVRKDDGNGKKDFYQQHWDGKQWVKGLGEIKRENIPIY
                     RYKGVKQAIANGESIFIVEGEGKADLLWSLGLPATCNLAGSGKWRETDSKDLEGASVIL
                     CPDRDVPGLKHCEAIAKDFPNAQWLYCFPDSPLWNHLPESRGADLKDWIDEGANKEDII
                     KGIGEKKPLKPQKTDKPNNVVSHPKFNPKPLETDKLGEVIDSLITENLTGSNREIKLAS
                     IAREFNLTQQQVTSIYKERLQENDQNLDKDDLYSDLENILKADSQRLDLTKVLPEKLAN
                     AINHHARLTGLRGEAYLTALLSGLSSTLHPDTALSLYPQTNWKVPPNLYAAIVALSGEG
                     KSIIGRAMIADPLKSLIELDDQDYEAKLSEHKSSLANYEMLKKSKNKDDLSDSFPDGPP
                     TKPTRRIRYFSGGTGEGIARYAANSQDKGMLLFRDEIAGLFKSANQYRGGRGSDSEDIL
                     EYFDGTPPITLRADPDKTLVCRKILLSIYGTIQPGVLEKLLGDKEDSSGQWARFIFCQL
                     PPSTLDLDIDAAYPDLTPMLTDLYRHFDRLPVATYDLSRPARIP"
     gene            complement(2627..3064)
                     /locus_tag="myaer_RS21065"
     CDS             complement(2627..3064)
                     /codon_start=1
                     /inference="COORDINATES: ab initio prediction:GeneMarkS+"
                     /locus_tag="myaer_RS21065"
                     /note="Derived by automated computational analysis using
                     gene prediction method: GeneMarkS+."
                     /product="hypothetical protein"
                     /protein_id="WP_103672901.1"
                     /transl_table=11
                     /translation="MYNLSNFSKLHQVCSTDKKDLSIDGHCDKSYHNLESSVKCPRCEN
                     LGELIESGPIRSLYQCRDCGIFAKLTHKKKRPDGLRRSIAQGELTKALEILEGAGHLDQ
                     DAIAALNLYRGDTVGIPGNASQNNQKTTATNDRCGFVLFEN"
     gene            complement(3277..3501)
                     /locus_tag="myaer_RS21070"
                     /old_locus_tag="myaer_p00015"
     CDS             complement(3277..3501)
                     /codon_start=1
                     /inference="COORDINATES: ab initio prediction:GeneMarkS+"
                     /locus_tag="myaer_RS21070"
                     /note="Derived by automated computational analysis using
                     gene prediction method: GeneMarkS+."
                     /old_locus_tag="myaer_p00015"
                     /product="hypothetical protein"
                     /protein_id="WP_103672902.1"
                     /transl_table=11
                     /translation="MEKHEIAAILTDTRYFTQNKNGDRLYDVEWQFKQGFLGFLALPVA
                     KFGNEEKAKRFQNRANEVLRELFDNEFSD"
     gene            complement(3546..4055)
                     /locus_tag="myaer_RS21075"
                     /old_locus_tag="myaer_p00020"
     CDS             complement(3546..4055)
                     /codon_start=1
                     /inference="COORDINATES: ab initio prediction:GeneMarkS+"
                     /locus_tag="myaer_RS21075"
                     /note="Derived by automated computational analysis using
                     gene prediction method: GeneMarkS+."
                     /old_locus_tag="myaer_p00020"
                     /product="hypothetical protein"
                     /protein_id="WP_103672903.1"
                     /transl_table=11
                     /translation="MQLIEAMATDRARRVNTKYGERTVIDAVRRDNGEKVTVWRGGDDD
                     YSQKYVIKNARLTLTLDNKGKYSLVEDPNLVSLGQPLPVEPVPVKPVHNFNHKIEETSE
                     KPSQNDQSTLSPNQKREIAQYVGDMADLFNFCLKTVDQRVENITPDDRRQVATSLFIAT
                     QKRFNI"
     gene            4289..4600
                     /locus_tag="myaer_RS21080"
                     /old_locus_tag="myaer_p00025"
     CDS             4289..4600
                     /codon_start=1
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_002762525.1"
                     /locus_tag="myaer_RS21080"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /old_locus_tag="myaer_p00025"
                     /product="hypothetical protein"
                     /protein_id="WP_103672904.1"
                     /transl_table=11
                     /translation="MPKINDNVVRVDFRCPIAIYQAIEKLAEENGQPIHHISGKVQISS
                     TIIDLLKTGLAIHAPDSLPQSNGGNLADKLSEKIDDAIEAKMNAAIASLRAELLTPNT"
     gene            4678..5226
                     /locus_tag="myaer_RS21085"
                     /old_locus_tag="myaer_p00030"
     CDS             4678..5226
                     /codon_start=1
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_007355626.1"
                     /locus_tag="myaer_RS21085"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /old_locus_tag="myaer_p00030"
                     /product="site-specific integrase"
                     /protein_id="WP_103672905.1"
                     /transl_table=11
                     /translation="MKRDRNGQAKILTDSEVSAIFSLLSARDRAIFAICLYGACRISEA
                     LSIRAGDIANGVIILRKNSTKGKKGTRSLPISPNLEKILNDYIDSIAFPGDFLFPGRDG
                     TKPLTTAYADLVLRESCQKLGLKGISTHSFRRTALTRMHLAGVPLRTIQRISGHSSLAA
                     LSVYLEVSNQNLIDAVGLI"
     gene            5323..6189
                     /locus_tag="myaer_RS21090"
                     /old_locus_tag="myaer_p00035"
     CDS             5323..6189
                     /codon_start=1
                     /inference="COORDINATES: ab initio prediction:GeneMarkS+"
                     /locus_tag="myaer_RS21090"
                     /note="Derived by automated computational analysis using
                     gene prediction method: GeneMarkS+."
                     /old_locus_tag="myaer_p00035"
                     /product="hypothetical protein"
                     /protein_id="WP_103672906.1"
                     /transl_table=11
                     /translation="MSKVISFSISDRYLDKLRSLYPALTDNLAVKQFVTDGLDSRLGNS
                     LDGSLDDGLDDRVKTLIESWVDGLLEVRLDASVGKLITSLSERMARLEARLDDNLDGSL
                     DDMERLLRLQREASIASPLPPSPSPSLPPDEPVQPAISEPDQPAIEDSEKKIVDSLDTL
                     IGSPIGIDESGSEPDTDAIESLTSESDSSIEGEDAIETVTPEVSPSPPDQPAIGTVPGD
                     SIDTEPDKGMNPNQAHEYLISKGIKDISYYYLNKWAKDKEGLIPWRGKNARKHLTLKDG
                     LYFPTTD"
     gene            6202..6423
                     /locus_tag="myaer_RS21095"
                     /old_locus_tag="myaer_p00040"
     CDS             6202..6423
                     /codon_start=1
                     /inference="COORDINATES: ab initio prediction:GeneMarkS+"
                     /locus_tag="myaer_RS21095"
                     /note="Derived by automated computational analysis using
                     gene prediction method: GeneMarkS+."
                     /old_locus_tag="myaer_p00040"
                     /product="hypothetical protein"
                     /protein_id="WP_103672907.1"
                     /transl_table=11
                     /translation="MTEYTYKDYKPYFVKRPKKPGYFTLREIRAMAVPGWPQSIEGLRK
                     KALREGWPRLGRRPSKGGAFEYRAGLPD"
     gene            complement(6651..6987)
                     /locus_tag="myaer_RS21055_UPPER"
                     /old_locus_tag="myaer_p00005"
     CDS             complement(6651..6987)
                     /codon_start=1
                     /inference="COORDINATES: ab initio prediction:GeneMarkS+"
                     /locus_tag="myaer_RS21055_UPPER"
                     /note="Derived by automated computational analysis using
                     gene prediction method: GeneMarkS+."
                     /old_locus_tag="myaer_p00005"
                     /product="DUF3987 domain-containing protein"
                     /protein_id="WP_103672908.1"
                     /transl_table=11
                     /translation="PVTFR"
ORIGIN
        1 ggtgacaaag gtgactcgct attaagtaag ttgtcacctt ttaagttttc cgtactggac
       61 gggtgattct cactcagtgg gttagtagtt tttacagcaa agtgattttt tgagtcacct
      121 ttgtcacctt ttaaattaaa atccttatct ggtaagggtt ctagtggggt gactatgctg
      181 tcacctttag tcacctttga gtcacctttg tcacctttta aggttttgaa ctcgatcgag
      241 cgagaggttt tcctgacttc cccgtaacct tgatcagcca attccttgaa tatttcagct
      301 attcgggtag tagggaagtc ttgacgggtt ttggtgttaa aagccttttt ccgtgcgtta
      361 ttagggctaa tccaatcggt agctaccgat agcagccttg cataggtgct aggtaagtca
      421 tccgataact tcccatacat ccccctcact tgattaataa aaaagcttac taactcaata
      481 gccgcgttaa cgatttcctt accaatctct atcgatggtg taataccgtg aaatagggag
      541 ttaatgagat gaaggtttaa agccaagcgt ccgatcagcc ctctcgcttt aattagcaca
      601 tttctcattg ccgggcgctg ttcaccatcg gctaatgatt taagtctgtt agcagcctta
      661 aggaatccgt gccggtctcg ataaatcata agtagccact ggcaagcgat cgaaatgtcg
      721 atataaatct gtcagcatcg gtgttaaatc cgggtaagcc gcgtcaatat caagatcaag
      781 ggttgagggg ggaagctgac aaaaaatgaa tcgcgcccat tggccgctgc tatcttcctt
      841 atcgcctaac agtttctcta gcaccccggg ttgaatcgtg ccataaatgc ttaaaagaat
      901 tttccgacat actaacgttt tgtcggggtc ggcccggagt gtaatcggtg gggtcccatc
      961 aaaatattct agaatgtcct cgctgtcaga cccacggcct ccccggtact ggttagcact
     1021 cttgaataaa ccggcgattt catctcggaa aagtaacatc cccttatctt ggctattagc
     1081 tgcataacgc gctatcccct caccagttcc cccgctaaaa tacctgatcc gccgggtagg
     1141 tttggtcggt ggtccatcgg ggaatgaatc gcttaaatca tctttgttct ttgatttttt
     1201 gagcatctca tagttggcta gagaggactt atgctcactt agcttcgctt catagtcctg
     1261 atcatccaat tcgattagtg acttaagagg atcggcaatc atggcacggc cgataatgct
     1321 ttttccttcc cccgataacg cgactatggc tgcgtacaag ttggggggaa ccttccagtt
     1381 ggtttgaggg tataggctta aggcagtgtc cgggtgaagg gttgatgata agccactgag
     1441 taacgcggtt aaataagcct cacccctcaa tcctgttagt ctggcatggt gattaatagc
     1501 gttagctaat ttttcaggga gtaccttagt aagatcaagc cgttggctgt cagcttttag
     1561 gatattttcg aggtcgctgt aaaggtcatc cttgtcaagg ttttggtcat tttcttgtaa
     1621 cctctcttta tatatagagg ttacttgctg ttgtgttaga ttgaattccc ttgcaatcga
     1681 agcgagtttt atttcacgat tggaaccggt gaggttctca gttatcagac tatcgataac
     1741 ttcccctagt ttgtcggttt cgagtggttt agggttgaat ttgggatgac tgactacatt
     1801 gttaggttta tccgtttttt gcggtttaag aggctttttc tcaccgattc ccttgataat
     1861 gtcttctttg ttggcccctt catctatcca atctttcaga tcagcccccc ttgattctgg
     1921 caagtggttc cataaggggg aatctgggaa acagtaaagc cattgggcat tggggaagtc
     1981 cttagctata gcctcgcagt gtttcaagcc gggaacatct cgatccggac ataaaatcac
     2041 gctggcccct tctaagtctt tactatctgt ctctcgccat tttcccgaac cggctaggtt
     2101 acaggttgcc ggcagtccca atgaccatag aagatcggcc ttaccctcac cctcaacgat
     2161 aaaaatactt tccccgttgg cgatcgcttg cttgactccc ttgtatcgat aaataggtat
     2221 attttcccgc ttaatttcgc ctaacccctt aacccattgt ttaccgtccc aatgctgctg
     2281 ataaaagtct tttttgccgt taccgtcatc ttttcttact actctgatta agggattacc
     2341 atcgcgatca ggatagtaat aatgggtagt tttagccggg cgtgggggtt tagcccatgt
     2401 ttccttttcc ttaataggcg cgtaatagga gtgaccttca ctatcggccc catcggtttt
     2461 ataccatccc ggtgcgggtt catggttacg cttgcatact gataaatcgc cgatcgagta
     2521 acaccagtca ggtttcccac agtgggggca tgggttatct ttggtgacag tgactagact
     2581 tttcccgttg gtttcgctat acttgctgta cattgtagta atgtccttaa ttttcaaaaa
     2641 gtacgaaacc acagcgatcg ttagtcgctg tggttttttg gttgttttga gaggcattgc
     2701 cggggatacc gacggtatcc ccccgataca gatttaaagc ggcgatcgca tcctgatcaa
     2761 gatgcccggc accttctaaa atttctaagg cttttgttaa ttccccttgg gcgatcgagc
     2821 gcctcaaccc gtccggtctt ttctttttat gagtgagttt tgcgaagatt ccacagtccc
     2881 ggcactgata gagagaccta atcgggccag actcgattaa ttcccctaaa ttttcgcacc
     2941 tagggcattt taccgatgat tctagattgt gatatgattt atcacaatga ccatcgattg
     3001 ataaatcttt tttgtcggtt gagcaaactt gatgaagttt gctaaagttt gatagattgt
     3061 acataagttt atgaatttgt tttcccagga acgctagtaa cgttatctgg ggatttttat
     3121 attttatact aaccgtatgg gaaaagttta taagtgcaaa atccctgcta tggtagtttg
     3181 atcataaagg gattcctatc aagtcaaaac cgcaattgtt tcggttttga ctgatacttg
     3241 ataactgata accgttaact ggaattaacc gctaagttag tcgctaaact cgttatcgaa
     3301 taactccctt agcacttcat ttgctcggtt ttgaaacctt ttagcctttt cctcgttgcc
     3361 aaactttgct acgggtaggg ctaaaaagcc caaaaacccc tgtttaaact gccattcaac
     3421 gtcatacaat cgatcgccat ttttgttttg ggtaaagtac cgtgtatcag tcagaattgc
     3481 cgcaatttcg tgtttttcca tctttttact cctgtttagg aggggaatta atcccctcaa
     3541 tagtgttaga tgttgaatcg cttctgagta gcgataaaca agcttgtagc gacttgccgg
     3601 cgatcatccg gggtaatatt ctctacccgt tggtcaacgg tttttaagca gaaattgaac
     3661 agatcagcca tatccccaac atattgcgcg atttctcgct tctgattagg cgataaggtt
     3721 gattggtcat tttgggaagg tttctcgctg gtttcctcga ttttatggtt aaaattgtga
     3781 acgggtttta ccggaaccgg ttccactggt aggggttgac ctaaactgac taaattaggg
     3841 tcttctacta gcgaatattt acccttattg tcaagggtta aggtcaagcg agcgttctta
     3901 atcacatact tttgagagta atcatcgtca ccaccgcgcc acacggtcac tttttccccg
     3961 ttatcacgtc ttaccgcgtc aattacggtt ctctcgccgt atttagtgtt aacgcgccgg
     4021 gcgcggtctg tggccattgc ttcgattagt tgcataatag agatatcctt tttggatgtt
     4081 aggcgatcgc tttttgattg gaagtctggg agcggtcgct tttggtttat atgtatatat
     4141 tatccgatac tctatcagat tgcaatacct atgcgattaa actatctgat aagtaaacga
     4201 taataatatc agatattcta ataggatatt tatcggagtt agtgatagta tgaatgtaaa
     4261 cccgataata tatctgatat agttatttat gcctaaaatt aacgataacg ttgtccgggt
     4321 tgacttccgg tgtccgatcg caatttacca agcgatcgag aaattagcag aggaaaatgg
     4381 gcagccgatc caccacataa gtggaaaagt acagataagc agtaccatca ttgacttatt
     4441 aaaaacaggg ttagcaattc acgccccgga ctccctaccc caatcgaatg gggggaattt
     4501 agccgataag ctatcagaaa aaatcgatga cgcgatcgag gctaaaatga acgcggcgat
     4561 cgcttccctt cgcgctgaat tactaacacc taacacctaa cccttaatct cctacccgtg
     4621 aagtgaaccc cacgggtttt taagcgtcgg tcacttggta attctgaaac tttaccaatg
     4681 aagcgcgatc gcaacggaca agccaaaatc cttactgaca gcgaagtatc agctatattt
     4741 agccttttaa gtgctagaga tagggctatt ttcgctatct gtctttatgg cgcgtgtaga
     4801 atctctgagg ccttgtcgat tagagcgggg gatatagcca atggcgtaat cattctcagg
     4861 aaaaattcga ctaaaggaaa aaagggaacg cgatcgctac ctatttctcc caatctcgaa
     4921 aaaattctca atgattacat cgactcgatc gctttccccg gtgacttcct ttttcccggt
     4981 agggacggca ctaaaccctt aaccactgcc tacgctgatc tggtattaag ggaatcttgc
     5041 caaaaattgg gacttaaggg tatatcgact cactcattcc gtaggaccgc gttaacccgt
     5101 atgcacttag caggggtccc actgcgaact attcagcgaa ttagtgggca ttcatcccta
     5161 gcggccctat cggtttatct ggaagtgagt aaccaaaacc ttatcgatgc tgtgggattg
     5221 atttaatgcg atcaagtgcc aattctcaaa gtgtcacagt tagatggtta catctaaact
     5281 aattagatga taataatatc atccaagatt agatgagatg caatgagtaa agtaatttca
     5341 ttttcaatca gcgatcgcta tctcgataag cttcgctccc tgtacccagc gctgactgac
     5401 aatctggcag tcaagcaatt tgtcacggac gggttagatt cacgcttagg caatagctta
     5461 gatggtagct tagatgacgg gttagatgat agggttaaaa cccttataga atcatgggta
     5521 gatggtttat tagaagtgag gttagatgct tctgtgggaa agttgatcac ttccttgagt
     5581 gagagaatgg caaggctaga ggcaagatta gatgataatt tagatggtag tttagatgat
     5641 atggaaagac tgctaaggtt acagcgagag gcttcgatcg cttcccccct tcccccttcc
     5701 ccttcccctt cccttccccc cgatgaaccg gttcaaccgg ctatcagtga accggatcaa
     5761 ccggcgatcg aggactcgga aaaaaagatt gtggactcgc tagatacttt aatcggttcc
     5821 ccaatcggaa tagatgagtc tggtagtgaa ccggacacgg acgcgatcga atccttgacc
     5881 agtgaatctg actcatctat tgagggtgag gacgcgatcg aaaccgtcac cccggaagtt
     5941 tcaccgtcac caccggacca accggcgatc ggaaccgttc cgggtgactc aattgacacg
     6001 gaaccggata agggcatgaa tcccaatcaa gcccatgagt acctaatatc taaggggata
     6061 aaagatatta gctattacta tctgaataaa tgggctaaag ataaagaagg gctgatacca
     6121 tggcggggca aaaatgcgcg aaaacaccta acactgaaag acggccttta cttccccaca
     6181 accgattaac cctagcctga catgactgag tacacctata aggattacaa gccctatttt
     6241 gtgaaacgtc ccaaaaaacc cggctatttc acccttagag aaatcagagc catggcagtt
     6301 ccggggtggc cgcaatcgat cgaaggcctg agaaaaaaag cacttaggga gggttggccc
     6361 cggttggggc gccgtccgtc taaaggggga gcctttgagt accgtgccgg gttgcccgat
     6421 taactgaaac ccttaccctg attgggttag gagaataaag taggttcttt tttggacctt
     6481 tcgatcgccc ctctcaatgg cgcgtcatct cgctagggac aaaaaaaatt acaccttgtt
     6541 gttgttatat aacaactatg aaacacttcc cacgggaaaa atgcccaaaa atacgcaaaa
     6601 attgcggttt tgggttttgg ggggattagg ttttagctag gcgatcgcta ttaggctttt
     6661 ttctttttct cgcgcttggg gccgggagtg tccggcgtga cttcatctaa ggcaaccgta
     6721 aaggggggat tttcctgata ccaatcttgg atggtgatta aatcaccatc gacactaatc
     6781 accccatggg cgcggccatt tttgttgtaa atacactcac cccccggttt aaggctgtca
     6841 gtgtcactta ccgggcgatc ggagtcggtt tcatcatggg gtgactcaag ggtgactggg
     6901 gtgactaagg gtgactgagt attttcaggg tagtcacctt ttgtaaagcc ttgctgtgat
     6961 tgagtttcac tatctaaagg tgactgg
//
