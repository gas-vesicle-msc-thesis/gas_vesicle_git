LOCUS       NZ_CP019463            15586 bp    DNA     circular CON 11-APR-2018
DEFINITION  Streptomyces autolyticus strain CGMCC0516 plasmid unnamed6, complete
            sequence.
ACCESSION   NZ_CP019463
VERSION     NZ_CP019463.1
DBLINK      BioProject: PRJNA224116
            BioSample: SAMN06209296
            Assembly: GCF_001983975.1
KEYWORDS    RefSeq.
SOURCE      Streptomyces autolyticus
  ORGANISM  Streptomyces autolyticus
            Bacteria; Actinobacteria; Streptomycetales; Streptomycetaceae;
            Streptomyces.
REFERENCE   1  (bases 1 to 15586)
  AUTHORS   Yin,M., Jiang,M., Ren,Z., Dong,Y. and Lu,T.
  TITLE     The complete genome sequence of Streptomyces autolyticus CGMCC 0516,
            the producer of geldanamycin, autolytimycin, reblastatin and
            elaiophylin
  JOURNAL   J. Biotechnol. 252, 27-31 (2017)
   PUBMED   28472671
REFERENCE   2  (bases 1 to 15586)
  AUTHORS   Yin,M., Jiang,M. and Lu,T.
  TITLE     Direct Submission
  JOURNAL   Submitted (23-JAN-2017) Yunnan University, Yunnan University, 2#
            Cuihu Beilu, Kunming, Yunnan 650091, China
COMMENT     ##Genome-Assembly-Data-START##
            Assembly Method                   :: SMRT Analysis v. 2.3.0
            Genome Coverage                   :: 279x
            Sequencing Technology             :: Illumina; PacBio
            ##Genome-Assembly-Data-END##
            ##Genome-Annotation-Data-START##
            Annotation Provider               :: NCBI
            Annotation Date                   :: 04/09/2018 15:36:40
            Annotation Pipeline               :: NCBI Prokaryotic Genome
            Annotation Method                 :: Best-placed reference protein
            Annotation Software revision      :: 4.5
            Features Annotated                :: Gene; CDS; rRNA; tRNA; ncRNA;
            Genes (total)                     :: 8,306
            CDS (total)                       :: 8,221
            Genes (coding)                    :: 7,702
            CDS (coding)                      :: 7,702
            Genes (RNA)                       :: 85
            rRNAs                             :: 6, 6, 6 (5S, 16S, 23S)
            complete rRNAs                    :: 6, 6, 6 (5S, 16S, 23S)
            tRNAs                             :: 64
            ncRNAs                            :: 3
            Pseudo Genes (total)              :: 519
            Pseudo Genes (ambiguous residues) :: 0 of 519
            Pseudo Genes (frameshifted)       :: 141 of 519
            Pseudo Genes (incomplete)         :: 431 of 519
            Pseudo Genes (internal stop)      :: 25 of 519
            Pseudo Genes (multiple problems)  :: 75 of 519
            CRISPR Arrays                     :: 3
            ##Genome-Annotation-Data-END##
            REFSEQ INFORMATION: The reference sequence was derived from
            CP019463.
            Annotation was added by the NCBI Prokaryotic Genome Annotation
            Pipeline (released 2013). Information about the Pipeline can be
            found here: https://www.ncbi.nlm.nih.gov/genome/annotation_prok/
            Annotation Pipeline
            set; GeneMarkS+
            repeat_region
            COMPLETENESS: full length.
            ##antiSMASH-Data-START##
            Version      :: 5.0.0-af0cea8(changed)
            Run date     :: 2019-06-07 16:18:07
            ##antiSMASH-Data-END##
FEATURES             Location/Qualifiers
     source          1..15586
                     /collection_date="1998-01-01"
                     /country="China:Yunnan"
                     /culture_collection="CGMCC:0516"
                     /db_xref="taxon:75293"
                     /isolation_source="soil sample"
                     /mol_type="genomic DNA"
                     /organism="Streptomyces autolyticus"
                     /plasmid="unnamed6"
                     /strain="CGMCC0516"
     gene            complement(<37..114)
                     /db_xref="GeneID:36392517"
                     /locus_tag="BV401_RS42590"
                     /old_locus_tag="BV401_41180"
                     /pseudo=""
     CDS             complement(<37..114)
                     /codon_start=1
                     /db_xref="GeneID:36392517"
                     /inference="COORDINATES: protein motif:HMM:PF08990.9"
                     /locus_tag="BV401_RS42590"
                     /note="incomplete; partial in the middle of a contig;
                     missing stop; Derived by automated computational analysis
                     using gene prediction method: Protein Homology."
                     /old_locus_tag="BV401_41180"
                     /product="hypothetical protein"
                     /pseudo=""
                     /transl_table=11
                     /translation="VSETKLRDYLKRATASLHETRQELKE"
     gene            complement(195..15558)
                     /db_xref="GeneID:34075738"
                     /locus_tag="BV401_RS41120"
                     /old_locus_tag="BV401_41185"
                     /pseudo=""
     CDS             complement(195..15558)
                     /codon_start=1
                     /db_xref="GeneID:34075738"
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_007491077.1"
                     /locus_tag="BV401_RS41120"
                     /note="frameshifted; Derived by automated computational
                     analysis using gene prediction method: Protein Homology."
                     /old_locus_tag="BV401_41185"
                     /product="polyketide synthase"
                     /pseudo=""
                     /transl_table=11
                     /translation="VESPEDLWELVASGTDAVSTFPWTVGGTRTCTTPTRRSRARAIRV
                     RAGSCTTRTASTPPSSA"
ORIGIN
        1 gacgatcgcg atgggttccc ggtccctgtc cttgagttcc ttcagctctt gacgagtttc
       61 gtgcaggctg gccgtcgcgc gcttgaggta gtcgcgcagc ttggtctcac tcacctttct
      121 gtcactcctc tgagcacccc ggacaggggt ggaaagcctt cggccggtaa cttccgggga
      181 cgggggcgtc cgcgtcacga gagctcctcg tcgaggaagt cgaagatctc gtcgtcggtg
      241 gccgactcga acttgtcggc ggcgacgacc ccaccgttcg cggtctccgt ctgctgcccc
      301 gcggtcagct tcgccagcac ctgctgcaag cgcgagtgag acgtgccgtg cggcttcgtc
      361 cgccaccacc tcggcgaagg agcgctccag cttctcagct ccgacagaac cggcagcacc
      421 agcgatgcgt ccggctcttc cgcgacgagc tcctgcgcca ggtagccggc cagcgccctg
      481 gacgtcgtgt agtcgaacag cagggtggcg gggagccgca gaccggtcac cgtgcccagc
      541 tggttgcgca gtccgaccgc ggtcagcgag tcgaagccca gctcgaggaa gccacggtcg
      601 gggcgatcat gcggggctgt cgtggcccag cgccatggcc gcgtgctccg tgaccagttc
      661 gaggagcgcc tgctgccggg cggcggggtt catcgagccg atccggcggc gcagttcggc
      721 ggggtcggcg ccggatccgc ccgtggacgc cttcgcgggg ccgcgccggc cgtgtccgga
      781 tgatgccccg cagcagcgcc gggatcggcc cgtcctggct cgccacgccg aggcgttgag
      841 ccgtgccgga ccagagtggc cgcgtcgatg ccgcgggcgg cgtcgaacag ggccagttgc
      901 ctcctcggag gacatcgggg tcacgccttc gcgggcgagg cgccgcatat gggcctcgtc
      961 cagcttgccc gtcataccgc tgcgctcctc ccacaggccc caggccagcg actggcggga
     1021 aggccctcgg cccggcgccg gtgggcgaac gcgtccagga aggcgttggc ggccgcgtag
     1081 ttggcctggc cggtgccgcc gagggttccg gagagcgagg agaagagcac gaacgcggac
     1141 aggtcgagac cggcggtcag ctcgtgcaga ttgagcaccg cgtccacctt gggccgcagc
     1201 acacggtcca cccgctcggg cgtgaggctt cgacgacgcc gtcgtccagc actccggcgg
     1261 tgtgcaccac ggcggtgagc gggtgttcgt ccggcaggga ggccagaagc gcggcgaggg
     1321 agtcgcggtc ggcggtgtca caggcggcga ccacggcgtg ggcgccgagg tccgtcagct
     1381 ccgtccgcag ttcggccgcg ccgtccgcgg cgaggccgcg gcggctgacc agcacgaggt
     1441 ggcggacgcc gtgctcgccg acgaggtgct tggcgaccag gccgccgagg ccggatgccg
     1501 ccccggtgac caggacggtg ccctgcgggt cccaggtgac gccggtcgtg accgaccgtg
     1561 cgtcgccctg ctcggacgcg ctgccatgcg ccgccacccg ggccagcggc cgaccaggac
     1621 cgtgccgttc cggaccacga tctgtggttc gtcggtggtg agcgccgccg ggaggatccg
     1681 gcgcgagcct tccctgtcgt ccaggtcggc caggacgaac cggccggggt tctcggactg
     1741 ggccgagcgc accaggcccc agacggccgc actggcgggg tcggccacat cggtgtcatc
     1801 gcccgcggcg atggcccccc gggtgaggaa gaccagccgg gagccgccga ggcggttgtc
     1861 ggccagccag ctcttgagca ggtcgagggc gtggtggccg gcttcccgta ccgcctccgc
     1921 cagttcccgc gtccggccgg tgcgggacac gttcgcggcg agtacgacgt cgggcgcggt
     1981 cgcgcccgcg tcgatggcct cggccagggc cgcgaggtcc gggtaggtgt ccacggtgac
     2041 atcggccgat ccgagggcgc cggtcagctc ggcccggtcg ggtccgacga gcgcccagcg
     2101 cccggcggcc gcgccgctcg tctccggggc ggtcggcacc cagtcgacct ggtacagcga
     2161 ctcgtgcggc tgggcggagt tgacctgctc cggggccacg ggccgcagta cgagcgagtc
     2221 gaccgaggcg acgagcgcgc cggagacgtc ggtgacgtgc agggagaccg tgtcctcgcc
     2281 gacgctggtc agccgcaccc gcagcgcgga cgcgccgctt gcgtggaggg tcaccccgtt
     2341 ccacgagaac gggagggtgc tgccgtcgct gccggacggc ttgaacgcca cggtgtgcag
     2401 cgaggcgtcg agcagcgccg ggtggagacc gtagcgcgcg gcctcgccct cctgtgcctc
     2461 gggcaggtcg acctcggcga agacctcgtc gccacggcgc caggcggcgc gcagcccctg
     2521 gaaggccggg ccgtaggcga agccgacgtc gatgaaccgg tcgtagagat cggccacgtc
     2581 gatggcctcc gcatccgcgg gcggccacac ggcgaagtcg gcggccacgg actccggtgc
     2641 cgtggtggac aggtccctgc tgagcaggcc ggtcgtatgg cgcgtccagg gtcgtccggc
     2701 tgggcgtcct cgggcctgga gtacagattg agcgaacggg cgccggactc gtccggctcg
     2761 cccacggcga gctggatctg gaccgcgccg cgtacgggca ggaccaacgg catttcgatg
     2821 gtcagttcct ccacccgctc gtagcccacc tgctcggcgg cgcgcagggc cagttccacg
     2881 aaggccgtgc cggggaagag cacggtgtcc gcgaaggcgt ggtcggccag ccacggctgg
     2941 gtggcgaccg acaggcgccc ggtgaacagg taggtgtcgg tgtcggccag ggcgacggcg
     3001 gcgcccagga gcggatggtc gggcgagtcc aggcccgccg atgtcacatc tccactggcg
     3061 gccggcgcgg cttcgagcca gtagcgctgc cgctggaagg cgtaggtggg cagctccacc
     3121 cgctgggcgc cacgcccggc gaacaccgcg tcccacttca ccgtgacgcc gtgcacatgg
     3181 agttcggcca gcgccatggt cagcgactga agctcgggcc ggtccttgcg cagtgcgggc
     3241 acgagggcgg gagccaccgc gccaccgggt tctgcggccg tcaggcagtc ctgcgccatc
     3301 gcggtgagca caccgtccgg gcccacctcg acgaacgagg tcacgcccag gttctccagg
     3361 gtccttaccc cgtccgcgaa ccgcacggcc tcgcgcacat gccggaccca gtactccggc
     3421 gagcacacct cgtccgcgtc ggccggggcg ccggtgacgt tggacaccag ggagatggtc
     3481 ggcggggcga acaacagccc ctcgacaacc ttccggaagt caccgagcac accgtccatg
     3541 cgcggggagt ggaaggcgtg gctgacgcgc aggcggcgtg tcttacggcc ctcccgctcc
     3601 cactgtccgg cgatctccag cacggcggct cgtcgcccgc gatgaccacg gcggtggggc
     3661 cgttgatggc cgcgatgctc acctcggcct ctcgaccggc cagcgacgcg gcgatctcct
     3721 cctcgggcgc ctggacggcc accatcgcgc cgccttcggg cagcgcctgc atcagccggc
     3781 cacgggccgc caccagggcg cacgcgtcct ccagggagaa caccccggcc acatgggcgg
     3841 cggcgacctc accgatggag tggccgatca gcacatccgg ggtgacaccc cagtgctcca
     3901 gcagccggaa cagcgccacc tcgatggcga acagcgcggg ctgggtgaac gccgtctggt
     3961 tcagcaactc ggcgtcggca ctgccctcga ccgcgaacat cacctcggtc agcggccggt
     4021 cgagcagcac atcgaggtag gcgcacacct cgtccagcgc ctgggcgaac accgggaagg
     4081 cgtcgtacag ctcccggccc atgcccagcc gctgcgcgcc ctggcccgag aacaggaacg
     4141 ccgtcttgtg ctcggcccgt gccacgccct ggatcagcgt ggccgcgttc ccgcccgccg
     4201 ccaacgcttc gagcccggag cggaactgcg ccgggtcctc cgcgaccaca gcggcgcggt
     4261 agggcagggc ggaacgggtg gtcgccagcg agtagccgat gtcggcgaca tcaccggccc
     4321 gcgcggtgcg cacaccgtcg aactcgcgca gccgcccggc ctgagcgctc agcgcatcgg
     4381 cgttgctgcc cgagagcagc cagggaacca ccgtgagacc cgcgggcgaa ccggtggcct
     4441 cgccacccgt ctccggcgac ggcgactcaa cggggtcggc cgcctcgatg atcgtgtgcg
     4501 cgttggtgcc gctgatcccg aaggacgaca cggccgcccg gcgcgggcgg tccgtctccg
     4561 gccacgcggt cgcctcgttc agcagcgaca cggcgccact ggtccagtcg atatgcggcg
     4621 agggctcgtc cgcgtggagc gtcttcggca gcacgccctc gcgcatggcc atcaccatct
     4681 tgatgacacc cgcgacaccg gccgcggcct gcgtgtgccc gatgttcgac ttgatcgagc
     4741 ccagccacag cggctggtcc tcggaacgcg cctggccgta cgtggccagc agcgcctgcg
     4801 cctcgatcgg gtcacccagc gaggtgcccg tgccatgtgc ctccaccgcg tccacatcgg
     4861 cggccgacag acccgcgttt tcaagggcct tgccgatcac acgctgctgc gacgggccgt
     4921 tcggcgccgt cagaccgttg gacgcgccgt cctggttgat cgccgaaccc cggacgacgg
     4981 ccagcacctt gtggccgcgg cgccgggcgt ccgacagccg ctccaccagc agcagaccca
     5041 caccctcgga ccagccggtg ccatcggcct cggcggagaa cgccttgcac cgtccgtccg
     5101 gcgacagcac acgctgggcg ctgaactcga cgaacgtgcc cggcgtggcc atgatcgtca
     5161 caccgccggc cagcgccatg tcgcactcgc cctggcgcag cgactggcag gccaggtgca
     5221 gggcgaccag cgacgacgaa cacgcggtgt ccaccgtcac ggccgggccc tcgaagccga
     5281 aggtgtagga gaggcggccg gaggcgacgc tggacgcgct gccgagtccg aggtagccct
     5341 gcacaccgtc cggcgcgctg atcgcccggc cgctgtagtc ctggtagttc gagccgatga
     5401 agacacccgc ctggctgccg cgcaacgccg ccgggtcgat tccggcccgc tcgaacgcct
     5461 cccaggacgt ctccagcagc agccgctgct gggggtccat ggccagggcc tcacgcggtg
     5521 agatgccgaa gaagtccggg tcgaagtcgg agacgttgtc caggaacccg ccggtgagcg
     5581 cgtaggtctt gcccggcttg tcggggtcgg ggtcgtagag cccgtcgagg tcccagccac
     5641 ggtcgtcggg gaacccggag atcgtgtcga ctccggcgtt caccacgtcc cacagctgtt
     5701 caggggtccg cacaccgccc gggaagcggc agctcatcgc cacgatcgcg atgggctcgt
     5761 cgaaggagtc cgatcgggtc ggcgtgacct cggcggcgcc gccttcctcg gtgccgaaca
     5821 tctcgccgtt caggaaccgc gccagcaccg tgggcgtggg gtagtcgaag accaggctga
     5881 ccggcagccg cagcccggtc gccgcgccga tccgattgcg cagttcaacg gcggtcagcg
     5941 agtcgaagcc gaggtcgcgg aaggcacggt ccgacgcgac cgcgtccgcg ctgtcgtgag
     6001 cgaggacatc ggcggcctgg gcacgcacca gatcgcgcag cgcggcttcg cgcttctccc
     6061 ggggcagggc ttccagccgg tcgtggagtt ccgcccgccc ggggtcggcg ccacccggtg
     6121 tggactgctt ccccgtggcc gtggtccgcg cgccgcgcag ctcgcggaag agggggctgt
     6181 cgacggagtc cgcttcgaag cgtgtccagt cgatgtcggc gacggcgagg aatgtctcgt
     6241 cgtggtccaa cgcctggtgc agtgccgaaa tcgccagctc gggggacatg gccagcatcc
     6301 ccgagcggct catctgctca ccgacctcac cggagacgag gccaccgccg gcccaggcgc
     6361 cccagccgat ggaggtggcc ggaaggccat ccgcacgccg ctgcgacgcc agcgcgtcga
     6421 gataggcatt acccgccgcg taactcccct gacccggacc acccaacgtg cccgccgccg
     6481 aagagaacaa cacaaacgcc gacagatcca tccccgccgt caactcatgc agattccacg
     6541 ccgcatccac cttcggacgc aacaccccag ccgcacgctc caccgtcagg gcgtccacga
     6601 cgccgtcgtc cagcacaccg gccgtgtgca ccacggcggt gagcggcaac tccgaaggaa
     6661 tcgcgtccag caccgcggcc agtgcgtcac ggtccgccgc gtcgcacgcc gccaccgtca
     6721 cccgggcccc gagctcctcc aactccgcct tcaacccggc ggcaccctcg gcatccatcc
     6781 cgcgacggct cgtcagcacc agatgctcgg cacccgtacg cgccagccag cgggccacat
     6841 gaccacccag cgcacccgta ccaccggtca ccaacaccga accacccgaa cgccacccac
     6901 cgacaccggt ggcctcagac gcggaagccc gcaccagccg cttgacgaac acacccgaac
     6961 cacgcaccgc gacctgatcc tcaccgaccg cgcctgccag cactccggcc agccggacca
     7021 gcgcccggct gtccatggcc tcgggcaggt ccacaacacc gccccagcgc tggggatact
     7081 ccgcccccac gatccggccc aggccccaca ccatcgactg caccgggctc accagccggt
     7141 cggaacgacc caccgacacc gcaccccggg taccacacca cagcggagcc accacacccg
     7201 catcgcccag cccctgaacc agacccacca tcgcggccag accacccggc accacaccga
     7261 acgccggatg ccgggactcg tccagcgcca gcagcgagaa cacacccgcc acgtccgacg
     7321 gctccaccgc cacgccgtca agcgccacgc gcagccgctc cgcaacggcc tcgcggtcat
     7381 ccgcggcccc gacgaccacg gagaccacat gcgcgccgcg accggtcaag ccgtccagca
     7441 cggtggcagc cagggtctcg tcaccacagg cttccggaac ggccacgagc caggtgcccg
     7501 agagcgtcga ctcggacgcg gacgccaggg gcttccagac cgccttgtaa cgccagctgt
     7561 ccaccgtgtt ctgctcgcgc cgctgacggc gccaggacga cagcgcggga agcagcgccg
     7621 tcagcgacga ctgccgctcc tcgtcctcca cctccaacgt ccgcgccagc gcctccagat
     7681 cctcgcgctc caccgcctcc cagaagcgcg catccaccgc atcggcgctc aagccgtcac
     7741 cggcggccgc cacctctgcg gccgagaggg cgtccagcca gtaccgctgg cgctggaagg
     7801 cgtaggtcgg cagctcgaca cggcgggcac ggtgtccggc gaacaccttc cgccagtcaa
     7861 ccgtggcccc attgacatgg agctcggcgg cggaggtcag gaaccgctcc agaccaccct
     7921 cgtcacgccg cagcgtgccc acgaccaccg cctgtgcgtc cgcggcctcc acggtctgct
     7981 gcacgggaag ggtgagcacc gggtgcgggc tcacctcgat gaacacgccg tgtccagtgt
     8041 cgagcagggt gcgggtggtg gcctccagct caacggtctg ccgcaggttc cggtaccagt
     8101 actcggcatc cagaccggcc gtatccacca gctcaccgga caccgtcgaa tagaacggca
     8161 cctccgagga gcggggttcc agatccgcca ggaccctcag cagctcctcg cggatcccct
     8221 ccacatgcgc ggagtgcgag gcgtagtcca ccggaacccg gcggacccgc acctcctcac
     8281 cctccagctc cgccaccatc tcatccagcg cacccggatc acccgagacc acgacggaac
     8341 ccggaccgtt gaccgcggcc accgagatcg caccgtccca agcggcgatg cgctccttgg
     8401 cccggtccgc cgacagtcca acggacacca tgccaccgtg gcccgccaga ccggcggcga
     8461 tggcctgcga ccgcagcgcc accaccttcg cggcgtcctc cagcgacaac gcacccgcca
     8521 cacacgccgc cgcgatctcg ccctggctgt gacccatcac cgcatccggc tccacaccca
     8581 ccgagcgcca cagcttcgcc agcgacacca tcaccgcgaa caacaccggc tgcaccacat
     8641 ccacccgatc gaaaccggga gcaccctcgc caccacgcag cacctcggtc agcgaccagt
     8701 ccacatacgc cgacagagcc gtctcacact cgccgatcgc ctccgcgaac accggcgagg
     8761 actccagcag ccccaccgcc atgcccatcc actgcgcccc ctgaccgggg aacacaaaca
     8821 ccgaacggcc ccgcgcatca gccacgccac gcgtcacacc cgaggccacc tcgtccccgg
     8881 ccaacgcgcc cagccgctcc aggagttgct cacggtcggc acccgtcacc accccgcggt
     8941 gctcgaacac cgagcgcgac gtcaccagcg agaacgccac agcggcggcg tccagctccg
     9001 ggcgctcgat cacgaactcc cgcagccgct cggcctgcgc ccgcaggccc tccgggctac
     9061 ggccggacac cagccacgcc accggaccgc cgacggcctc ggcctcggca tcctcggccg
     9121 cctcgacctc cggggcctgc tccaggatcg cgtgcgagtt ggtcccgctc acgccgaagg
     9181 acgacacgcc ggcgcggcgc gggtggtccg actccggcca ggcggtggcc tcggtgagca
     9241 gcgcgatgtc gcccgccgac cagtccacgt gctgtgacgg ctcatcgatg tgcagcgact
     9301 tcggcaacac gccgtgccgc atcgccatca ccatcttgat gacaccggcc acaccggccg
     9361 ccgcctgcga atggccgatg ttcgacttca ccgatccgag ccacagcggc tggtccgtgg
     9421 gccggtcctg gccgtaggtg gccagcagag cctgcgcctc gatcgggtca cccagcgacg
     9481 tacccgtacc gtgcgcctcc accgcgtcga cctctgcgcc cgacagccgg gcggtcttga
     9541 gcgccgcgcg gatcacccgc tgctgggacg ggccgttcgg cgccgtcaga ccgttggacg
     9601 cgccgtcctg gttgaccgcc gaaccccgga cgagggccag cacctcgtgg ccgttgcgcc
     9661 gggcgtccga cagccgctcg acgagcagca gcccgacgcc ctcggcgaag ccgaatccat
     9721 cggcgtcatt gctgaacgcc ttgcacttgc cgtcggcgga cagcgcgcgc tgacggctga
     9781 agtccaccag tccggcctgt gtggacatca ccgtcgcgcc accggcgagc gccaagtcgc
     9841 actcgccctg gcgcagcgcc tggaccgcga ggtgcagcgc gaccagcgac gacgagcacg
     9901 cggtgtccac cgtcaccgcg ggcccctcca agcccaaggt gtaggagata cggcccgagg
     9961 ccacgctgcc cgcactgccg gtgccgaggt agccctcaaa gccttcgggg gcgttgcctt
    10021 gcagccgggt gccgtagtcg ttgtacacaa cgccgacgaa gacgccggtc cggcttccct
    10081 tcgccgactc ggggtcgata ccggcccgct cgaacgcctc ccatgaggtc tccagcagca
    10141 gccgctgctg cgggtccatc gacagcgcct cacggggcga gatcccgaag aacgccgggt
    10201 cgaactggtc ggcgtcgtag aggaagccac cgttgcgggc gtagtacgtg ccctcgcgcg
    10261 cggggtccgg gtcgtatgtc gtctcgaggt cccagccacg gttggacggg agcgaggcgg
    10321 tggcgtcggt gccccccatc accaggtgcc acagttcctc gggcgtccgc accccaccgg
    10381 ggaatcggca gctcatggag acgatcgcga tcggatcgtc gtccgtgcgg gacagggcct
    10441 cggctgatcc gcccgccgtc ggtgccgcgg tggccgtgcc ctgggacccg aggatctccg
    10501 aacgcagatg gcccgccagt accgtcgggt tcgggtagtc gaaaacgagc gtcgcgggca
    10561 ggcgcaaacc ggtttccgca ctcagccggt tgcgcagttc cacggccgtg agcgagtcga
    10621 agcccagctc cttgaagggc cgtcccacct cgacggtgtc cgagttcgcg tggcccaggg
    10681 cggcggccac ctggatacgg atgagctcct ccagcaggaa ctgctcctgg tccgcctcgg
    10741 acatgctgac cagtcgccgc cgcaggcggg cggcgatgtc gtccacgtcc gcgtctccgg
    10801 caccggcgac gcgccggacc gtcatccccg gtacgacatt gcggaagagg gcggggagtt
    10861 cggcgccctg cgcacgcagc gccgggatat ccagccggaa gagaaccggg acgaccgcat
    10921 ccgagaccag ggcggcgtcg aacagctcaa ggccctcgtc cgtggccagc ggcagcacgc
    10981 ccgagcggga tattcgctgg atgtcgtcct cgccgaggtg tccggtcatg ccactggcct
    11041 cggcccacag accccacgcc atcgacacac ccggcagccc ctgcgcccgc cgccacgagg
    11101 cgagaccatc caggaacaca ttcgcagccg cgtagttgct ctgcccggca ccacccagca
    11161 caccagtggc cgacgagaac agcacgaacg ccgacagatc catccccgcc gtcagctcat
    11221 gcagattcca cgcagcgtcc accttcgccc gcaacaccgg ctcaacccgc tcacgcgtca
    11281 acgacgtcag caacccgtca tccagcacac ccgccgcatg caccacacca ctgagcgcaa
    11341 aaccctccgg caaccccgac aacaccccgg ccaaagcctc accatccgcg gcatcacacg
    11401 ccgcgatcac cacctcagca cccgccccct ccaactccgc ccgcaactcc gccgcaccct
    11461 cggcctccgc accacgacgc gacaacaaca ccagcctgct caccccatgc cgcaccacca
    11521 gatgccgcga caccacacca cccaacacac ccgtaccacc cgtcaccaac accgcacccg
    11581 aaccaaaccc acccgacacc tcacccgaca cccgcacccg acccagacgc ggcacccgca
    11641 ccacacccga acgcaccacc acctgctcct cacccagagc cagcaccccg ggcagaacgc
    11701 caccgacacc ctccacatca tcggtgtcca ccagcaccag acgccccggg ttctccgact
    11761 gcgcggaccg cactagcccc cacaccgccg ccccggccat gtcctccaca ccatcgcccg
    11821 catcgacagc accccgcgtc accaccacca accgctcacc cgcgagatcc tcatccgcca
    11881 cccacgcccg cacccgctcc aggacctcaa ggacccggcc atacacctca cccaccacat
    11941 ccggaccctc ggaaacgacc cgcaccacct cagtccgcga cgccacctca cccccggccg
    12001 ccacagcccc aggcgcgtcc acccactcca cccggaacaa cccatcgcgc gccgctccgc
    12061 cgaccgcgtt cagctgatcc gcgtcgaccg ggcgcagcac cagcgactcg accgaggcca
    12121 ccccacgccc ggactcatcc cccaccagca ccgacaccgc atccgaccca gcacccaccg
    12181 acgccaaccg cacccgcaac gacgtcgcac ccaccgcctc caaagcgaac ccacgccacg
    12241 agaacggcaa caaactcccc gaaccacccg caagagcccc actcacaccc aacgcatgca
    12301 acgccgcatc gaacaacgcc ggatgcaaac caaactgccc cgcccgctcc cgctccccct
    12361 ccggaagccc aacctcagca aacacctcac catcacgccg ccacgcacgc ctcaacccct
    12421 gaaaaacagg cccatactcc aacccgttct ccgcagcccg cacatactca cccgacacat
    12481 caaccaactc agcccccacc ggaggccaca caccaaagtc gtactgtgtc tcggagctgg
    12541 gcacggccga accgagcaca ccatgggcat gccgcaccca tgccgcgcca tcgccgtccg
    12601 cctcctcccg cgagtacacg ctcacctcac ggaagccgga agcgtcggcc tcccccacct
    12661 caacctggag gtgtacgcca cccttctcgg gcacgatcag cggcgcttcg atgaccagtt
    12721 cctcgacccg gccgcagccg acctgatcgc ccgcccgtac caccagctcg acgaacgccg
    12781 tccccggcag cagcatcacc ccgtgtacgg cgtgatccgc cagccactca tgcgtggcca
    12841 acgacaaccg cccactcagc agcaccccgc caccactcgc caacgacacc accgcaccca
    12901 gcaacggatg ccccagcgaa cccaacccca ccgaacccac atcacccaca ccggcgggag
    12961 cgtcaagcca gaagcgctcg tgctggaagg cgtaggtcgg caattcaacg cggtgggcgc
    13021 cgtgtccggc gaacaccttt tgccagtcaa cgcgcgcccc gctgacgtga agctcagcgg
    13081 cggaggtcag gaaccgctcg gccccgccct catcgcgccg tagtgtcccc acgaccacgg
    13141 cccgcgactc agcggcctca actgtctgct gcaccggaag ggtcagcacc ggatgcgggc
    13201 tgacctcgat gaacacggtg tggcccgcgt cgagcagcgt gcgagtcgtg gactccagct
    13261 caaccgtctg ccgcaggttc cggtaccagt actcggcgtc cagctgagcc gtgtccacca
    13321 gctcaccggt caccgtcgag tagaacggca cctccgagga ccgcggctga aggtccgcca
    13381 gaaccctcag cagctcctca cgaatcccct ccacatgcgc ggaatgcgag gcatagtcca
    13441 ccggaacccg acggacccgc acctcctcac cctccagctc cgcaaccatc tcatccagcg
    13501 cgtccgggtc accggccacc acgaccgaac ccggaccatt gaccgccgcc accgaaatcg
    13561 ccccacccca ccgggcgata cgctcctccg cctgctccgc ggacaacccg accgacacca
    13621 tgccaccacg accagcaagc cccaccgcga tcgcctgcga ccgcaacgcc accaccttcg
    13681 cggcatcctc cagcgacaac ccacccgcca cacacgccgc agcaatctca ccctgactat
    13741 gacccatcac cgcatccggc tccacaccca ccgaacgcca caaccgcgcc aacgacacca
    13801 tcaccgcaaa caacaccggc tgcaccacat ccacccgatc aaaaccaggc gcaccctcaa
    13861 caccacgcaa cacacccgtc aacgaccagt ccacatacgc cgacagcgcc gcctcacact
    13921 caccaaccgc ctccgcgaac accggcgagg actccaacaa ccccactgcc atacccaccc
    13981 actgcgcccc ctgccccggg aacacaaaca ccgaacggcc gggaggcgcg ctctcgccca
    14041 ccaccacgcc cggaagagcg gacttctccg cgacggcgcc cagaccggac agcaactcgt
    14101 cccggtcccg gcccaccagc accgcgcggt gatccagcac cgaccgggtc atcaccagcg
    14161 accgcgccac atcggcggcg cccagctccg aacgcccggc cacgtacgcc cgcagccgct
    14221 cggcctgggc ccgcagcccc tcggcaccgt ggcccgacac cacccaggcc accggaccgt
    14281 cggcgacctc gggctcagcg tccccggccg cctcggcctc cggggcctgc tccaggatca
    14341 catgcgcgtt cgtcccgctc acaccgaacg aggacacacc ggccctgcgc ggctcacccg
    14401 tctccggcca cggggtcgac tcggtcagca gcgacaccgc ccccgccgac cagtccacgt
    14461 gctccgacgg ctcctctaca tgcagggtct tcggcagcac accgtgccgc atcgccatca
    14521 ccatcttgat gacgccaccc acacccgcgg ccgcctgcgt gtgacccacg ttcgacttga
    14581 acgagcccag ccacaacggc cgatcggctc cggcgcgctc cttgccgtag gtggccagca
    14641 gcgcctgtgc ctcgatgggg tcgcccagcg gtgttccggt gccgtgtgcc tccaccgcgt
    14701 cgacatccgc cgcggacagg ccggcgttgc gcagcgcgtc gcggatcacc cgctgctgcg
    14761 aggggccgtt gggagccgtc agccggctgc tcgccccgtc ctggttcacg gcggcgcccc
    14821 gtaccacggc cagcacctgg tgtccgttgc gctgggcgtc ggacagtcgc tccagcagca
    14881 gcacaccggc gccctcggac catccggtgc cgtcggccgc gttcgagaac gacttgcagc
    14941 gtccgtccac ggcgagtccg cgcaaccggc tgaactcgat gaacacccct ggtgtcgcca
    15001 tcatggccac gccaccggcg agcgcgagcg agcagtcgcc cttgtggagc gcctggacgg
    15061 ccagttggag cgccacgagc gacgaggagc atgccgtgtc cacggtcacg gcggggcctt
    15121 cgaagccgaa ggtgtaggag atacggccgg aggcgacgct tccggcgctg ccgttgccga
    15181 ggtggccttc gaggccctcg ggtatggact ggatgcggga gccgtagtcg ttgtacatca
    15241 cgccgacgaa cacgccggtg cggctgccac gcgcggacgc cgggtcgatg cccgcgcgct
    15301 ccagcgcctc ccatgaggtc tccagcagca gccgctgctg tgggtcggtc gccagcgcct
    15361 cacgcggact catgccgaag aaggaggcgt cgaagccgtt cgcgttgtgc aggaacccgc
    15421 cctcacgcgt atagctcttg cccggcttct ccgggtcggg gtcgtacagg tcctcgtccc
    15481 acccacggtc cacgggaagg tggacaccgc gtcggtgccc gaggcgacca gctcccacag
    15541 gtcttcgggc gactccactc cgccgggata cctgcacccc atgccg
//
