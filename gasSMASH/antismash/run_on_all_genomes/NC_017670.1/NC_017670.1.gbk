LOCUS       NC_017670               3329 bp    DNA     circular CON 15-JUN-2017
DEFINITION  Halobacillus halophilus DSM 2266 plasmid PL3 complete sequence.
ACCESSION   NC_017670
VERSION     NC_017670.1
DBLINK      BioProject: PRJNA224116
            BioSample: SAMEA2272168
            Assembly: GCF_000284515.1
KEYWORDS    RefSeq; complete replicon.
SOURCE      Halobacillus halophilus DSM 2266
  ORGANISM  Halobacillus halophilus DSM 2266
            Bacteria; Firmicutes; Bacilli; Bacillales; Bacillaceae;
            Halobacillus.
REFERENCE   1
  AUTHORS   Saum,S.H., Pfeiffer,F., Palm,P., Rampp,M., Schuster,S.C., Muller,V.
            and Oesterhelt,D.
  TITLE     Chloride and organic osmolytes: a hybrid strategy to cope with
            elevated salinities by the moderately halophilic, chloride-dependent
            bacterium Halobacillus halophilus
  JOURNAL   Environ. Microbiol. 15 (5), 1619-1633 (2013)
   PUBMED   22583374
REFERENCE   2  (bases 1 to 3329)
  AUTHORS   Pfeiffer,F.
  TITLE     Direct Submission
  JOURNAL   Submitted (01-MAR-2012) Max-Planck Institute of Biochemistry, Dept
            Membrane Biochemistry, Am Klopferspitz 18, D-82152 Martinsried,
            GERMANY
COMMENT     ##Genome-Annotation-Data-START##
            Annotation Provider               :: NCBI
            Annotation Date                   :: 06/15/2017 19:15:55
            Annotation Pipeline               :: NCBI Prokaryotic Genome
            Annotation Method                 :: Best-placed reference protein
            Annotation Software revision      :: 4.2
            Features Annotated                :: Gene; CDS; rRNA; tRNA; ncRNA;
            Genes (total)                     :: 4,112
            CDS (total)                       :: 4,020
            Genes (coding)                    :: 3,975
            CDS (coding)                      :: 3,975
            Genes (RNA)                       :: 92
            rRNAs                             :: 7, 7, 7 (5S, 16S, 23S)
            complete rRNAs                    :: 7, 7, 7 (5S, 16S, 23S)
            tRNAs                             :: 67
            ncRNAs                            :: 4
            Pseudo Genes (total)              :: 45
            Pseudo Genes (ambiguous residues) :: 0 of 45
            Pseudo Genes (frameshifted)       :: 21 of 45
            Pseudo Genes (incomplete)         :: 13 of 45
            Pseudo Genes (internal stop)      :: 22 of 45
            Pseudo Genes (multiple problems)  :: 10 of 45
            ##Genome-Annotation-Data-END##
            REFSEQ INFORMATION: The reference sequence was derived from
            HE717025.
            Accession numbers for all replicons in this genome project:
            HE717023, HE717024, HE717025.
            Annotation was added by the NCBI Prokaryotic Genome Annotation
            Pipeline (released 2013). Information about the Pipeline can be
            found here: https://www.ncbi.nlm.nih.gov/genome/annotation_prok/
            Annotation Pipeline
            set; GeneMarkS+
            repeat_region
            COMPLETENESS: full length.
            ##antiSMASH-Data-START##
            Version      :: 5.0.0-af0cea8(changed)
            Run date     :: 2019-06-07 14:46:24
            ##antiSMASH-Data-END##
FEATURES             Location/Qualifiers
     gene            1..>1137
                     /locus_tag="HBHAL_RS20295_LOWER"
                     /old_locus_tag="HBHAL_7001"
                     /pseudo=""
     CDS             1..>1137
                     /codon_start=1
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_017473913.1"
                     /locus_tag="HBHAL_RS20295_LOWER"
                     /note="internal stop; incomplete; partial in the middle of
                     a contig; missing stop; Derived by automated computational
                     analysis using gene prediction method: Protein Homology."
                     /old_locus_tag="HBHAL_7001"
                     /product="replication protein"
                     /pseudo=""
                     /transl_table=11
                     /translation="G"
     source          1..3329
                     /db_xref="taxon:866895"
                     /mol_type="genomic DNA"
                     /organism="Halobacillus halophilus DSM 2266"
                     /plasmid="PL3"
                     /strain="type strain: DSM 2266"
     gene            1238..1762
                     /locus_tag="HBHAL_RS20300"
                     /old_locus_tag="HBHAL_7002"
     CDS             1238..1762
                     /codon_start=1
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_014645402.1"
                     /locus_tag="HBHAL_RS20300"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /old_locus_tag="HBHAL_7002"
                     /product="hypothetical protein"
                     /protein_id="WP_014645402.1"
                     /transl_table=11
                     /translation="MTTDTKELKYEPSDMALMLEIKESTLRKYSLLLEKEGYTFHKGSV
                     GRRWYSDSDLMVLRRFMELKNGDMSLETACHAVVAWAKGRNIAPSVMENANDLRDSERY
                     LLDKLEKQNELIQELFARLDRQEEFFKQRDEWLIEKLKEQNEKPKLQLESETNSEQKPK
                     PTFLSKLFGHR"
     gene            3318..3329
                     /locus_tag="HBHAL_RS20295_UPPER"
                     /old_locus_tag="HBHAL_7001"
                     /pseudo=""
     CDS             3318..3329
                     /codon_start=1
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_017473913.1"
                     /locus_tag="HBHAL_RS20295_UPPER"
                     /note="internal stop; incomplete; partial in the middle of
                     a contig; missing stop; Derived by automated computational
                     analysis using gene prediction method: Protein Homology."
                     /old_locus_tag="HBHAL_7001"
                     /product="replication protein"
                     /pseudo=""
                     /transl_table=11
                     /translation="MIIG"
ORIGIN
        1 ggatgatcgg ggttcaagaa aggagtatta atactattta tgcactgtaa tcatagcact
       61 caaaccttga atgaatcaac tggtgaaata ttggttgatc aaacacaatc aggtaaagaa
      121 agagattgga agggtaaaaa ggaacggtca caacttacag ctgaacattt tgatctggcg
      181 ggactcacga gtaaagctga acgaatgaat gagtgtgcgg atacattggt gtttaaaaag
      241 acacctgaag cgttgaaact ataccaatca tggttctgta aagtgaggtt gtgtccaatg
      301 tgcaactgga gaagatcatt aaaaattgct tatcataata aaaagatcgt agagacggta
      361 aatgagcgtc aaaaggtccg ttggctattc ttaaccctta ccgttaagaa tacagactct
      421 gagagccttt ctgagacgat ttcagacatg ttcaagggtt tcaataggct ttctaagtat
      481 aaagccttta aaacgtctat aaagggcttt tttagagcct tggaagtcac aagggataga
      541 gatgagtaca ttacaaagaa aaggtatgat aataatccag catattataa gcgcaaagga
      601 ttagagattg ggtctgaaaa tccgaactac ggtacatatc atcctcattt tcatgttttg
      661 ctatgtgtac ctacaagtta cttcaaaaag aaaaattact acataacaca agatgagtgg
      721 acaaccttat ggaaacgcgc aatgaagctt gattattccc ctgtagtaaa cgtgaagaaa
      781 gttaaaccaa aggaaaactt agaagacgtt aaaacttatg aagatgactt taagagggcg
      841 atcaaggaac aaaatgctgt tttagaagtg tcgaagtatc ctgtaaaaga tacagatgtc
      901 atcaaaggaa gtgaggtcac agatgaaaat attgaaactg taaaagcgtt agatgctgct
      961 ttatcttata agcgtcttat tggttatggc ggattactta aagagatcca taaggagctt
     1021 aatttgggag atgcggaaga tggggatcta attaatattt ctgaggaaga tgaagtagcc
     1081 aatggtgttt ttgaggtagt cgcaaaatgg catcctggta tgaaaaatta tgtagtgtac
     1141 tgaaataaga ttaaaagagt catgccgaat ttggttatgg ctttttttaa agggctttta
     1201 ttagaatttg aagaatatat cagttgaggt gagcgacatg acaactgata caaaagagtt
     1261 gaaatatgag ccttcagata tggcacttat gcttgagata aaagaatcta cgttacgaaa
     1321 atatagtctt cttttggaga aagagggtta cacattccat aaaggatcag ttggacgaag
     1381 atggtatagc gatagtgatt taatggtgtt gcgccgattt atggagttaa aaaacggtga
     1441 catgtcactc gaaactgctt gtcatgcagt tgtagcttgg gcaaaaggta gaaatatcgc
     1501 accttctgtt atggaaaatg ctaatgattt gcgtgatagc gagcgttatc ttttagataa
     1561 attagaaaag caaaatgagt taattcaaga attatttgct cgcttagatc gacaggaaga
     1621 attttttaag caacgtgatg aatggttaat tgaaaagtta aaagagcaga atgaaaaacc
     1681 aaaattacaa ctggaatccg aaactaattc agagcaaaaa ccgaagccga cttttctaag
     1741 taaacttttt ggtcatagat aattttaaaa aagcccctca acccttttat gagttcggga
     1801 ggcttttttt tattgctccg attttttggt tttccccgaa gggttattca gctatgctga
     1861 acgtcaaaat ttcatcgaga gcagcgagaa aaaattaccc attaaaacca cgaaaaaaat
     1921 cgtgggggta aatgggcact gtgtggtttt ttaggataaa gaattaatgc taatttgcta
     1981 taaatgtgcg aaaaagattt ttcgttcgtt aaataaagta gagtcgcaaa gaaatacttc
     2041 ttcatgcgta ttaggaagta agggggagat aatcattatc cagaatggag aaaggttcgt
     2101 ttagaattct tttatgatat acgtaagcag gttgtgagga tctgatctcc gttataaatt
     2161 ttttaaattg gctatttact ttcttctgcc taatggttta gatggttttg aatcaaatgg
     2221 gagaggagtg agtttctatt tttttgcttt atttataatt gacccacctt gtttgtggca
     2281 aagaaacata atggagtgcc tataacataa caagatactt tttcgtgata gaacatggtg
     2341 ttgggtatgc ctatagttaa gttaaggggg tttttatggt tttagatcag tttttaagag
     2401 tgtcggtctt attctagtca agacgtgttt gaaagtggga taatattttg agggggtgga
     2461 cgagctatga aagcaaagat acttattgaa tttgaagtga gaaaagagga tgttcagcat
     2521 gcttttggct gggctgttct tggggcagaa gtcattcgct tttttttggg ataacgtaaa
     2581 aagagaggcg ttgacgcacc tcccttttta ctcacttttt ttctcacaaa aaagtagccc
     2641 cctattccgt gcaaagaaag ggaaagggct actttttatt ttatcacaat ctgaatgagt
     2701 gtttatgacg cagaaaaaaa cgtcccttgt tgcgggtggg aatccctcaa gtaaactaac
     2761 aaaggttgcc ccgcccaggg cgaatttccc ggggtggggt tgcctttggg agtttgaggg
     2821 atgccggcca ccctagcaac aaggaaacgg atttttttga gtaataaaaa gcgaatggag
     2881 cagataaggt atttttattg tttaaactgt gttagagatt gtagatcaga tgttttggtt
     2941 ctgaaatttg ttttttggtg ttgtttagta ttaactctcg taatagcgaa gagtttaaag
     3001 ttcttttgga ttatatgtta ggtcatatct ctaaagagga atttgattct tttatggaaa
     3061 aattcttaga agaagatgat aaattagatg attaaaaaac tcctattttt aggggttttt
     3121 taacgttctt gaaacgggtt ttctcttatc ttgatacata tagaaacaat gagattctta
     3181 attttagcct ttcagccttg atttgatagg gttttagcta aaaaatcatg gtggatagaa
     3241 aaaagcccct gtgttatggt tatgttgtcg agacaaaacc aataaaacat aggggctttt
     3301 gcacccttct gaacctgatg atcattggc
//
