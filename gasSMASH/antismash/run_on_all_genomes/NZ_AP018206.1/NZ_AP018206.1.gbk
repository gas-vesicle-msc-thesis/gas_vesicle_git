LOCUS       NZ_AP018206            13170 bp    DNA     circular CON 29-SEP-2017
DEFINITION  Leptolyngbya boryana NIES-2135 plasmid plasmid3 DNA, complete
            genome.
ACCESSION   NZ_AP018206
VERSION     NZ_AP018206.1
DBLINK      BioProject: PRJNA224116
            BioSample: SAMD00079812
            Assembly: GCF_002368255.1
KEYWORDS    RefSeq.
SOURCE      Leptolyngbya boryana NIES-2135
  ORGANISM  Leptolyngbya boryana NIES-2135
            Bacteria; Cyanobacteria; Synechococcales; Leptolyngbyaceae;
            Leptolyngbya.
REFERENCE   1
  AUTHORS   Hirose,Y., Shimura,Y., Fujisawa,T., Nakamura,Y. and Kawachi,M.
  TITLE     Genome sequencing of cyanobaciteial culture collection at National
            Institute for Environmental Studies (NIES)
  JOURNAL   Unpublished
REFERENCE   2  (bases 1 to 13170)
  AUTHORS   Hirose,Y., Shimura,Y. and Fujisawa,T.
  TITLE     Direct Submission
  JOURNAL   Submitted (23-JUN-2017) Contact:Yuu Hirose Toyohashi University of
            Technology, Department of Environmental and Life Sciences;
            Hibarigaoka-1-1 Tenpakucho, Toyohashi, Aichi 441-8122, Japan URL
            :http://mcc.nies.go.jp/
            strainList.do?strainId=2527&strainNumberEn=NIES-2135
COMMENT     ##Genome-Assembly-Data-START##
            Assembly Method                   :: GS De Novo Assembler v. 2.9
            Genome Coverage                   :: 59x
            Sequencing Technology             :: Ilumina MiSeq
            ##Genome-Assembly-Data-END##
            ##Genome-Annotation-Data-START##
            Annotation Provider               :: NCBI
            Annotation Date                   :: 09/28/2017 13:21:42
            Annotation Pipeline               :: NCBI Prokaryotic Genome
            Annotation Method                 :: Best-placed reference protein
            Annotation Software revision      :: 4.2
            Features Annotated                :: Gene; CDS; rRNA; tRNA; ncRNA;
            Genes (total)                     :: 6,757
            CDS (total)                       :: 6,674
            Genes (coding)                    :: 6,491
            CDS (coding)                      :: 6,491
            Genes (RNA)                       :: 83
            rRNAs                             :: 4, 4, 4 (5S, 16S, 23S)
            complete rRNAs                    :: 4, 4, 4 (5S, 16S, 23S)
            tRNAs                             :: 67
            ncRNAs                            :: 4
            Pseudo Genes (total)              :: 183
            Pseudo Genes (ambiguous residues) :: 0 of 183
            Pseudo Genes (frameshifted)       :: 85 of 183
            Pseudo Genes (incomplete)         :: 89 of 183
            Pseudo Genes (internal stop)      :: 44 of 183
            Pseudo Genes (multiple problems)  :: 29 of 183
            CRISPR Arrays                     :: 5
            ##Genome-Annotation-Data-END##
            REFSEQ INFORMATION: The reference sequence was derived from
            AP018206.
            NBRP Genome Information Upgrading Program.: Genome sequencing of
            cyanobaciteial culture collection at National Institute for
            Environmental Studies (NIES).
            Annotated using prokka 1.11 from http://www.vicbioinformatics.com.
            Annotated at D-FAST https://dfast.nig.ac.jp
            Annotation was added by the NCBI Prokaryotic Genome Annotation
            Pipeline (released 2013). Information about the Pipeline can be
            found here: https://www.ncbi.nlm.nih.gov/genome/annotation_prok/
            Annotation Pipeline
            set; GeneMarkS+
            repeat_region
            COMPLETENESS: full length.
            ##antiSMASH-Data-START##
            Version      :: 5.0.0-af0cea8(changed)
            Run date     :: 2019-06-07 15:12:16
            ##antiSMASH-Data-END##
FEATURES             Location/Qualifiers
     source          1..13170
                     /culture_collection="NIES:2135"
                     /db_xref="taxon:1973484"
                     /mol_type="genomic DNA"
                     /organism="Leptolyngbya boryana NIES-2135"
                     /plasmid="plasmid3"
                     /strain="NIES-2135"
     gene            complement(199..1785)
                     /locus_tag="CA740_RS33735"
                     /old_locus_tag="NIES2135_68150"
     CDS             complement(199..1785)
                     /codon_start=1
                     /inference="COORDINATES: ab initio prediction:GeneMarkS+"
                     /locus_tag="CA740_RS33735"
                     /note="Derived by automated computational analysis using
                     gene prediction method: GeneMarkS+."
                     /old_locus_tag="NIES2135_68150"
                     /product="hypothetical protein"
                     /protein_id="WP_096735614.1"
                     /transl_table=11
                     /translation="MMKLSTSQFNPAQNSESNFYALFPHRYDYLWAEHPAPGERPQWQT
                     QSKHPLSDRLLQQGAFLYGVRFGSRTNYVLIDIDKNSAYHPSRDPFAIQRLTGALEQIG
                     LVSYVAVTSSHSGGLHIYFPFTAEQKSYQMGRVVQVIAQNAGFKVKPGHLELFPNARPY
                     QEKLSLYAGHRLPLQLGSYLLGPDFEPIFTIEAEFVRQWQFAQARNDISTEVIDQVWKT
                     AHRQERAITKNAAKFLNDLDAEIEPGWTGTGQTNWILSKVADREFIFRHVLSGGEPLEG
                     ESLVAAILQVVTSLPGYKEFCRHQHEIENRVRDWARCVEKHRYHYGGTKAKLMGNSSPQ
                     SKPPNQNALKAEDAQRRIREAVADLIAKDEFPSAITARRKAIERYGISPTTLNKYRSLW
                     HPNELDHTPKDQPEKVPAEKSSEPLSNKDVQTLSLNKFLDTSAAPLEQAEVLNDRIGGC
                     GGSSIPPALKQKILAKLEQARRGHSVVSIETKRQQRASEQYQAKLTRWLESGDPILVKE
                     ARERLGLPPLT"
     gene            complement(2245..2505)
                     /locus_tag="CA740_RS33740"
     CDS             complement(2245..2505)
                     /codon_start=1
                     /inference="COORDINATES: ab initio prediction:GeneMarkS+"
                     /locus_tag="CA740_RS33740"
                     /note="Derived by automated computational analysis using
                     gene prediction method: GeneMarkS+."
                     /product="hypothetical protein"
                     /protein_id="WP_096735615.1"
                     /transl_table=11
                     /translation="MNGREGELNQLFSQWDDTFCNSMMTISAVDRLVHHALIFDIQAES
                     FRRQTADKRSQRKSNRGCDPFLGMEFCGLEQKFCSKHRCCR"
     gene            2656..3270
                     /locus_tag="CA740_RS33745"
                     /old_locus_tag="NIES2135_68160"
     CDS             2656..3270
                     /codon_start=1
                     /inference="COORDINATES: protein motif:HMM:PF07015.9"
                     /locus_tag="CA740_RS33745"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /old_locus_tag="NIES2135_68160"
                     /product="ParA family protein"
                     /protein_id="WP_096735616.1"
                     /transl_table=11
                     /translation="MIITIASFKGGVGKTTTALHLAQYLGKRRGNRNVVLLDGDPNRSA
                     LSWYERGQQRAAFSVMDGDQEPSVFDHLIIDTPARTDPTELLPVADASDLLIIPSEISI
                     FSLEATISTVDVLRSLSENKYRILLTMLPTRGLRREASAREALKQSGLIVFNAGIKNRA
                     VYQDAALEGLPVGELHSRAATSAWSDYQVLGKEICKNWGTK"
     gene            3267..4043
                     /locus_tag="CA740_RS33750"
                     /old_locus_tag="NIES2135_68170"
     CDS             3267..4043
                     /codon_start=1
                     /inference="COORDINATES: ab initio prediction:GeneMarkS+"
                     /locus_tag="CA740_RS33750"
                     /note="Derived by automated computational analysis using
                     gene prediction method: GeneMarkS+."
                     /old_locus_tag="NIES2135_68170"
                     /product="chromosome partitioning protein ParB"
                     /protein_id="WP_096735617.1"
                     /transl_table=11
                     /translation="MSASKLTGALAIAKSRSVDTPSSSSTLSLDNISERVEGNSRPLNP
                     AHIEELANSIAVVGLIAPIAVDIQGRLLAGGHRRAAILLLRDQDSKTFAKQFPNEQVPI
                     RRYDFDSTIEPEKALAIEASENEKRRDYTPAEVRALADRLRAAGYRDASGKPKKGEKAL
                     RPALELIVGKSIRQVRRYLNDGANASAKNGLQKLDKQSRSDVRILSRALISLQKWADLA
                     EDYENPGKATKDLKGQVNRMIRKIEGTIAELEAEEK"
     gene            complement(4128..4508)
                     /locus_tag="CA740_RS33755"
                     /old_locus_tag="NIES2135_68180"
     CDS             complement(4128..4508)
                     /codon_start=1
                     /inference="COORDINATES: ab initio prediction:GeneMarkS+"
                     /locus_tag="CA740_RS33755"
                     /note="Derived by automated computational analysis using
                     gene prediction method: GeneMarkS+."
                     /old_locus_tag="NIES2135_68180"
                     /product="hypothetical protein"
                     /protein_id="WP_017288256.1"
                     /transl_table=11
                     /translation="MLRQLLSASALMLATVPALSMSAIAASAPRVTVFNQGGYVADYQI
                     NYSINGQRKDFKVSGIIVGGKRTVTLPLGSQNITVRGQMQTGLFWEPRREIFNQSARDG
                     SCFKTFGTIFRGEWSRDCTADF"
     gene            5005..>6507
                     /locus_tag="CA740_RS33760"
                     /old_locus_tag="NIES2135_68190"
                     /pseudo=""
     CDS             5005..>6507
                     /codon_start=1
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_015158909.1"
                     /locus_tag="CA740_RS33760"
                     /note="incomplete; partial in the middle of a contig;
                     missing stop; Derived by automated computational analysis
                     using gene prediction method: Protein Homology."
                     /old_locus_tag="NIES2135_68190"
                     /product="IS21 family transposase"
                     /pseudo=""
                     /transl_table=11
                     /translation="METRRVSGKVKIYQVQVYMTAREKGFTQVEAAELAGFSARTGQRI
                     EAGDHQPNRGRIRDWRSTPDPLAEVWESELEPMLKANPGLQPTTLFEWLQERYPGKYPQ
                     VLRTVQRRVAAWKALHGEPKEIMFELRHEPGMMGLSDFTELKGIEIRIAGKPFEHLLYH
                     YRLAYSGWQYAQIIQGGESFIALSEGLQNALFACGGVPKQHRTDSLSAAYRNMGGARNK
                     PLTRLYDDLCSHYRMQPTRNNTGIAHENGGIESPHGHLKNRIKQMLLLRGSHDFDSIAD
                     YQDLINRAIAKLNDLHPRKIEEEKQYLQPLPKYRVPDYEVLTATVSCRSTIDVRCVLYT
                     VPSRLIGQSLELHLYHDRIIGYLNRHSVVELPRIRVSDKAKRRARCINYRHIAEGLRLK
                     PRAFLYCTWQQELLPNQIWRELWSQLKCRFDLDSAAVLMVEALYIATTQDKEIAVASYL
                     QAQLNANSLTLATLRKQFQLLSDLSLPNLTTTQHNLEHYDQLL"
     gene            6578..7318
                     /locus_tag="CA740_RS33765"
                     /old_locus_tag="NIES2135_68200"
     CDS             6578..7318
                     /codon_start=1
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_015146091.1"
                     /locus_tag="CA740_RS33765"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /old_locus_tag="NIES2135_68200"
                     /product="DUF815 domain-containing protein"
                     /protein_id="WP_096735583.1"
                     /transl_table=11
                     /translation="MLHHWETLEQQAMQEGWSYAQFLLVLCESEVQHRWSNRIQRALRE
                     AQLPSGKTVSNFDFSHCPSFNPAPLMQMADDPTWLGRAENLLLFGASGVGKSHLASAVS
                     RRMVEFGKRVRFFSALALVQQLQQAKLQLQLQAMLKKLDRFDLLVLDDLSYVKKTEAET
                     SVLFELIAHRYERKSLLITANQPFSQWDAIFTDSMMTVAAVDRLVHHALIVEIQTESYR
                     KQSAVSRSEASKTAKKETVQPKRS"
     gene            complement(7523..7999)
                     /locus_tag="CA740_RS33770"
                     /old_locus_tag="NIES2135_68210"
     CDS             complement(7523..7999)
                     /codon_start=1
                     /inference="COORDINATES: protein motif:HMM:PF13546.4"
                     /locus_tag="CA740_RS33770"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /old_locus_tag="NIES2135_68210"
                     /product="hypothetical protein"
                     /protein_id="WP_096735618.1"
                     /transl_table=11
                     /translation="MTFALSEIALSQPEVEADLKQLETWTNDFNALMTRLAPRFKRAEL
                     RERVKDHLQGLFSSAECKNSWQLAEQLGQSTLYGIQHLLGRAQWSADAVRDDLQTYVNE
                     ALGDPEAVFVVDETGFLKKGDHSAGVQPQYSGTAHQVKNCQIGVFLGYAARDGC"
     rRNA            complement(8257..8373)
                     /db_xref="RFAM:RF00001"
                     /gene="rrf"
                     /inference="COORDINATES: nucleotide
                     motif:Rfam:12.0:RF00001"
                     /inference="COORDINATES: profile:INFERNAL:1.1.1"
                     /locus_tag="CA740_RS33775"
                     /note="Derived by automated computational analysis using
                     gene prediction method: cmsearch."
                     /product="5S ribosomal RNA"
     gene            complement(8257..8373)
                     /gene="rrf"
                     /locus_tag="CA740_RS33775"
     rRNA            complement(8488..11376)
                     /locus_tag="CA740_RS33780"
                     /old_locus_tag="NIES2135_68220"
                     /product="23S ribosomal RNA"
     gene            complement(8488..11376)
                     /locus_tag="CA740_RS33780"
                     /old_locus_tag="NIES2135_68220"
     rRNA            complement(11657..13142)
                     /locus_tag="CA740_RS33785"
                     /old_locus_tag="NIES2135_68230"
                     /product="16S ribosomal RNA"
     gene            complement(11657..13142)
                     /locus_tag="CA740_RS33785"
                     /old_locus_tag="NIES2135_68230"
ORIGIN
        1 ttgaaacttt cttacgtttc ggaaagtttt cttgtttcgt cttgaaaatc tatttcaaga
       61 cttaaatgtt taacgagagc tgatgctctt tctggcttcc aaactatgtt gttgtctagg
      121 ttcattgcgt caactttcgt tttccgacgc tttcccacta tagcatgatg atcaggaaag
      181 ctgtcaagaa attagatctc atgtcagtgg tggcagtcca agccgctctc tcgcttcttt
      241 aactaggatt ggatcaccag attctaacca tcgtgtcaat ttagcttgat attgttcgct
      301 agcacgttgt tgacgtttag tttcgatcga aacaacggag tgccctcggc gtgcttgctc
      361 aagcttagcc aaaatctttt gctttaaggc tggcgggatc gaactccccc cgcacccccc
      421 tattcgatcg ttgagtactt ctgcttgctc tagaggagca gcagaagtat ctaaaaactt
      481 atttaggctt agggtctgta catccttatt tgataagggt tctgatgatt tttctgcagg
      541 gactttttct ggctgatctt taggagtgtg atctaactcg ttgggatgcc aaagggatcg
      601 atatttgttc agcgttgttg gactgatgcc ataacgctcg atcgcttttc gtcgtgctgt
      661 aatagcgcta ggaaattcgt ctttagcaat aagatccgca acagcttcac gaattcgccg
      721 ttgtgcatct tcggctttga gagcattttg attcgggggt ttagactgcg gactagagtt
      781 acccataagt ttcgctttcg ttccaccgta atgatatcga tgtttttcta cacaccgcgc
      841 ccagtctcgt actcggtttt caatctcgtg ctgatgacga caaaattctt tgtagccagg
      901 cagtgacgtg acaacttgaa gaatggcggc aacgagagat tctccttcga ggggttctcc
      961 gcctgagagc acatgacgga agatgaattc gcgatcggcg actttactga gaatccagtt
     1021 ggtctgtcca gttccagtcc agccgggttc aatttctgcg tctaggtcgt tgagaaattt
     1081 cgcggcgttc tttgtgatgg cacgttcttg tcggtgggca gttttccaaa cttggtcgat
     1141 gacttctgtg ctgatatcgt tgcgggcttg ggcaaattgc cactgccgta caaattcggc
     1201 ttctatcgtg aagattggct caaagtctgg gccgaggagg taagagccga gttggagtgg
     1261 aaggcggtgt cctgcgtaaa gactgagttt ttcttgatag ggacgggcat taggaaaaag
     1321 ttccagatgt ccgggtttga ctttgaaccc cgcattttga gcgatcactt ggacgactct
     1381 gcccatttgg taagattttt gctcagccgt aaagggaaag tagatgtgca acccgccgct
     1441 gtgactggaa gtgacggcaa cataagagac gagaccaatc tgctccaatg cgccagtcag
     1501 tcgctgaatc gcaaatggat cgcggctggg atgatacgcg ctatttttgt cgatgtcgat
     1561 tagaacgtaa tttgtcctgg aaccaaaacg cacgccatag agaaacgctc cctgttggag
     1621 gaggcgatcg ctcaggggat gtttgctttg agtttgccac tgcgggcgtt ctcccggtgc
     1681 ggggtgctct gcccacaaat aatcgtatcg gtgcgggaag agcgcataga agttggattc
     1741 ggaattctgg gcgggattga attgagaggt agaaagtttc atcattactc cttctgggag
     1801 ccgaaccttg gatgaaaatt caactttctt gtgtcatctc tcgactgatg cgatacaatc
     1861 ctgtcagata gtatcatctc tatcagcgtt aagctgactg tgaaagtcga gtcatccttc
     1921 cggggcttat aaagtttctc aggcttactt accctctggt ttcggctcgg cttttgccgt
     1981 tttaaacctt gttttctcga ctatttcaaa aaataatctg catagcatca tacaactgat
     2041 ccaggaattt acaaggaaat ccttacaggg attagatcag ctacattttt tgagatccaa
     2101 atttcttaac aattcttgaa tctgcggaag cccttcaact gtcacaccga agatcaggat
     2161 agactctcgg actaaaaatg gcatacatcg ctttttggtt atattttgag tcccgcattt
     2221 caccgttaaa gaactgtcac aacctcaacg gcaacatcgg tgttttgagc agaacttctg
     2281 ctctaatccg cagaactcca tcccaagaaa aggatcgcag cctctgtttg atttgcgttg
     2341 agatcgcttg tctgctgttt gcctacgaaa actctcggct tgaatgtcaa aaatcagcgc
     2401 atgatgcacc aggcggtcaa ccgcagagat cgtcatcatt gagttgcaga atgtatcgtc
     2461 ccactgactg aagagttgat tcaattcccc ttctcgccca ttcatattca tttagatttg
     2521 ttggtattga atatatttcc aggcactgat tcgtcctcgt tatagaacgt tctcagtgtc
     2581 ttttttggct tgaaatcttt gacggattcc tcggcgtgtg caaaatggca atgtgttcca
     2641 gctagaatta gtaccatgat tattacgatc gccagcttca agggtggtgt aggtaaaaca
     2701 accacagcat tgcatttagc tcaatattta gggaaacgtc gaggcaaccg caatgttgta
     2761 ttactagatg gagatcccaa taggagtgct ttgagttggt atgagcgtgg gcagcaacgt
     2821 gcagcatttt ctgtgatgga tggcgaccag gaaccatcag tgttcgatca tttgattatt
     2881 gacacacctg cccgtacaga tccaactgag cttctacctg tagcagatgc aagtgacttg
     2941 ctgattatcc cttcggagat ttccatcttt tcattagaag ctacaatctc aactgtagac
     3001 gttttacgat ctctctctga aaacaaatat cgaattctct taactatgct tcctacacga
     3061 ggtctcagac gagaagccag tgcgagggag gcattgaagc aatctggtct gatagtgttc
     3121 aacgccggaa ttaaaaaccg agctgtttac caagatgctg cgctagaagg tttgccggta
     3181 ggagaattac atagtagggc tgccacttct gcttggagcg attatcaagt tttaggcaaa
     3241 gaaatttgta aaaattgggg aacaaaatga gtgcgagtaa gttaactgga gcattagcaa
     3301 tcgccaaaag ccgcagtgta gacacacctt catcctcctc aacgctatca ctcgataaca
     3361 tttctgagcg agtggaagga aatagccgtc cgcttaaccc tgctcatatt gaagaattag
     3421 ccaatagcat tgcagtagtg ggactaattg ccccgatcgc agtagacatt cagggacgat
     3481 tattagctgg agggcatcga cgtgcagcaa ttttgttatt gcgcgatcaa gattcgaaga
     3541 ccttcgcaaa gcagtttcct aatgaacaag tccccattcg tcgttatgac tttgacagca
     3601 ccatcgaacc ggaaaaagcc ttagcaatag aggcgagcga gaatgagaag cgaagagact
     3661 ataccccagc agaagtaaga gcgctcgcag atcgattaag agcagcaggt tatcgagatg
     3721 cgtctggaaa acccaagaaa ggagaaaaag ctctacgtcc tgccttggaa ttgattgttg
     3781 gaaagtcgat taggcaggtg agacgctatc tgaatgatgg tgcaaatgct tctgctaaaa
     3841 acgggttgca aaaactggac aagcaaagtc ggtcagacgt ccgaattctg agccgagcac
     3901 tcatcagctt acaaaaatgg gcagatttag ctgaagacta tgagaatcca ggtaaagcca
     3961 cgaaggatct caaaggtcaa gtgaatcgca tgattcggaa aattgaaggc acgatcgcag
     4021 agttggaagc cgaggaaaaa taagtactcc tgattgatgt tgaagcgata gctctgcaaa
     4081 aaagcgcagg attcttgcag agccttagat atatttggtg tatcaatcta gaaatcagct
     4141 gtgcaatcgc gtgaccattc tcctctgaag atagtaccga aagtcttgaa gcagcttcca
     4201 tcacgagcag attgattgaa gatttctcgt ctcggctccc agaacaaccc ggtttgcatt
     4261 tgaccccgca ctgtaatgtt ttggctgcca agtggcaatg ttactgttcg tttaccacca
     4321 acaatgattc cagagacttt gaagtcttta cgttgtccat taatgctgta gttaatttga
     4381 tagtctgcga catatccacc ttgattgaat acggtgactc gtggggctga tgcagcgatc
     4441 gcagacattg agagggcagg aacagtcgca agcattaacg cggaggcaga aagaagttga
     4501 cgtagcatgt tggttctcct ggatttgttc gtagtttcga gcttctgagg agatacacgt
     4561 ttgtaagttt gggtttatgc aggtttgctg aaaagaacgt cgtgattcta gcgatcgcta
     4621 gacgcgatcg cggatttagg acatcagaaa gtgagttgcc tactaaatgt caatcacaaa
     4681 gtcgagtcgc gacagtgacg aagacgtgtg agtaaacacc aaaagaccta tctagataat
     4741 tcaacgcatc aattgtcgct caacaggtga cctctcgcgg gatgccagct tcagcacatc
     4801 gtgctggatc atcgatccaa ctttgcggca gatacagctc tcgatctatc aacgtatacc
     4861 cgtctgttag gtgacaatac aactggcgaa cgaacgtcag ggtattggcg agctaaacct
     4921 ttctccggac gattgcgtac tgcttgactc gacgagtcaa gtgccgagca agaaaaagac
     4981 actctgagag agttccatcg atacatggag acgcgaagag tgtctggaaa agtcaaaata
     5041 tatcaagtcc aagtgtatat gaccgcacga gaaaaaggct tcacgcaagt agaagccgca
     5101 gaactggctg gcttttctgc ccgaaccgga cagcgcatcg aagcaggcga ccatcaaccg
     5161 aatcgaggac gcatccgtga ctggcgcagc acccctgatc cgttagcaga agtttgggaa
     5221 tctgaacttg aacccatgct caaagcaaat cctggtcttc agccgacaac gctatttgaa
     5281 tggctgcaag agcgctatcc cggcaagtat ccccaagtgc ttcgcacggt gcaacgacga
     5341 gttgccgcct ggaaagcctt gcatggcgaa ccgaaagaga tcatgttcga gctacggcat
     5401 gaaccgggga tgatgggctt atcggatttc accgaactca aaggaattga gatcaggatt
     5461 gcgggcaagc catttgagca tttgttgtat cattaccgac tggcttacag tggctggcag
     5521 tacgctcaga tcattcaggg cggtgagagc tttattgcat tgtcggaagg attacagaat
     5581 gcgctgtttg cttgtggagg agtcccgaag caacatcgta ccgatagtct cagtgcggct
     5641 tatcgcaaca tgggtggagc gcgaaataag cccttgacgc gcttgtatga tgacctatgc
     5701 agtcattacc gaatgcaacc aacgcgcaac aatactggta tcgctcatga gaatggaggg
     5761 atagagtcac cgcacggaca tttaaagaat cgcatcaagc aaatgctctt acttagaggc
     5821 agtcatgact ttgacagcat cgctgactat caagacctga tcaatcgagc gattgccaaa
     5881 ttgaacgatc tccatcccag aaaaattgag gaggagaaac aatatttgca acccttgcct
     5941 aagtatcgag tgcctgacta tgaggtgctc accgcaacgg tcagttgccg cagtacgatt
     6001 gatgtgcgct gtgtgctcta cacggttcca tctcgactga ttggacagtc actcgaactg
     6061 catctctacc atgaccggat tatcggctac ctcaatcgtc actcagtcgt ggagctccct
     6121 cgcattcgag tcagtgataa agcaaaacgg cgcgctcggt gcatcaacta tcggcatatt
     6181 gcagaagggc tacgtctcaa gcccagagct ttcttgtact gtacctggca acaagaactt
     6241 ctgccaaatc agatatggcg ggagctttgg tcgcaactca aatgccggtt cgacctcgac
     6301 agtgctgcgg tgctgatggt agaagctctg tacattgcga ccacgcaaga caaagaaatc
     6361 gcagtagcaa gctacctgca agcacaactc aatgcaaata gcctgaccct ggctacactc
     6421 cgcaagcagt ttcaactctt gagcgatcta agcctgccca atctcacgac gactcaacac
     6481 aatttagaac actatgacca actcttgcac cgagaacccg cccaccagtc cgtatcagaa
     6541 tctcagcctc cacctcaaac gactgcacct acctcacatg ctgcatcact gggaaacact
     6601 cgaacaacaa gcgatgcagg aaggctggtc ttacgcacag ttcttgctgg ttctgtgcga
     6661 atcggaagtc caacaccgat ggagcaaccg tatccaacgc gctctgagag aagcccaact
     6721 gccaagcgga aaaacggttt ccaactttga cttttcccat tgtccgagct tcaaccctgc
     6781 tcccttgatg cagatggcgg atgatcctac ttggctcgga cgcgccgaaa accttttgtt
     6841 gttcggagct tcaggcgttg gaaagtcaca tttagcaagc gctgtctcgc gccgcatggt
     6901 ggagtttggt aagcgagtca ggttcttttc tgccttagct ctagtccagc aattacagca
     6961 agcaaagctg caactgcaat tgcaagcaat gctgaagaag ctagaccgct ttgacctact
     7021 cgtgcttgat gatttgagct atgtcaaaaa gactgaggcg gaaacctccg tgctgtttga
     7081 gctaattgct catcgatatg agcgcaaaag cttactgatt actgccaacc agcccttcag
     7141 ccaatgggat gctatcttca ccgattcgat gatgacagtc gcagccgttg accgtttggt
     7201 gcatcatgct ttgattgttg aaattcagac cgagagctac cgcaaacaat cggcagtgtc
     7261 tcgttctgaa gcatcgaaga ccgccaagaa agagaccgtt caaccgaaac gttcctagct
     7321 gtcgtttcga tcttaattgt cgtctcgatc ttcgttgtca ttttgatcat cattgtcgtg
     7381 cagttacctt gagcattctg cacaacttta atctgtcatt ctttagcgat tactgttctt
     7441 gatctcgcga ctaaccaaga atgttcttag ttcttactag cccgtctttg tcactgtcgc
     7501 gactcgaact tgtgcttgac atctaacacc cgtctcttgc cgcatagccc agaaatacgc
     7561 caatctgaca gttcttaacc tgatgcgcgg tgccactgta ctgaggttgc actccagcgg
     7621 aatgatctcc tttcttcaga aagccagtct catctaccac aaataccgct tctggatctc
     7681 cgagggcttc attcacatac gtttgcaagt catctctgac tgcatctgcg ctccactggg
     7741 ctcgacctaa gagatgctga atcccgtaaa gcgtggattg accgagttgt tcggccagct
     7801 gccagctatt cttgcattct gctgaactga acaatccttg cagatggtct ttcactcgct
     7861 cgcgaagctc tgcgcgtttg aagcgaggcg caagacgagt catcagcgca ttgaaatcat
     7921 ttgtccaggt ctctagctgt ttgaggtcag cttcaacctc cggttgactc agcgcaattt
     7981 cggaaagggc aaaagtcatg agacacaccc taaaacgctt tcttgagctt gattctaagc
     8041 aactgcaatc ttgatttcaa tatctacaac tgtagtgccg gtcaagttga actagaaatt
     8101 ttggcaagat agttgacgtt tcatagacaa ctcaacaact ttctggcgat tgaggtagcc
     8161 cacaatccag gcatagtaca gatgaaattc cagtgaagaa agcgaaaagc cgaacgtagt
     8221 catgcttgta actacgctcg gcttctctaa aaaacaccct ggcggttagc tattttgaca
     8281 ggaggctacc ctccaactat cgtcgccgca aatgcgtttc accactcagt tcgggatgga
     8341 tgagtgtggg tccacatcgc catcaccacc aggaaactcg ttgagtctgg ctcaaattca
     8401 ccagaaccca gaaggctgca tagtttttga tgtcagtttt gaatgtagaa atctcaaaca
     8461 ccaaggctta gattcaatct gattcaagag gtcaagccct cggtctatta gtactcctcg
     8521 gctgcataca ttactgcact tccacctaga gcctatcaac gcgtgttctc cgcgtgacct
     8581 tactggttta acaccatgag agtactcatc ttgaggtggg cttcccactt agatgctttc
     8641 agcggttatc cactccgcac ttggctaccc agcgtttacc gttggcacga tagctggtac
     8701 accagcggtg cgttcttccc ggtcctctcg tactaaggaa gactcctctc aatactcttg
     8761 cgcctgcacc ggatatggac cgaactgtct cacgacgttc tgaacccagc tcacgtaccg
     8821 ctttaatggg cgaacagccc aacccttggg acgtactacc gccccaggtt gcgatgagcc
     8881 gacatcgagg tgccaaacct ccccgtcgat gtgaactctt gggggagatc agcctgttat
     8941 ccctagagta acttttatcc gtttagcgac ggcctttcca cgcagcgccg tcggatcact
     9001 aaagccgact ttcgtccctg ctcgacatgt tcgtctcaca gtcaagctct cttgtgcttt
     9061 tacactctac ggctgatttc caaccagcct gagagaacct ttgcgcgcct ccgttaccat
     9121 ttaggaggcg accgccccag tcaaactgcc cacctgaaac tgttccctcc ccggattacg
     9181 ggcgaaggtt agaattctag ccttaacaga gtggtatctc accgttggct cctcaatccc
     9241 cacaaggatt gaatcatagc ctcccaccta tcctgcgcag ttaaagcccg aacccaattc
     9301 caggctacag taaagcttca tagggtcttt ctgtccaggt gcaggtagtc cgtatcttca
     9361 cagacaatcc tatttcgccg agcctctctc cgagacagcg cccaaatcgt tacgcctttc
     9421 gtgcgggtcg gaacttaccc gacaaggaat ttcgctacct taggaccgtt atagttacgg
     9481 ccgccgttca ccggggcttc agtcgctagc ttcaggacga atccctgacc aacttcttta
     9541 accttccggc actgggcagg cgtcagcccc catacatcgt tttgcaactt agcggagacc
     9601 tgtgtttttg gtaaacagtc gcttgggcct cttcactgcg accacctctc ggtggcaccc
     9661 cttctcccga agttacgggg tcattttgcc gagttcctta gagagagtta tctcgcgccc
     9721 tttagttttc tcaaccatcc tacctgtgtc ggtttcgggt acgggtaatt tgaattatcg
     9781 tgatccggga ttttcttgga agcctgacat cacacacttc gagtccgtag actctcgtac
     9841 tcacacctca gctcaagtcg tttccgccga ctctcatcac ctcgaatgct tgaaccccta
     9901 accaacatgg ggctgtgtta gccttctccg tcccccgtca caatccaaat tgagtactgg
     9961 aatcttcacc agttgtccat cgactacgtc tttcgacctc gccttaggcc ccgactcacc
    10021 ctccgcggac gagccttccg gaggaaccct taggatttca gggcattgga ttctcaccaa
    10081 tgtttgcgct actcaagccg acattctcac ttctgcttcg tccacacctg cttgccgctg
    10141 atgcttctca ctacaacaga acgctcccct accactcaca taaatgcaag tccatagctt
    10201 cggtaaacac cttagccccg ttcattttcg gcgcaggaac gcttgaccag tgagctatta
    10261 cgcactcttt taaggatggc tgcttctagg caaacctcct ggttgtctct gcattcccac
    10321 ctcctttatc actgaggtgt tattttggga ccttagctga tggtctgggc tgtttccctc
    10381 ttgacgatga agcttatccc ccaccgtctc actggcaatc tattcatctg gtattcagag
    10441 tttgactcga tttggtaccg gtctcccagc ccgcaccgaa tcagtgcttt acccccagac
    10501 ttaaacaatt accgctgcgc ctcaacacat ttcggggaga accagctagc tcccggttcg
    10561 attggcattt cacccctaac cacacctcat ccgctgattt ttcaacatca gtcggttcgg
    10621 acctccactt ggtgttaccc aagcttcatc ctggacatgg ttagatcacc ggggttcggg
    10681 tctataaata cagatatcgc gccctattca gactcggttt ccctttgact tcggcatctc
    10741 cgccttaatc tacctgtacc tataagtcgc cggctcattc ttcaacaggc acacggtcag
    10801 acgttcaatc gtcctcccat tgcttgtaag cagatggttt catgttctat ttcactcccc
    10861 tcccggggtt cttttcaccg ttccctcgcg gtactagttc tctatcggtc acacaggagt
    10921 atttagcctt tcgaggtggt cctcgatgat tcacacggaa tttcacgtgc tccatgctac
    10981 tcgggataca gctagctcag ttaagttttc aactacagga ctttcacctt ctctggtgta
    11041 gcttctcact acttcgttta accgctctga tacacgttgc tgtcccacaa ccccagatga
    11101 taaatcacct ggtttaggct gtttccagtt cgctcgccgc tactacggaa atcgcgtttg
    11161 ctttctcttc ctccagctac taagatgttt caattcgctg ggttagcttg ctctacccta
    11221 tggattcagg tagtcatgtt aaaggttgcc ctattcggaa atcttcggat caatgcttac
    11281 ttccagctcc ccgaagcata tcgtcggtcg tcacgtcctt catcgcctct gtgtgcctag
    11341 gtatccacca tctgcccttt gtagcttgac cactaattat ttcaatcagt tttagtcctt
    11401 tacattggtg tctgtgtatc tctacctgac atcattacta tgcagttttc ttggttcttc
    11461 ggctgagaac aatctcagca gttagcatag ttaacctatg actattgctt gagatgttct
    11521 actcacaatc tagcttgatt gctgagcgtg ttatgaacta gacaagtttg gaaacctagc
    11581 accaactctc accgacctta agatgaccaa cttctgattg acacaaatag tcaacctgaa
    11641 ggggtaggtc tccttttaag gaggtgatcc agccacacct tccggtacgg ctaccttgtt
    11701 acgacttcac cccagtcatc agccccacct tcggcgtcct cctctgcaag cagttagagt
    11761 aacgacttcg ggcgtgacca actcccatgg tgtgacgggc ggtgtgtaca aggcccggga
    11821 acgtattcac cgcagtatgc tgacctgcga ttactagcga ttccgccttc atgcaggcga
    11881 gttgcagcct gcaatctgaa ctgagcgccg gtttatgaga ttggctcacc atcgctggct
    11941 ggctgctctt tgtccgacgc attgtagtac gtgtgtagcc caaggcgtat ggggcatgct
    12001 gacttgacgt catccacacc ttcctccggt ttgtcaccgg cagtctctct agagtgccca
    12061 actgaatgct ggcaactaaa aacgtgggtt gcgctcgttg cgggacttaa cccaacatct
    12121 cacgacacga gctgacgaca gccatgcacc acctgtctct cggctcccga aggcacccct
    12181 aactttcatc agggttccga ggatgtcaag ccttggtaag gttcttcgcg ttgcatcgaa
    12241 ttaaaccaca tactccaccg cttgtgcggg cccccgtcaa ttcctttgag tttcacactt
    12301 gcgtgcgtac tccccaggcg gacaacttaa cgcgttggct acggcacggc tcgggtcgat
    12361 acgaaccacg cctagttgtc atcgtttacg gctaggacta caggggtatc taatcccttt
    12421 cgctccccta gctttcgtcc ctcagtgtca gtgcagaccc agtaacacgc tttcgccgct
    12481 ggtgttcttc ccaatatcta cgcatttcac cgctacactg ggaattcctg ttacccctat
    12541 cgcactctag ttcatcagtt tccactgccc ttatgcggtt aagccgcacg ctttgacagc
    12601 agacttgata aaccacctac ggacgcttta cgcccaataa ttccggataa cgcttgcatc
    12661 ctccgtatta ccgcggctgc tggcacggag ttagccgatg ctgattcatc aggtaccgtc
    12721 atcgattctt ccctgataaa agaggtttac aacccaaaag ccgtcctccc tcacgcggta
    12781 ttgctccgtc aggctttcgc ccattgcgga aaattcccca ctgctgcctc ccgtaggagt
    12841 ctggaccgtg tctcagttcc agtgtgcctg gtcatcctct cagaccaagt acagatcgtc
    12901 gccttggtgt gccgttacca ctccaactag ctaatctgac gcgagccaat ctctagacaa
    12961 taaatctttc accctaaggc acattcggta ttagcagtcg tttccaactg ttgtccccaa
    13021 tctaaaggca tgttctcacg cgttactcac ccgtccgcca ctatctccga agagaccgtt
    13081 cgacttgcat gtgttaagca taccgccagc gttcatcctg agccaggatc aaactctcca
    13141 tagtaaatag agtaagcttg tagctcaatt
//
