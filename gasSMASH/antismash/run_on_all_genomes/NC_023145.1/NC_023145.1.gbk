LOCUS       NC_023145               2035 bp    DNA     circular CON 02-APR-2017
DEFINITION  Rhodococcus pyridinivorans SB3094 plasmid, complete sequence.
ACCESSION   NC_023145
VERSION     NC_023145.1
DBLINK      BioProject: PRJNA224116
            Assembly: GCF_000511305.1
KEYWORDS    RefSeq.
SOURCE      Rhodococcus pyridinivorans SB3094
  ORGANISM  Rhodococcus pyridinivorans SB3094
            Bacteria; Actinobacteria; Corynebacteriales; Nocardiaceae;
            Rhodococcus.
REFERENCE   1  (bases 1 to 2035)
  AUTHORS   Dueholm,M.S., Albertsen,M., D'Imperio,S., Tale,V.P., Lewis,D.,
            Nielsen,P.H. and Nielsen,J.L.
  TITLE     Complete Genome of Rhodococcus pyridinivorans SB3094, a
            Methyl-Ethyl-Ketone-Degrading Bacterium Used for Bioaugmentation
  JOURNAL   Genome Announc 2 (3), e00525-14 (2014)
   PUBMED   24874690
  REMARK    Publication Status: Online-Only
REFERENCE   2  (bases 1 to 2035)
  AUTHORS   Dueholm,M.S., Albertsen,M., D'Imperio,S., Tale,V.P., Lewis,D.,
            Nielsen,P.H. and Nielsen,J.L.
  TITLE     Direct Submission
  JOURNAL   Submitted (17-DEC-2013) Biotechnology, Chemistry, and Environmental
            Engineering, Aalborg University, Sohngaardsholmsvej 49, Aalborg
            9000, Denmark
COMMENT     ##Genome-Assembly-Data-START##
            Assembly Method                   :: CLC Genomics v. 6
            Genome Coverage                   :: 249
            Sequencing Technology             :: Illumina
            ##Genome-Assembly-Data-END##
            ##Genome-Annotation-Data-START##
            Annotation Provider               :: NCBI
            Annotation Date                   :: 04/02/2017 03:26:30
            Annotation Pipeline               :: NCBI Prokaryotic Genome
            Annotation Method                 :: Best-placed reference protein
            Annotation Software revision      :: 4.1
            Features Annotated                :: Gene; CDS; rRNA; tRNA; ncRNA;
            Genes (total)                     :: 5,165
            CDS (total)                       :: 5,095
            Genes (coding)                    :: 4,893
            CDS (coding)                      :: 4,893
            Genes (RNA)                       :: 70
            rRNAs                             :: 4, 4, 4 (5S, 16S, 23S)
            complete rRNAs                    :: 4, 4, 4 (5S, 16S, 23S)
            tRNAs                             :: 55
            ncRNAs                            :: 3
            Pseudo Genes (total)              :: 202
            Pseudo Genes (ambiguous residues) :: 0 of 202
            Pseudo Genes (frameshifted)       :: 109 of 202
            Pseudo Genes (incomplete)         :: 123 of 202
            Pseudo Genes (internal stop)      :: 14 of 202
            Pseudo Genes (multiple problems)  :: 42 of 202
            ##Genome-Annotation-Data-END##
            REFSEQ INFORMATION: The reference sequence was derived from
            CP006998.
            The bacteria sequenced are properties of Novozymes Biological,
            Salem, VA. Requests should be directed to wastewater@novozymes.com.
            Annotation was added by the NCBI Prokaryotic Genome Annotation
            Pipeline (released 2013). Information about the Pipeline can be
            found here: https://www.ncbi.nlm.nih.gov/genome/annotation_prok/
            Annotation Pipeline
            set; GeneMarkS+
            repeat_region
            COMPLETENESS: full length.
            ##antiSMASH-Data-START##
            Version      :: 5.0.0-af0cea8(changed)
            Run date     :: 2019-06-07 15:05:29
            ##antiSMASH-Data-END##
FEATURES             Location/Qualifiers
     gene            1..131
                     /db_xref="GeneID:29940061"
                     /locus_tag="Y013_RS25535_LOWER"
                     /old_locus_tag="Y013_26500"
     CDS             1..131
                     /codon_start=1
                     /db_xref="GeneID:29940061"
                     /inference="COORDINATES: ab initio prediction:GeneMarkS+"
                     /locus_tag="Y013_RS25535_LOWER"
                     /note="Derived by automated computational analysis using
                     gene prediction method: GeneMarkS+."
                     /old_locus_tag="Y013_26500"
                     /product="hypothetical protein"
                     /protein_id="WP_024100494.1"
                     /transl_table=11
                     /translation="TPSMRTPRPRCVK"
     source          1..2035
                     /db_xref="taxon:1435356"
                     /mol_type="genomic DNA"
                     /organism="Rhodococcus pyridinivorans SB3094"
                     /plasmid="unnamed2"
                     /strain="SB3094"
     gene            730..1662
                     /db_xref="GeneID:29940062"
                     /locus_tag="Y013_RS25540"
                     /old_locus_tag="Y013_26505"
     CDS             730..1662
                     /codon_start=1
                     /db_xref="GeneID:29940062"
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_010840486.1"
                     /locus_tag="Y013_RS25540"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /old_locus_tag="Y013_26505"
                     /product="hypothetical protein"
                     /protein_id="WP_024100495.1"
                     /transl_table=11
                     /translation="MTDRGGSHAEDWEQLWLPLWPLATDDLLLGVYRMPRHDALERRYL
                     EANPQALSNLLVVDVDHPDAALRTLSAAGNHPLPNAIVENPRNGHAHAVWALTEPFTRT
                     EYARRKPLAYAAAVTEGLRRAVDGDAAYSGLMTKNPTHSAWATHWIHPAPRSLAELEHG
                     LGRHMPPPRWRQSKRRRENPVGLGRNCALFESARTWAYREIRYHWGDPAGLDRAIWAEA
                     AQINTAFSEPLPDSEVRAIAASIHRWIVTKSRMWADGPVVYEATFVAMQSARGRKSGKV
                     MTPAKIEANRRRATKFDRDLVWKEATDGS"
     gene            1652..1933
                     /db_xref="GeneID:29940060"
                     /locus_tag="Y013_RS25545"
                     /old_locus_tag="Y013_26510"
     CDS             1652..1933
                     /codon_start=1
                     /db_xref="GeneID:29940060"
                     /inference="COORDINATES: similar to AA
                     sequence:RefSeq:WP_010840487.1"
                     /locus_tag="Y013_RS25545"
                     /note="Derived by automated computational analysis using
                     gene prediction method: Protein Homology."
                     /old_locus_tag="Y013_26510"
                     /product="replication protein RepB"
                     /protein_id="WP_024100496.1"
                     /transl_table=11
                     /translation="MGAENPVRRTRTAREVAERIGASSRTVRRIMAEPREEYLARAAER
                     RERVLELRAQGLKLREIAEEVGMTVGGVGTILHHARKAEQSQSEEATA"
     gene            1930..2035
                     /db_xref="GeneID:29940061"
                     /locus_tag="Y013_RS25535_UPPER"
                     /old_locus_tag="Y013_26500"
     CDS             1930..2035
                     /codon_start=1
                     /db_xref="GeneID:29940061"
                     /inference="COORDINATES: ab initio prediction:GeneMarkS+"
                     /locus_tag="Y013_RS25535_UPPER"
                     /note="Derived by automated computational analysis using
                     gene prediction method: GeneMarkS+."
                     /old_locus_tag="Y013_26500"
                     /product="hypothetical protein"
                     /protein_id="WP_024100494.1"
                     /transl_table=11
                     /translation="VSETIAARIVAVQSQLNAVHTELRALAELVNMFDA"
ORIGIN
        1 acaccctcga tgcggacacc gagacctcgg tgcgtgaagt gatcgactca ctcgccgatg
       61 ccggcctcgc cctcaacggc gccgacgaac ctctctccac agccgcacac cacgcccgtt
      121 tgctccccta gccggctcga gagctgggag gatttacagc ggagcgtccg gtcgcccccc
      181 ctgcgggacc ggacgctccg ctgtagggcg cactcccgtc cttcttcggt gatcgtttcg
      241 tccggccaaa aatcgggaca cacctcttgc agaagttctg acaccgggaa aggccggcct
      301 cgggaccgct caccgacccc ccggttcggc tctgagatcg ccggagagcg ccgtgtccta
      361 tcccccgggg tttccgggtg cgtgcggtcc gtctgcggcg ctctcggagc cacacagacg
      421 gaccgcacgg ggatgtgggg cccgccggta ggtccgagaa aagttctgcc gcatccatca
      481 gccgtacccg gctcaccttc cgccctcccc cgacacatcg aaccgtcacg tgccactcaa
      541 ctggccgtgg tcccgatcaa ccacgaatca agatcactac acacctcatg cactaaagct
      601 gcgaacacga gaaacgacgg tccggacgta acggaagagt ggtatttccc gagtggcgcg
      661 gcgaaacatc tgacttggtt cggcgcgtcc tacctaaaaa ataggtcttc gtgtgcgagg
      721 gtcttccgca tgacagaccg cgggggatcg cacgccgagg attgggaaca gttgtggctg
      781 ccgctgtggc ccctggcaac ggacgatttg ttgctggggg tgtatcggat gcctcgccat
      841 gacgcgctcg aacggcgcta tctcgaggcc aaccctcagg cgctgagcaa tctgttggtc
      901 gtcgatgtcg accacccgga cgcggcactg cggacgctgt cggcggcagg aaaccatccg
      961 ctgccgaacg cgatcgtgga gaacccccgc aacggacacg cgcacgccgt gtgggcactg
     1021 accgagccgt tcacacgcac cgagtacgcc cgcaggaaac ccctcgccta cgccgctgcc
     1081 gtcaccgagg gactgcgtcg ggccgtcgac ggtgatgccg cttactcggg tctgatgacc
     1141 aagaacccga cccattcggc atgggcaacg cactggatcc accccgcacc gcggagcttg
     1201 gccgaactcg aacacgggct cggccggcac atgcccccgc cgcggtggag gcagagcaaa
     1261 cgacgccgtg agaacccggt cgggctcggt cgcaactgcg cactattcga gtcagcgcga
     1321 acctgggcct atcgcgaaat ccgttatcac tggggcgatc ccgctggttt ggaccgagcg
     1381 atctgggctg aggccgcgca aatcaatacc gccttttccg agcccctgcc cgacagcgaa
     1441 gtacgcgcga ttgcagcctc catccatcgc tggatcgtca ccaaatcacg catgtgggcc
     1501 gacggtccgg tcgtctacga ggccaccttc gtcgccatgc aatccgcccg cggccgcaag
     1561 agcggcaaag tcatgacacc ggcgaagatc gaggccaatc gtcggcgggc gacgaagttc
     1621 gaccgcgatc tggtgtggaa ggaggccact gatgggagct gagaatccag ttcggcgcac
     1681 ccgcacagcc cgcgaggtcg cagagcggat cggtgcgtcg tctcgcacag tgcgtcgcat
     1741 catggccgag ccgcgggagg agtacctcgc tcgcgctgcc gagcgtcggg agcgggtgct
     1801 cgaactacgc gcccagggcc tgaagctgcg cgagatcgcc gaggaggtcg gcatgacagt
     1861 cggtggcgtc ggcacgatcc tgcaccacgc gcgtaaggcg gagcagtccc agtccgagga
     1921 ggcgacagcg tgagcgagac gatcgcggcc cgcatcgttg cggtgcagtc acagctgaac
     1981 gccgtgcaca ccgagctccg cgctttggcg gagctggtga acatgttcga cgcgg
//
